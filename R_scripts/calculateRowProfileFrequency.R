calculateRowProfileFrequency=function(reviews,threshold){
  
          id_review_validation <-rep(0,1)
          aspectvalue <-rep(0,1)
          aspectlocation <-rep(0,1)
          aspectroom <-rep(0,1)
          aspectcleanliness <-rep(0,1)
          aspectsleepquality<-rep(0,1)
          aspectservice <-rep(0,1)
          aspectfacility<-rep(0,1)
          aspectfoodquality <-rep(0,1)
          profile <-  data.frame(aspectvalue,aspectlocation,aspectroom,aspectcleanliness,aspectsleepquality,aspectservice,aspectfacility,aspectfoodquality)
          
            for(i in 1:dim(reviews)[1])  
            {
              profile$aspectvalue[1] <- profile$aspectvalue[1] +  reviews[i,c(1)]
              profile$aspectlocation[1] <- profile$aspectlocation[1] +  reviews[i,c(3)]
              profile$aspectroom[1] <- profile$aspectroom[1] +  reviews[i,c(5)]
              profile$aspectcleanliness[1] <- profile$aspectcleanliness[1] +  reviews[i,c(7)]
              profile$aspectsleepquality[1]<-profile$aspectsleepquality[1] + reviews[i,c(9)]
              profile$aspectservice[1] <- profile$aspectservice[1] +  reviews[i,c(11)]
              profile$aspectfacility[1]<- profile$aspectfacility[1] +  reviews[i,c(13)]
              profile$aspectfoodquality[1] <- profile$aspectfoodquality[1] +  reviews[i,c(15)]
            }
         
          
           profile$aspectvalue <- if ( profile$aspectvalue[1]>= threshold) 1 else 0
           profile$aspectlocation <- if ( profile$aspectlocation[1]>= threshold) 1 else 0
           profile$aspectroom<- if (profile$aspectroom[1] >= threshold) 1 else 0
           profile$aspectcleanliness <- if (profile$aspectcleanliness[1] >= threshold) 1 else 0
           profile$aspectsleepquality<-if ( profile$aspectsleepquality[1]>= threshold) 1 else 0
           profile$aspectservice <- if ( profile$aspectservice[1]>= threshold) 1 else 0
           profile$aspectfacility<- if ( profile$aspectfacility[1]>= threshold) 1 else 0
           profile$aspectfoodquality <-if ( profile$aspectfoodquality[1]>= threshold) 1 else 0
           
          
          # profile$aspectvalue <-  profile$aspectvalue[1]
          # profile$aspectlocation <-  profile$aspectlocation[1]
          # profile$aspectroom<- profile$aspectroom[1]
          # profile$aspectcleanliness <- profile$aspectcleanliness[1] 
          # profile$aspectsleepquality<- profile$aspectsleepquality[1]
          # profile$aspectservice <-  profile$aspectservice[1]
          # profile$aspectfacility<-  profile$aspectfacility[1]
          # profile$aspectfoodquality <- profile$aspectfoodquality[1]
          
          return(profile)
}

