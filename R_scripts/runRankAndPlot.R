#' Generate the profile based in frequency and weight. Based on the profile rank the review
#' and generate a Graph.
#' 
#' @param debugFunction enable debug mode
#' @param build the user profile and save the data in ../profile
#' @return 
#' @examples
#' runRankAndPlot(TRUE,TRUE)
#files: runRankAndPlot
#files: buildProfileFrequency
#files: buildProfileWeights
#files: getTestData
#files: getTestDataWeights
#files: calculateRowProfileWeights
#files: calculateRowProfileFrequency





runRankAndPlot = function(debugFunction=FALSE,buildProfile=FALSE,runRank=FALSE,con){
  
  library(rjags)
  library(coda)
  library(stringr)
  library(ggplot2)
  library(plyr)
  
  
  source("./R_scripts/buildProfileFrequency.R")
  source("./R_scripts/buildProfileWeights.R")
  
  source("./R_scripts/rankWeightsProfile.R")
  source("./R_scripts/rankFrequency.R")
  
  if (debugFunction==TRUE)
    
  {
    debugonce(buildProfileFrequency)
    debugonce(buildProfileWeights)
  }
  
  
  print("Loading Cross Validation Table")
  fileRDS  <- gzfile('./tables/cross_validation.rds')
  cross_validation <-readRDS(fileRDS)
  
  if(buildProfile==TRUE)
  {
    buildProfileFrequency(cross_validation,1,con)
    #buildProfileWeights(cross_validation,con)
  }
  
  if(runRank==TRUE)
  {
    print("Rank")
    print("Frequency 1")
    #debugonce(rankFrequency)
    rankFrequency(cross_validation,1,con)
    
    
    #print("Simple Weights")
    #debugonce(rankSimpleWeights)
    #rankWeightsProfile(cross_validation,con)
  }
  print("Process finished")
  
}



