/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.aspectoracle;

import com.aylien.textapi.TextAPIClient;
import com.aylien.textapi.TextAPIException;
import com.aylien.textapi.parameters.AspectBasedSentimentParams;
import com.aylien.textapi.parameters.SentimentParams;
import com.aylien.textapi.responses.Aspect;
import com.aylien.textapi.responses.AspectSentence;
import com.aylien.textapi.responses.AspectsSentiment;
import com.aylien.textapi.responses.Sentiment;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author bonesso
 */
public class RunOracle {
    
    private static final String PERSISTENCE_UNIT_NAME = "DataOracle";    
    private static EntityManagerFactory factory;
    
    
    public static void main(String[] args){
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
    
        Query query = em.createQuery("select s from Reviews s ");
        query.setMaxResults(10);
        
        
        TextAPIClient client = new TextAPIClient("bb5a70bb", "1c0eea5e4a316311ef57776874bbe847");       
        AspectBasedSentimentParams.Builder builder = AspectBasedSentimentParams.newBuilder();
        builder.setDomain(AspectBasedSentimentParams.StandardDomain.RESTAURANTS);
       
        AspectsSentiment aspectsSentiment;
        try {
            
            List<Reviews> lstReviews = query.getResultList();
            for (Reviews review : lstReviews) {
                    builder.setText(review.getFreetext());
                    aspectsSentiment = client.aspectBasedSentiment(builder.build());
                    for (Aspect aspect: aspectsSentiment.getAspects()) {
                            System.out.println(aspect);
                    }
                    for (AspectSentence sentence: aspectsSentiment.getSentences()) {
                            System.out.println(sentence);
            }
                    
            }
            
            
            
        } catch (TextAPIException ex) {
            Logger.getLogger(RunOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
        

        /*try {
            dataSet.loadAndSave(em);
        } catch (IOException e) {
            e.printStackTrace();*/
        
        
        System.out.println("Bem vindo !!!");
    }
    
}
