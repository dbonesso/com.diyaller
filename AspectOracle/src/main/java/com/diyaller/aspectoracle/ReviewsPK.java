/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.aspectoracle;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author bonesso
 */
@Embeddable
public class ReviewsPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "hotel")
    private String hotel;
    @Basic(optional = false)
    @Column(name = "author")
    private String author;

    public ReviewsPK() {
    }

    public ReviewsPK(String id, String hotel, String author) {
        this.id = id;
        this.hotel = hotel;
        this.author = author;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        hash += (hotel != null ? hotel.hashCode() : 0);
        hash += (author != null ? author.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReviewsPK)) {
            return false;
        }
        ReviewsPK other = (ReviewsPK) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        if ((this.hotel == null && other.hotel != null) || (this.hotel != null && !this.hotel.equals(other.hotel))) {
            return false;
        }
        if ((this.author == null && other.author != null) || (this.author != null && !this.author.equals(other.author))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.aspectoracle.ReviewsPK[ id=" + id + ", hotel=" + hotel + ", author=" + author + " ]";
    }
    
}
