getHotelReviews = function(merchantid,con){
  
  queryDB <- sprintf( "SELECT
                      matrix_hotel_aspect.aspectvalue , matrix_hotel_aspect.valuepolarity,
                      matrix_hotel_aspect.aspectlocation, matrix_hotel_aspect.locationpolarity,
                      matrix_hotel_aspect.aspectroom, matrix_hotel_aspect.roompolarity,
                      matrix_hotel_aspect.aspectcleanliness, matrix_hotel_aspect.cleanlinesspolarity,
                      matrix_hotel_aspect.aspectsleepquality, matrix_hotel_aspect.sleepqualitypolarity,
                      matrix_hotel_aspect.aspectservice, matrix_hotel_aspect.servicepolarity,
                      matrix_hotel_aspect.aspectfacility, matrix_hotel_aspect.facilitypolarity,
                      matrix_hotel_aspect.aspectfoodquality, matrix_hotel_aspect.foodqualitypolarity,
                      matrix_hotel_aspect.overallpolarity, review_hotel.id, review_hotel.merchantid,                         
                      rating_hotel.value, 
                      rating_hotel.location, 
                      rating_hotel.rooms, 
                      rating_hotel.cleanliness, 
                      rating_hotel.sleepquality, 
                      rating_hotel.service,    
                      matrix_hotel_aspect.overall
                      FROM public.matrix_hotel_aspect,  public.review_hotel,  public.merchant_hotel,
                      public.rating_hotel
                      WHERE matrix_hotel_aspect.id_review_hotel = review_hotel.id AND merchant_hotel.merchantid = review_hotel.merchantid AND   
                      matrix_hotel_aspect.done = 1  AND
                      public.rating_hotel.id_review = review_hotel.id and 
                      review_hotel.merchantid IN (SELECT merchantid from  review_hotel where id = '%s')", paste(merchantid,sep="") )
  
  
  recordSource  <- dbSendQuery(con,queryDB)
  table <- fetch(recordSource, n = -1)
  return(table)
  
}