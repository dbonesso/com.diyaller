
rankFrequency=function(profile){

workdir<- getwd()
setwd("..")
setwd("..")
 
 
                   
  
relevant <- 10
  
for(i in 1:dim(profile)[1])  # for each row
{
  all_reviews_from_hotel <- sqldf(sprintf("select *  from data_hotels where hotel in (select hotel from data_hotels where id = '%s')",profile$id_validation[i]))    
  position <- rep(0,dim(all_reviews_from_hotel)[1])
  rank <- rep(0,dim(all_reviews_from_hotel)[1])
  gain <-  rep(0,dim(all_reviews_from_hotel)[1])
  dg <-  rep(0,dim(all_reviews_from_hotel)[1])
  idg <-  rep(0,dim(all_reviews_from_hotel)[1])
  dcg <-  rep(0,dim(all_reviews_from_hotel)[1])
  idcg <-  rep(0,dim(all_reviews_from_hotel)[1])
  ndcg <-  rep(0,dim(all_reviews_from_hotel)[1])
  reviewId <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectvalue <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectlocation <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectroom <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectcleanliness <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectsleepquality <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectservice <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectfacility <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectfoodquality <- rep(0,dim(all_reviews_from_hotel)[1])
  overall <-rep(0,dim(all_reviews_from_hotel)[1])
  

  rank_reviews <- data.frame(position,rank,gain,dg,idg,dcg,idcg,ndcg,reviewId,aspectvalue,aspectlocation,aspectroom,aspectcleanliness,aspectsleepquality,aspectservice,aspectfacility,aspectfoodquality,overall)
  for(j in 1:dim(all_reviews_from_hotel)[1])  # for each row
  {
    
    rank_reviews$position[j] <- 0
    rank_reviews$rank[j]     <- 0
    rank_reviews$gain[j]     <- 0
    rank_reviews$dg[j]       <- 0
    rank_reviews$idg[j]      <- 0
    rank_reviews$dcg[j]      <- 0
    rank_reviews$idcg[j]     <- 0
    rank_reviews$ndcg[j]     <- 0
    rank_reviews$reviewId[j] <- all_reviews_from_hotel$id[j]
    rank_reviews$aspectvalue[j] <- if (all_reviews_from_hotel$aspectvalue[j] > 0) 1 else 0
    rank_reviews$aspectlocation[j] <- if (all_reviews_from_hotel$aspectlocation[j] > 0) 1 else 0
    rank_reviews$aspectroom[j] <- if (all_reviews_from_hotel$aspectroom[j] > 0) 1 else 0
    rank_reviews$aspectcleanliness[j] <- if (all_reviews_from_hotel$aspectcleanliness[j] > 0) 1 else 0
    rank_reviews$aspectsleepquality[j] <- if (all_reviews_from_hotel$aspectsleepquality[j] > 0) 1 else 0
    rank_reviews$aspectservice[j] <- if (all_reviews_from_hotel$aspectservice[j] > 0) 1 else 0
    rank_reviews$aspectfacility[j] <- if (all_reviews_from_hotel$aspectfacility[j] > 0) 1 else 0
    rank_reviews$aspectfoodquality[j] <- if (all_reviews_from_hotel$aspectfoodquality[j] > 0) 1 else 0
    rank_reviews$overall[j] <- all_reviews_from_hotel$overall[j]
    
    a1 <- rank_reviews$aspectvalue[j] * if (profile$aspectvalue[i]  > 0) 1 else 0
    a2 <- rank_reviews$aspectlocation[j] * if (profile$aspectlocation[i]  > 0) 1 else 0
    a3 <- rank_reviews$aspectroom[j] * if (profile$aspectroom[i]  > 0) 1 else 0
    a4 <- rank_reviews$aspectcleanliness[j] * if (profile$aspectcleanliness[i]  > 0) 1 else 0
    a5 <- rank_reviews$aspectsleepquality[j] * if (profile$aspectsleepquality[i] > 0) 1 else 0
    a6 <- rank_reviews$aspectservice[j] * if (profile$aspectservice[i]  > 0) 1 else 0
    a7 <- rank_reviews$aspectfacility[j] * if (profile$aspectfacility[i] > 0) 1 else 0
    a8 <- rank_reviews$aspectfoodquality[j] * if (profile$aspectfoodquality[i] > 0) 1 else 0
    rank_reviews$rank[j] <- a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8
   
    
  }
  
  rank_reviews<-rank_reviews[ order(-rank_reviews[,2]), ]
  #count <- 0;
  
  #save the rank frequency
  #for(j in 1:dim(rank_reviews)[1])  # for each row
  #{
  #  count = count + 1  
  #  rank_reviews$position[j]  <- count
  #} 
  
  rank_name <- paste('./rank_cross_validation/rank_frequency','_',profile$id_validation[i],'.rds',sep="")
  
  #just load the rank base
  rank_base_line_name <- paste('./rank_cross_validation/rank_frequency_human1','_',profile$id_validation[i],'.rds',sep="")
  fileRDS <- gzfile(rank_base_line_name)
  rank_base_line<-readRDS(fileRDS)
  close(fileRDS)
  #browser()
  #save the rank frequency
  count <- 0
  for(j in 1:dim(rank_reviews)[1])  # for each row
  {
    count = count + 1  
    rank_reviews$position[j]  <- count
    base_line <-rank_base_line[rank_base_line$reviewId==rank_reviews$reviewId[j],] 
    rank_reviews$gain[j] <-  base_line$gain
    rank_reviews$dg[j]   <-  rank_reviews$gain[j] / rank_reviews$position[j]
    rank_reviews$idg[j]  <-  rank_base_line$gain[j] / rank_base_line$position[j]
    
    if(j > 1){ 
      rank_reviews$idcg[j] <-  rank_reviews$idcg[j] + (rank_reviews$idg[j] + rank_reviews$idcg[j-1])
      rank_reviews$dcg[j]  <-  rank_reviews$dcg[j] + (rank_reviews$dg[j] + rank_reviews$dcg[j-1])       
    }else
    { 
      rank_reviews$idcg[j] <-  rank_reviews$idcg[j] + rank_reviews$idg[j] 
      rank_reviews$dcg[j]  <-  rank_reviews$dcg[j] + rank_reviews$dg[j] 
    }
    rank_reviews$ndcg[j] <-  (rank_reviews$dcg[j] /rank_reviews$idcg[j])
    
  }
  
  
 
  
  
  saveRDS(rank_reviews,file=rank_name) 
  
}







id_validation <-  rep(0,dim(profile)[1])
id_test <-  rep(0,dim(profile)[1])
id_hotel <-  rep(0,dim(profile)[1])
MRR <-  rep(0,dim(profile)[1])
NDGC <-  rep(0,dim(profile)[1])
authorreviews  <-  rep(0,dim(profile)[1])
rank_result <- data.frame(id_validation,id_test,id_hotel,MRR,NDGC,authorreviews)


for(i in 1:dim(profile)[1])  # for each row
{
  
  rank_name <- paste('./rank_cross_validation/rank_frequency','_',profile$id_validation[i],'.rds',sep="")
  fileRDS <- gzfile(rank_name)
  rank<-readRDS(fileRDS)
  close(fileRDS)  
  #MRR
  rank_position <-rank[rank$reviewId==profile$id_validation[i],]
  rank_result$id_validation[i] <- profile$id_validation[i]
  n <- dim(rank)[1]
  rank_result$MRR[i] <-  (1/rank_position$position)
  #NDG
  rank_result$id_validation[i] <- cross_validation$id_validation[i]
  rank_result$id_test[i] <-  cross_validation$id_test[i]
  rank_result$id_hotel[i] <- cross_validation$id_hotel[i]
  sizeNDCG <- dim(rank)[1]
  rank_result$NDGC[i] <-  rank$ndcg[sizeNDCG]
  rank_result$authorreviews[i] <-  (profile$authorreviews[i])
  
}

rank_name <- paste('./result_MRR/rank_frequency','.rds',sep="")
saveRDS(rank_result,file=rank_name)

setwd(workdir)



}