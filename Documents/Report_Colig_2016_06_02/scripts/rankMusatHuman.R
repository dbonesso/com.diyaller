
rankMusatHuman=function(human_profile){

workdir<- getwd()
setwd("..")
setwd("..")
  
 
                   
  
relevant <- 10
teste <- 0    
for(i in 1:dim(human_profile)[1])  # for each row
{
  all_reviews_from_hotel <- sqldf(sprintf("select *  from data_hotels where hotel in (select hotel from data_hotels where id = '%s')",human_profile$review[i]))    
  teste <- teste + dim(all_reviews_from_hotel)[1]    
  position <- rep(0,dim(all_reviews_from_hotel)[1])
  rank <- rep(0,dim(all_reviews_from_hotel)[1])
  gain <-  rep(0,dim(all_reviews_from_hotel)[1])
  dg <-  rep(0,dim(all_reviews_from_hotel)[1])
  idg <-  rep(0,dim(all_reviews_from_hotel)[1])
  dcg <-  rep(0,dim(all_reviews_from_hotel)[1])
  idcg <-  rep(0,dim(all_reviews_from_hotel)[1])
  ndcg <-  rep(0,dim(all_reviews_from_hotel)[1])
  reviewId <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectvalue <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectlocation <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectroom <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectcleanliness <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectsleepquality <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectservice <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectfacility <- rep(0,dim(all_reviews_from_hotel)[1])
  aspectfoodquality <- rep(0,dim(all_reviews_from_hotel)[1])
  overall <-rep(0,dim(all_reviews_from_hotel)[1])
  

  rank_reviews <- data.frame(position,rank,gain,dg,idg,dcg,idcg,ndcg,reviewId,aspectvalue,aspectlocation,aspectroom,aspectcleanliness,aspectsleepquality,aspectservice,aspectfacility,aspectfoodquality,overall)
  for(j in 1:dim(all_reviews_from_hotel)[1])  # for each row
  {
    
    rank_reviews$position[j] <- 0
    rank_reviews$rank[j]     <- 0
    rank_reviews$gain[j]     <- 0
    rank_reviews$dg[j]       <- 0
    rank_reviews$idg[j]      <- 0
    rank_reviews$dcg[j]      <- 0
    rank_reviews$idcg[j]     <- 0
    rank_reviews$ndcg[j]     <- 0
    rank_reviews$reviewId[j] <- all_reviews_from_hotel$id[j]
    rank_reviews$aspectvalue[j] <- if (as.numeric(all_reviews_from_hotel$aspectvalue[j]) > 0) 1 else 0
    rank_reviews$aspectlocation[j] <- if (as.numeric(all_reviews_from_hotel$aspectlocation[j]) > 0) 1 else 0
    rank_reviews$aspectroom[j] <- if (as.numeric(all_reviews_from_hotel$aspectroom[j]) > 0) 1 else 0
    rank_reviews$aspectcleanliness[j] <- if (as.numeric(all_reviews_from_hotel$aspectcleanliness[j]) > 0) 1 else 0
    rank_reviews$aspectsleepquality[j] <- if (as.numeric(all_reviews_from_hotel$aspectsleepquality[j]) > 0) 1 else 0
    rank_reviews$aspectservice[j] <- if (as.numeric(all_reviews_from_hotel$aspectservice[j]) > 0) 1 else 0
    rank_reviews$aspectfacility[j] <- if (as.numeric(all_reviews_from_hotel$aspectfacility[j]) > 0) 1 else 0
    rank_reviews$aspectfoodquality[j] <- if (as.numeric(all_reviews_from_hotel$aspectfoodquality[j]) > 0) 1 else 0
    rank_reviews$overall[j] <- all_reviews_from_hotel$overall[j]
    
    a1 <- rank_reviews$aspectvalue[j] * if (human_profile$weightprice[i]  > 0) 1 else 0
    a2 <- rank_reviews$aspectlocation[j] * if (human_profile$weightlocation[i]  > 0) 1 else 0
    a3 <- rank_reviews$aspectroom[j] * if (human_profile$weightroom[i]  > 0) 1 else 0
    a4 <- rank_reviews$aspectcleanliness[j] * if (human_profile$weightclean[i]  > 0) 1 else 0
    a5 <- rank_reviews$aspectsleepquality[j] * if (human_profile$weightsleep[i] > 0) 1 else 0
    a6 <- rank_reviews$aspectservice[j] * if (human_profile$weightservice[i]  > 0) 1 else 0
    a7 <- rank_reviews$aspectfacility[j] * if (human_profile$weightfacility[i] > 0) 1 else 0
    a8 <- rank_reviews$aspectfoodquality[j] * if (human_profile$weightfood[i] > 0) 1 else 0
    rank_reviews$rank[j] <- (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8) * rank_reviews$overall[j]
   
    
  }
  
  rank_reviews<-rank_reviews[ order(-rank_reviews[,2]), ]
  max_rank <- max(rank_reviews$rank)
  min_rank <- min(rank_reviews$rank)
  for(j in 1:dim(all_reviews_from_hotel)[1])  # for each row
  {
    
    #rank_reviews$gain[j] <- max_rank - rank_reviews$rank[j]
    rank_reviews$gain[j] <-round((((rank_reviews$rank[j] - min_rank) * (4 - 0)) / ((max_rank - min_rank) + 0)))
  }
  count <- 0;
  
  #save the rank frequency
  for(j in 1:dim(rank_reviews)[1])  # for each row
  {
    count = count + 1  
    rank_reviews$position[j]  <- count
  } 
  
  rank_name <- paste('./rank_cross_validation/rank_musat_human','_',human_profile$review[i],'.rds',sep="")
  saveRDS(rank_reviews,file=rank_name) 
  
}

print("Musat")
print(teste)


id_validation <-  rep(0,dim(human_profile)[1])
#id_test <-  rep(0,dim(human_profile)[1])
#id_hotel <-  rep(0,dim(human_profile)[1])
MRR <-  rep(0,dim(human_profile)[1])
#authorreviews  <-  rep(0,dim(human_profile)[1])
rank_result <- data.frame(id_validation,MRR)


for(i in 1:dim(human_profile)[1])  # for each row
{
  
  rank_name <- paste('./rank_cross_validation/rank_musat_human','_',human_profile$review[i],'.rds',sep="")
  fileRDS <- gzfile(rank_name)
  rank<-readRDS(fileRDS)
  close(fileRDS)  
  rank_position <-rank[rank$reviewId==human_profile$review[i],]
  rank_result$id_validation[i] <- human_profile$review[i]
  #rank_result$id_test[i] <-  cross_validation$id_test[i]
  #rank_result$id_hotel[i] <- cross_validation$id_hotel[i]
  n <- dim(rank)[1]
  rank_result$MRR[i] <-  (1/rank_position$position)
  #rank_result$authorreviews[i] <-  (user_profile$authorreviews[i])
  
}

rank_name <- paste('./result_MRR/rank_musat_human','.rds',sep="")
saveRDS(rank_result,file=rank_name)

setwd(workdir)



}