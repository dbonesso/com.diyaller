saveDataMongo = function (reviews){
  
  m <- mongo(collection = "experiment.review")
  
  
  reviewId               <-  rep(0,205)
  reviewText             <-  rep(0,205)
  groupReview            <-  rep(0,205)
  experiments            <-  rep(0,205)
  reviewExperiment  <-  data.frame(reviewId,reviewText,groupReview,experiments)
  
  for(i in 1:dim(reviews)[2])
  {
    reviewExperiment$reviewId[i] <- reviews$id[i]
    reviewExperiment$reviewText[i] <- reviews$content[i]
     if (i  < 52)
      reviewExperiment$groupReview[i] <- 1
     
    if(i < 104 && i > 52)
       reviewExperiment$groupReview[i] <- 2
    
    if(i < 156 && i > 104)
      reviewExperiment$groupReview[i] <- 3
    
    if( i > 156)
      reviewExperiment$groupReview[i] <- 4
    
    reviewExperiment$experiments[i] <- 0
  }
  
  
  m$insert(reviewExperiment)
  
}