const express = require('express');
const bodyParser = require('body-parser')
const app = express();

var mongo = require('mongodb');
var monk = require('monk');
//var db = monk('mongodb://experimental:data103@ds011024.mlab.com:11024/experimental');
var db = monk('mongodb://experimental:data103@ds021182.mlab.com:21182/experimental_test');
var session = require('client-sessions');


app.use(session({
  cookieName: 'session',
  secret: 'the-secret-007',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));

app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static(__dirname + '/public'));
app.set('view engine','pug');




app.use(function(req, res, next) {
  if (req.session && req.session.user) {

        db.get('user').findOne({ user: req.session.user }, function(err, user) {
        if (user) {
          req.session.user = user['user'];
          req.session.trained = user['trained'];
          req.session.stars = user['stars'];

          res.locals.user = user['user'];
          res.locals.trained = user['trained'];
          res.locals.stars = user['stars'];
          if (req.session.trained == 'true')
          {

            db.get(req.session.user).count({analized: { $eq: "true" }}, function (error, analyzed) {
                  db.get(req.session.user).count({ }, function (error, countTotal) {
                    req.session.analyzed  = analyzed + 1;
                    req.session.countTotal = countTotal;
                    res.locals.analyzed  = analyzed + 1;
                    res.locals.countTotal = countTotal;
                  });
            });


          }

       }
        next();
      });
} else {
  next();
}
});


function restrict(req, res, next) {
  if (req.session && req.session.user) {
    next();
  } else {
    res.redirect('/login');
  }
}

app.listen(3000, ()=>{
            console.log('Listening on 3000');
          });



app.get('/',(req,res) =>{
    res.render('login',{ title : 'Experiment.' })
})


app.get('/instructions',restrict,(req,res) =>{

    res.render('instructions.pug',{ title : 'Instructions.' })
})




app.get('/login',(req,res) =>{
    res.render('login');
})



//create the user and send to judgment.
app.post('/login',(req,res) =>{

   db.get('user').findOne({user : req.body['email']},function(error, auth){
      //if user already exist!!!
      if (auth != null) {
        req.session.user = auth["user"];
               db.get(req.session.user).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
               if(firstReview == undefined)
               {
                        db.get(req.session.user).count({analized: { $eq: "false" }}, function (error, analyzed) {
                                          if (analyzed == 0)
                                          {
                                             var data={};
                                             data.judge = req.body['email']
                                             res.render('thankyou.pug',data);
                                          }
                        });
               }else
               {
                 db.get(req.session.user).count({analized: { $eq: "true" }}, function (error, analyzed) {
                       db.get(req.session.user).count({ }, function (error, countTotal) {
                         req.session.analyzed  = analyzed + 1;
                         req.session.countTotal = countTotal;
                         res.locals.analyzed  = analyzed + 1;
                         res.locals.countTotal = countTotal;
                         res.render('researchform.pug', firstReview);
                       });
                 });
               }

        });
      } else {
          var user ={
          judge : req.body['email']
          };
          res.render('createUser.pug',user);
     }
   });
});


//this function is called after read the instructions in the first login.
app.post('/instructions',restrict,(req,res) =>{

          if (req.session.trained == 'false')
          {

                    if(req.session.train){
                                  db.get('reviewsTrain').findOne({train : req.session.train},function(err, firstReview) {
                                            res.locals.analyzed   =  req.session.analyzed;
                                            res.locals.countTotal = req.session.countTotal;
                                            res.render('researchform.pug',firstReview);
                                  });
                    }else
                    {
                        db.get('reviewsTrain').findOne({train : 1},function(err, firstReview) {
                                  req.session.analyzed  = 1;
                                  req.session.countTotal = 2;
                                  req.session.train = 1;
                                  res.locals.analyzed  = 1;
                                  res.locals.countTotal = 2;
                                  res.render('researchform.pug',firstReview);
                        });
                      }
           }
           else
           {

               db.get(req.session.user).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
                     res.locals.analyzed  = req.session.analyzed ;
                     res.locals.countTotal = req.session.countTotal;
                     res.render('researchform.pug',firstReview);
                });
           }



});




//save the  judgment and goto the next.
app.post('/savejudgement',restrict,(req,res) =>{
   if(req.session.trained == 'false')
   {
        if(req.session.train ==2){
                 req.session.trained = 'true';
                 db.get(req.session.user).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
                       db.get(req.session.user).count({analized: { $eq: "true" }}, function (error, analyzed) {
                             db.get(req.session.user).count({ }, function (error, countTotal) {
                               db.get('user').update(
                                        {user : req.session.user},
                                        {$set:{trained:"true"}},
                                        function (err, result) {
                                            if (err) throw err;
                                                req.session.analyzed  = analyzed + 1;
                                                req.session.countTotal = countTotal;
                                                res.locals.analyzed  = analyzed + 1;
                                                res.locals.countTotal = countTotal;
                                                res.render('researchform.pug',firstReview);
                                        });
                          });
                             });
                       });


        }else
        {
           db.get('reviewsTrain').findOne({train : req.session.train + 1},function(err, firstReview) {
               req.session.analyzed  = req.session.train + 1;
               req.session.countTotal = req.session.train + 1;
               res.locals.analyzed  = req.session.train + 1;
               res.locals.countTotal = req.session.train + 1;
               req.session.train = req.session.train + 1 ;
               res.render('researchform.pug',firstReview);
            });
         }
   }else {


      var id    = req.body['id'];
      var judge = req.session.user;
      var value = req.body['value']===undefined ? false : true;
      var location = req.body['location']===undefined ? false : true;
      var room = req.body['room']===undefined ? false : true;
      var clean = req.body['clean']===undefined ? false : true;
      var service = req.body['service']===undefined ? false : true;
      var sleep = req.body['sleep']===undefined ? false : true;
      var facility = req.body['facility']===undefined ? false : true;
      var food = req.body['food']===undefined ? false : true;

      var weightvalue = req.body['weightvalue'] ===undefined ? 0 : req.body['weightvalue'];
      var weightlocation = req.body['weightlocation'] ===undefined ? 0 : req.body['weightlocation'];
      var weightroom = req.body['weightroom']===undefined ? 0 : req.body['weightroom'];
      var weightclean = req.body['weightclean']===undefined ? 0 : req.body['weightclean'];
      var weightservice = req.body['weightservice']===undefined ? 0 : req.body['weightservice'];
      var weightsleep = req.body['weightsleep']===undefined ? 0 : req.body['weightsleep'];
      var weightfacility = req.body['weightfacility']===undefined ? 0 : req.body['weightfacility'] ;
      var weightfood = req.body['weightfood']===undefined ? 0 : req.body['weightfood'];


    if(value == false)
      weightvalue =0;
    if(location == false)
      weightlocation =0;
    if(room == false)
      weightroom =0;
    if(clean == false)
      weightclean =0;
    if(service == false)
      weightservice =0;
    if(sleep == false)
      weightsleep =0;
    if(facility == false)
      weightfacility =0;
    if(food == false)
      weightfood =0;


      var review ={};
      review.id = id;
      review.judge = judge;
      review.value = value;
      review.location = location;
      review.room  = room;
      review.clean = clean;
      review.service = service;
      review.sleep = sleep;
      review.facility = facility;
      review.food = food;
      review.weightvalue  = weightvalue;
      review.weightlocation = weightlocation;
      review.weightroom  = weightroom;
      review.weightclean  = weightclean;
      review.weightservice = weightservice;
      review.weightsleep = weightsleep;
      review.weightfacility = weightfacility;
      review.weightfood = weightfood;


       db.get('judgement').insert(review ,function (err, result){
       db.get(req.session.user).update(
                    {id : req.body['id']},
                    {$set:{analized:"true" }},
                    function (err, result) {
                        if (err) throw err;
                        db.get(req.session.user).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
                            if (err) throw err;
                            if(firstReview == undefined)
                            {
                                     db.get(req.session.user).count({analized: { $eq: "false" }}, function (error, analyzed) {
                                                       if (analyzed == 0)
                                                       {
                                                          var data={};
                                                          data.judge = req.session.user
                                                          res.render('thankyou.pug',data);
                                                       }
                                     });
                            }else{
                                db.get(req.session.user).count({analized: { $eq: "true" }}, function (error, analyzed) {
                                      db.get(req.session.user).count({ }, function (error, countTotal) {
                                        req.session.analyzed  = analyzed + 1;
                                        req.session.countTotal = countTotal;
                                        res.locals.analyzed  = analyzed + 1;
                                        res.locals.countTotal = countTotal;
                                        res.render('researchform.pug',firstReview);
                                      });
                                });
                            }


                        });

                    });

     });
   }
});


//save the judgment and close section.
app.post('/closeSessionSave',restrict,(req,res) =>{
  var id    = req.body['id'];
  var judge = req.session.user;
  var value = req.body['value']===undefined ? false : true;
  var location = req.body['location']===undefined ? false : true;
  var room = req.body['room']===undefined ? false : true;
  var clean = req.body['clean']===undefined ? false : true;
  var service = req.body['service']===undefined ? false : true;
  var sleep = req.body['sleep']===undefined ? false : true;
  var facility = req.body['facility']===undefined ? false : true;
  var food = req.body['food']===undefined ? false : true;

  var weightvalue = req.body['weightvalue'] ===undefined ? 0 : req.body['weightvalue'];
  var weightlocation = req.body['weightlocation'] ===undefined ? 0 : req.body['weightlocation'];
  var weightroom = req.body['weightroom']===undefined ? 0 : req.body['weightroom'];
  var weightclean = req.body['weightclean']===undefined ? 0 : req.body['weightclean'];
  var weightservice = req.body['weightservice']===undefined ? 0 : req.body['weightservice'];
  var weightsleep = req.body['weightsleep']===undefined ? 0 : req.body['weightsleep'];
  var weightfacility = req.body['weightfacility']===undefined ? 0 : req.body['weightfacility'] ;
  var weightfood = req.body['weightfood']===undefined ? 0 : req.body['weightfood'];


if(value == false)
  weightvalue =0;
if(location == false)
  weightlocation =0;
if(room == false)
  weightroom =0;
if(clean == false)
  weightclean =0;
if(service == false)
  weightservice =0;
if(sleep == false)
  weightsleep =0;
if(facility == false)
  weightfacility =0;
if(food == false)
  weightfood =0;


  var review ={};
  review.id = id;
  review.judge = judge;
  review.value = value;
  review.location = location;
  review.room  = room;
  review.clean = clean;
  review.service = service;
  review.sleep = sleep;
  review.facility = facility;
  review.food = food;
  review.weightvalue  = weightvalue;
  review.weightlocation = weightlocation;
  review.weightroom  = weightroom;
  review.weightclean  = weightclean;
  review.weightservice = weightservice;
  review.weightsleep = weightsleep;
  review.weightfacility = weightfacility;
  review.weightfood = weightfood;

  db.get('judgement').insert(review ,function (err, result){
    db.get(req.session.user).update(
                 {id : req.body['id']},
                 {$set:{analized:"true" }},
                 function (err, result) {
                     if (err) throw err;
                     req.session.reset();
                     res.redirect('/login');


                 });

  });
});



//save the judgment and close section.
app.post('/thankyou',restrict,(req,res) =>{
   var survey  ={};
   survey.judge =  req.session.user;
   survey.text  =req.body['suggestion'];
   db.get(req.session.user).drop();
   db.get('user').remove( {user : req.session.user } )

   db.get('survey').insert(survey ,function (err, result){
       req.session.reset();
       res.redirect('/login');
  });

});




//save the judgment and close section.
app.post('/Exit',(req,res) =>{
  req.session.reset();
  res.redirect('/login');

});



//save the judgment and close section.
app.post('/createUser',(req,res) =>{
  //take the collection which are not analyzed yet or have the minor number of analyzes

  db.get('reviewExperimentControl').findOne({"$query":{},"$orderby":{"total":1}},function(error, resultGroup){
      //take all reviews of the previus select group

      db.get('review').find({ group: resultGroup["group"] },function(error, resultReview) {
             if(error) throw err;
             var countJudge = resultGroup['total'] + 1;
             var activateStarts = 'false';
             if(resultGroup["group"]=='group1')
             {
                 var activateStarts = 'true';
             }



             db.get('reviewExperimentControl').update(
                          { group : resultGroup["group"]},
                          { $set: { total: countJudge } },
                          function (err, result) {
                              if (err) throw err;
                              var user ={
                                  user : req.body['judge'],
                                  group : resultGroup["group"],
                                  completed : "false",
                                  trained : "false",
                                  stars : activateStarts
                              };
                              db.get('user').insert(user ,function (err, result) {
                                  if (err) throw err;
                                  db.get(req.body['judge']).insert(resultReview ,function (err, result) {
                                      if (err) throw err;
                                      db.get(req.body['judge']).update(
                                                   {},
                                                   {$set:{analized:"false", judge: req.body['judge']}},
                                                   { multi: true },
                                                   function (err, result) {
                                                       if (err) throw err;
                                                       db.get(req.body['judge']).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
                                                            if (err) throw err;
                                                             var data ={
                                                                  id : firstReview['id'],
                                                                  judge : req.body['judge']

                                                             };
                                                             req.session.trained = 'false';
                                                             req.session.user = req.body['judge'];
                                                             res.render('instructions',data);
                                                       });


                                                   });
                                  });
                              });
              })







      });

  });


});
