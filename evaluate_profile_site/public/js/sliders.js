$(function() {
  $( "#sliderPrice" ).slider({
    value:1,
    min: 1,
    max: 3,
    step: 1,
    slide: function( event, ui ) {

      if (ui.value==1)
      {
             $( "#inputPrice" ).val(" Not Important.");
             $( "#weightvalue" ).val(1);

      }
      if (ui.value==2)
      {
             $( "#inputPrice" ).val("Important.");
             $( "#weightvalue" ).val(2);
      }
      if (ui.value==3)
      {
              $( "#inputPrice" ).val("Very important.");
              $( "#weightvalue" ).val(3);
      }



    }
  });

  if ($( "#sliderPrice" ).slider( "value")==1)
  {
      $( "#inputPrice" ).val("Not Important.");
      $( "#weightvalue" ).val(1);
    }



});

$(document).ready(function() {
     $("#divprice").hide();
     $("#divlocation").hide();
     $("#divroom").hide();
     $("#divclean").hide();
     $("#divservice").hide();
     $("#divsleep").hide();
     $("#divfacility").hide();
     $("#divfood").hide();

});


function checkPrice(chk) {

  if(chk.checked) {
      $("#divprice").show();
  }else
  {
      $("#divprice").hide();
  }
}


$(function() {
  $( "#sliderLocation" ).slider({
    value:1,
    min: 1,
    max: 3,
    step: 1,
    slide: function( event, ui ) {

      if (ui.value==1)
      {
             $( "#inputLocation" ).val(" Not Important.");
             $( "#weightlocation" ).val(1);
      }
      if (ui.value==2)
      {
             $( "#inputLocation" ).val("Important.");
             $( "#weightlocation" ).val(2);
      }
      if (ui.value==3)
      {
              $( "#inputLocation" ).val("Very important.");
              $( "#weightlocation" ).val(3);
      }



    }
  });

  if ($( "#sliderLocation" ).slider( "value")==1)
  {
      $( "#inputLocation" ).val("Not Important.");
      $( "#weightlocation" ).val(1);
  }



});

function checkLocation(chk) {

  if(chk.checked) {
      $("#divlocation").show();
  }else
  {
      $("#divlocation").hide();
  }
}





$(function() {
  $( "#sliderRoom" ).slider({
    value:1,
    min: 1,
    max: 3,
    step: 1,
    slide: function( event, ui ) {

      if (ui.value==1){
             $( "#inputRoom" ).val(" Not Important.");
             $( "#weightroom" ).val(1);
      }
      if (ui.value==2){
             $( "#inputRoom" ).val("Important.");
             $( "#weightroom" ).val(2);
      }
      if (ui.value==3){
              $( "#inputRoom" ).val("Very important.");
              $( "#weightroom" ).val(3);
      }



    }
  });

  if ($( "#sliderRoom" ).slider( "value")==1)
  {
      $( "#inputRoom" ).val("Not Important.");
      $( "#weightroom" ).val(1);
  }



});


function checkRoom(chk) {

  if(chk.checked) {
      $("#divroom").show();
  }else
  {
      $("#divroom").hide();
  }
}


$(function() {
  $( "#sliderClean" ).slider({
    value:1,
    min: 1,
    max: 3,
    step: 1,
    slide: function( event, ui ) {

      if (ui.value==1){
             $( "#inputClean" ).val(" Not Important.");
             $( "#weightclean" ).val(1);
      }
      if (ui.value==2){
             $( "#inputClean" ).val("Important.");
             $( "#weightclean" ).val(2);
      }
      if (ui.value==3){
              $( "#inputClean" ).val("Very important.");
              $( "#weightclean" ).val(3);
      }



    }
  });

  if ($( "#sliderClean" ).slider( "value")==1)
  {
      $( "#inputClean" ).val("Not Important.");
      $( "#weightclean" ).val(1);
  }



});


function checkClean(chk) {

  if(chk.checked) {
      $("#divclean").show();
  }else
  {
      $("#divclean").hide();
  }
}


$(function() {
  $( "#sliderService" ).slider({
    value:1,
    min: 1,
    max: 3,
    step: 1,
    slide: function( event, ui ) {

      if (ui.value==1)
      {
             $( "#inputService" ).val(" Not Important.");
             $( "#weightservice" ).val(1);
      }
      if (ui.value==2)
      {
             $( "#inputService" ).val("Important.");
             $( "#weightservice" ).val(2);
      }
      if (ui.value==3)
      {
              $( "#inputService" ).val("Very important.");
              $( "#weightservice" ).val(3);
      }



    }
  });

  if ($( "#sliderService" ).slider( "value")==1)
  {
      $( "#inputService" ).val("Not Important.");
      $( "#weightservice" ).val(1);
  }



});

function checkService(chk) {

  if(chk.checked) {
      $("#divservice").show();
  }else
  {
      $("#divservice").hide();
  }
}





$(function() {
  $( "#sliderSleep" ).slider({
    value:1,
    min: 1,
    max: 3,
    step: 1,
    slide: function( event, ui ) {

      if (ui.value==1)
      {
             $( "#inputSleep" ).val(" Not Important.");
             $( "#weightsleep" ).val(1);
      }
      if (ui.value==2)
      {
             $( "#inputSleep" ).val("Important.");
             $( "#weightsleep" ).val(2);
      }
      if (ui.value==3)
      {
              $( "#inputSleep" ).val("Very important.");
              $( "#weightsleep" ).val(3);
      }



    }
  });

  if ($( "#sliderSleep" ).slider( "value")==1)
  {
      $( "#inputSleep" ).val("Not Important.");
      $( "#weightsleep" ).val(1);
  }



});

function checkSleep(chk) {

  if(chk.checked) {
      $("#divsleep").show();
  }else
  {
      $("#divsleep").hide();
  }
}




$(function() {
  $( "#sliderFacility" ).slider({
    value:1,
    min: 1,
    max: 3,
    step: 1,
    slide: function( event, ui ) {

      if (ui.value==1)
      {
             $( "#inputFacility" ).val(" Not Important.");
               $( "#weightfacility" ).val(1);
      }
      if (ui.value==2)
      {
             $( "#inputFacility" ).val("Important.");
               $( "#weightfacility" ).val(2);
      }
      if (ui.value==3)
      {
              $( "#inputFacility" ).val("Very important.");
                $( "#weightfacility" ).val(3);
      }



    }
  });

  if ($( "#sliderFacility" ).slider( "value")==1)
  {
      $( "#inputFacility" ).val("Not Important.");
      $( "#weightfacility" ).val(1);
  }



});


function checkFacility(chk) {

  if(chk.checked) {
      $("#divfacility").show();
  }else
  {
      $("#divfacility").hide();
  }
}



$(function() {
  $( "#sliderFood" ).slider({
    value:1,
    min: 1,
    max: 3,
    step: 1,
    slide: function( event, ui ) {

      if (ui.value==1)
      {
             $( "#inputFood" ).val(" Not Important.");
             $( "#weightfood" ).val(1);
      }
      if (ui.value==2)
      {
             $( "#inputFood" ).val("Important.");
             $( "#weightfood" ).val(2);
      }
      if (ui.value==3)
      {
              $( "#inputFood" ).val("Very important.");
              $( "#weightfood" ).val(3);
      }



    }
  });

  if ($( "#sliderFood" ).slider( "value")==1)
  {
      $( "#inputFood" ).val("Not Important.");
      $( "#weightfood" ).val(1);
  }



});

function checkFood(chk) {

  if(chk.checked) {
      $("#divfood").show();
  }else
  {
      $("#divfood").hide();
  }
}
