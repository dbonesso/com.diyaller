
          # JAGS model specification begins here...
  model {
  # Likelihood:
  for( i in 1:N ) {
  y[i] ~ dnorm( y.hat[i] , tau )
  y.hat[i] <- b0 +  inprod( b[1:nPred] , x[i,1:nPred])
  }
  
  # Prior (assumes standardized data):Check the model is reasonable 
  tau ~dgamma(1,0.001)
  sigma ~ dunif( 0 , 5 )
  b0 ~ dnorm(0,0.0001)
  
  for ( j in 1:nPred ) {
  b[j] ~ dnorm( 0 , 1.0E-2 )
  }
  }
  # ... end JAGS model specification
  
