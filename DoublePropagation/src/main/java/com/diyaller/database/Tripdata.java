/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "tripdata")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tripdata.findAll", query = "SELECT t FROM Tripdata t"),
    @NamedQuery(name = "Tripdata.findById", query = "SELECT t FROM Tripdata t WHERE t.id = :id"),
    @NamedQuery(name = "Tripdata.findByFreetext", query = "SELECT t FROM Tripdata t WHERE t.freetext = :freetext"),
    @NamedQuery(name = "Tripdata.findByAuthor", query = "SELECT t FROM Tripdata t WHERE t.author = :author"),
    @NamedQuery(name = "Tripdata.findByHotel", query = "SELECT t FROM Tripdata t WHERE t.hotel = :hotel"),
    @NamedQuery(name = "Tripdata.findByProcessed", query = "SELECT t FROM Tripdata t WHERE t.processed = :processed")})
public class Tripdata implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Column(name = "freetext")
    private String freetext;
    @Column(name = "author")
    private String author;
    @Column(name = "hotel")
    private String hotel;
    @Column(name = "processed")
    private Boolean processed;

    public Tripdata() {
    }

    public Tripdata(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFreetext() {
        return freetext;
    }

    public void setFreetext(String freetext) {
        this.freetext = freetext;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tripdata)) {
            return false;
        }
        Tripdata other = (Tripdata) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.Tripdata[ id=" + id + " ]";
    }
    
}
