/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "user_profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserProfile.findAll", query = "SELECT u FROM UserProfile u"),
    @NamedQuery(name = "UserProfile.findById", query = "SELECT u FROM UserProfile u WHERE u.id = :id"),
    @NamedQuery(name = "UserProfile.findByAspectvalue", query = "SELECT u FROM UserProfile u WHERE u.aspectvalue = :aspectvalue"),
    @NamedQuery(name = "UserProfile.findByValuepolarity", query = "SELECT u FROM UserProfile u WHERE u.valuepolarity = :valuepolarity"),
    @NamedQuery(name = "UserProfile.findByAspectlocation", query = "SELECT u FROM UserProfile u WHERE u.aspectlocation = :aspectlocation"),
    @NamedQuery(name = "UserProfile.findByLocationpolarity", query = "SELECT u FROM UserProfile u WHERE u.locationpolarity = :locationpolarity"),
    @NamedQuery(name = "UserProfile.findByAspectroom", query = "SELECT u FROM UserProfile u WHERE u.aspectroom = :aspectroom"),
    @NamedQuery(name = "UserProfile.findByRoompolarity", query = "SELECT u FROM UserProfile u WHERE u.roompolarity = :roompolarity"),
    @NamedQuery(name = "UserProfile.findByAspectcleanliness", query = "SELECT u FROM UserProfile u WHERE u.aspectcleanliness = :aspectcleanliness"),
    @NamedQuery(name = "UserProfile.findByCleanlinesspolarity", query = "SELECT u FROM UserProfile u WHERE u.cleanlinesspolarity = :cleanlinesspolarity"),
    @NamedQuery(name = "UserProfile.findByAspectsleepquality", query = "SELECT u FROM UserProfile u WHERE u.aspectsleepquality = :aspectsleepquality"),
    @NamedQuery(name = "UserProfile.findBySleepqualitypolarity", query = "SELECT u FROM UserProfile u WHERE u.sleepqualitypolarity = :sleepqualitypolarity"),
    @NamedQuery(name = "UserProfile.findByAspectservice", query = "SELECT u FROM UserProfile u WHERE u.aspectservice = :aspectservice"),
    @NamedQuery(name = "UserProfile.findByServicepolarity", query = "SELECT u FROM UserProfile u WHERE u.servicepolarity = :servicepolarity"),
    @NamedQuery(name = "UserProfile.findByAspectfacility", query = "SELECT u FROM UserProfile u WHERE u.aspectfacility = :aspectfacility"),
    @NamedQuery(name = "UserProfile.findByFacilitypolarity", query = "SELECT u FROM UserProfile u WHERE u.facilitypolarity = :facilitypolarity"),
    @NamedQuery(name = "UserProfile.findByAspectfoodquality", query = "SELECT u FROM UserProfile u WHERE u.aspectfoodquality = :aspectfoodquality"),
    @NamedQuery(name = "UserProfile.findByFoodqualitypolarity", query = "SELECT u FROM UserProfile u WHERE u.foodqualitypolarity = :foodqualitypolarity"),
    @NamedQuery(name = "UserProfile.findByOverall", query = "SELECT u FROM UserProfile u WHERE u.overall = :overall"),
    @NamedQuery(name = "UserProfile.findByOverallpolarity", query = "SELECT u FROM UserProfile u WHERE u.overallpolarity = :overallpolarity")})
public class UserProfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "aspectvalue")
    private Double aspectvalue;
    @Column(name = "valuepolarity")
    private Double valuepolarity;
    @Column(name = "aspectlocation")
    private Double aspectlocation;
    @Column(name = "locationpolarity")
    private Double locationpolarity;
    @Column(name = "aspectroom")
    private Double aspectroom;
    @Column(name = "roompolarity")
    private Double roompolarity;
    @Column(name = "aspectcleanliness")
    private Double aspectcleanliness;
    @Column(name = "cleanlinesspolarity")
    private Double cleanlinesspolarity;
    @Column(name = "aspectsleepquality")
    private Double aspectsleepquality;
    @Column(name = "sleepqualitypolarity")
    private Double sleepqualitypolarity;
    @Column(name = "aspectservice")
    private Double aspectservice;
    @Column(name = "servicepolarity")
    private Double servicepolarity;
    @Column(name = "aspectfacility")
    private Double aspectfacility;
    @Column(name = "facilitypolarity")
    private Double facilitypolarity;
    @Column(name = "aspectfoodquality")
    private Double aspectfoodquality;
    @Column(name = "foodqualitypolarity")
    private Double foodqualitypolarity;
    @Column(name = "overall")
    private Double overall;
    @Column(name = "overallpolarity")
    private Double overallpolarity;
    @JoinColumn(name = "id_review_hotel", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ReviewHotel idReviewHotel;

    public UserProfile() {
    }

    public UserProfile(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAspectvalue() {
        return aspectvalue;
    }

    public void setAspectvalue(Double aspectvalue) {
        this.aspectvalue = aspectvalue;
    }

    public Double getValuepolarity() {
        return valuepolarity;
    }

    public void setValuepolarity(Double valuepolarity) {
        this.valuepolarity = valuepolarity;
    }

    public Double getAspectlocation() {
        return aspectlocation;
    }

    public void setAspectlocation(Double aspectlocation) {
        this.aspectlocation = aspectlocation;
    }

    public Double getLocationpolarity() {
        return locationpolarity;
    }

    public void setLocationpolarity(Double locationpolarity) {
        this.locationpolarity = locationpolarity;
    }

    public Double getAspectroom() {
        return aspectroom;
    }

    public void setAspectroom(Double aspectroom) {
        this.aspectroom = aspectroom;
    }

    public Double getRoompolarity() {
        return roompolarity;
    }

    public void setRoompolarity(Double roompolarity) {
        this.roompolarity = roompolarity;
    }

    public Double getAspectcleanliness() {
        return aspectcleanliness;
    }

    public void setAspectcleanliness(Double aspectcleanliness) {
        this.aspectcleanliness = aspectcleanliness;
    }

    public Double getCleanlinesspolarity() {
        return cleanlinesspolarity;
    }

    public void setCleanlinesspolarity(Double cleanlinesspolarity) {
        this.cleanlinesspolarity = cleanlinesspolarity;
    }

    public Double getAspectsleepquality() {
        return aspectsleepquality;
    }

    public void setAspectsleepquality(Double aspectsleepquality) {
        this.aspectsleepquality = aspectsleepquality;
    }

    public Double getSleepqualitypolarity() {
        return sleepqualitypolarity;
    }

    public void setSleepqualitypolarity(Double sleepqualitypolarity) {
        this.sleepqualitypolarity = sleepqualitypolarity;
    }

    public Double getAspectservice() {
        return aspectservice;
    }

    public void setAspectservice(Double aspectservice) {
        this.aspectservice = aspectservice;
    }

    public Double getServicepolarity() {
        return servicepolarity;
    }

    public void setServicepolarity(Double servicepolarity) {
        this.servicepolarity = servicepolarity;
    }

    public Double getAspectfacility() {
        return aspectfacility;
    }

    public void setAspectfacility(Double aspectfacility) {
        this.aspectfacility = aspectfacility;
    }

    public Double getFacilitypolarity() {
        return facilitypolarity;
    }

    public void setFacilitypolarity(Double facilitypolarity) {
        this.facilitypolarity = facilitypolarity;
    }

    public Double getAspectfoodquality() {
        return aspectfoodquality;
    }

    public void setAspectfoodquality(Double aspectfoodquality) {
        this.aspectfoodquality = aspectfoodquality;
    }

    public Double getFoodqualitypolarity() {
        return foodqualitypolarity;
    }

    public void setFoodqualitypolarity(Double foodqualitypolarity) {
        this.foodqualitypolarity = foodqualitypolarity;
    }

    public Double getOverall() {
        return overall;
    }

    public void setOverall(Double overall) {
        this.overall = overall;
    }

    public Double getOverallpolarity() {
        return overallpolarity;
    }

    public void setOverallpolarity(Double overallpolarity) {
        this.overallpolarity = overallpolarity;
    }

    public ReviewHotel getIdReviewHotel() {
        return idReviewHotel;
    }

    public void setIdReviewHotel(ReviewHotel idReviewHotel) {
        this.idReviewHotel = idReviewHotel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserProfile)) {
            return false;
        }
        UserProfile other = (UserProfile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.UserProfile[ id=" + id + " ]";
    }
    
}
