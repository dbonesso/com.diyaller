/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import com.diyaller.doublepropagation.AbstractReview;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "review_hotel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReviewHotel.findAll", query = "SELECT r FROM ReviewHotel r"),
    @NamedQuery(name = "ReviewHotel.findByTitle", query = "SELECT r FROM ReviewHotel r WHERE r.title = :title"),
    @NamedQuery(name = "ReviewHotel.findByAuthor", query = "SELECT r FROM ReviewHotel r WHERE r.author = :author"),
    @NamedQuery(name = "ReviewHotel.findByContent", query = "SELECT r FROM ReviewHotel r WHERE r.content = :content"),
    @NamedQuery(name = "ReviewHotel.findByDate", query = "SELECT r FROM ReviewHotel r WHERE r.date = :date"),
    @NamedQuery(name = "ReviewHotel.findById", query = "SELECT r FROM ReviewHotel r WHERE r.id = :id")})
public class ReviewHotel implements Serializable,AbstractReview {

    private static final long serialVersionUID = 1L;
    @Column(name = "title")
    private String title;
    @Column(name = "author")
    private String author;
    @Column(name = "content")
    private String content;
    @Column(name = "date")
    private String date;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @JoinColumn(name = "merchantid", referencedColumnName = "merchantid")
    @ManyToOne
    private MerchantHotel merchantid;
    @OneToMany(mappedBy = "idReviewHotel")
    private Collection<ReviewHotelAspect> reviewHotelAspectCollection;
    @OneToMany(mappedBy = "idReview")
    private Collection<RatingHotel> ratingHotelCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReviewHotel")
    private Collection<UserProfile> userProfileCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReviewHotel")
    private Collection<UserReviewHotelAspect> userReviewHotelAspectCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReviewHotel")
    private Collection<MatrixHotelAspect> matrixHotelAspectCollection;

    public ReviewHotel() {
    }

    public ReviewHotel(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MerchantHotel getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(MerchantHotel merchantid) {
        this.merchantid = merchantid;
    }

    @XmlTransient
    public Collection<ReviewHotelAspect> getReviewHotelAspectCollection() {
        return reviewHotelAspectCollection;
    }

    public void setReviewHotelAspectCollection(Collection<ReviewHotelAspect> reviewHotelAspectCollection) {
        this.reviewHotelAspectCollection = reviewHotelAspectCollection;
    }

    @XmlTransient
    public Collection<RatingHotel> getRatingHotelCollection() {
        return ratingHotelCollection;
    }

    public void setRatingHotelCollection(Collection<RatingHotel> ratingHotelCollection) {
        this.ratingHotelCollection = ratingHotelCollection;
    }

    @XmlTransient
    public Collection<UserProfile> getUserProfileCollection() {
        return userProfileCollection;
    }

    public void setUserProfileCollection(Collection<UserProfile> userProfileCollection) {
        this.userProfileCollection = userProfileCollection;
    }

    @XmlTransient
    public Collection<UserReviewHotelAspect> getUserReviewHotelAspectCollection() {
        return userReviewHotelAspectCollection;
    }

    public void setUserReviewHotelAspectCollection(Collection<UserReviewHotelAspect> userReviewHotelAspectCollection) {
        this.userReviewHotelAspectCollection = userReviewHotelAspectCollection;
    }

    @XmlTransient
    public Collection<MatrixHotelAspect> getMatrixHotelAspectCollection() {
        return matrixHotelAspectCollection;
    }

    public void setMatrixHotelAspectCollection(Collection<MatrixHotelAspect> matrixHotelAspectCollection) {
        this.matrixHotelAspectCollection = matrixHotelAspectCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReviewHotel)) {
            return false;
        }
        ReviewHotel other = (ReviewHotel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.ReviewHotel[ id=" + id + " ]";
    }
    
}
