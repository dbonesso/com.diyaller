/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "revieworacle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Revieworacle.findAll", query = "SELECT r FROM Revieworacle r"),
    @NamedQuery(name = "Revieworacle.findById", query = "SELECT r FROM Revieworacle r WHERE r.revieworaclePK.id = :id"),
    @NamedQuery(name = "Revieworacle.findByHotel", query = "SELECT r FROM Revieworacle r WHERE r.revieworaclePK.hotel = :hotel"),
    @NamedQuery(name = "Revieworacle.findByAuthor", query = "SELECT r FROM Revieworacle r WHERE r.revieworaclePK.author = :author"),
    @NamedQuery(name = "Revieworacle.findBySentence", query = "SELECT r FROM Revieworacle r WHERE r.revieworaclePK.sentence = :sentence"),
    @NamedQuery(name = "Revieworacle.findByAspect", query = "SELECT r FROM Revieworacle r WHERE r.revieworaclePK.aspect = :aspect"),
    @NamedQuery(name = "Revieworacle.findByAspectconfidence", query = "SELECT r FROM Revieworacle r WHERE r.aspectconfidence = :aspectconfidence"),
    @NamedQuery(name = "Revieworacle.findByPolarity", query = "SELECT r FROM Revieworacle r WHERE r.polarity = :polarity"),
    @NamedQuery(name = "Revieworacle.findByPolarityconfidence", query = "SELECT r FROM Revieworacle r WHERE r.polarityconfidence = :polarityconfidence")})
public class Revieworacle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RevieworaclePK revieworaclePK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "aspectconfidence")
    private Double aspectconfidence;
    @Column(name = "polarity")
    private String polarity;
    @Column(name = "polarityconfidence")
    private Double polarityconfidence;

    public Revieworacle() {
    }

    public Revieworacle(RevieworaclePK revieworaclePK) {
        this.revieworaclePK = revieworaclePK;
    }

    public Revieworacle(String id, String hotel, String author, String sentence, String aspect) {
        this.revieworaclePK = new RevieworaclePK(id, hotel, author, sentence, aspect);
    }

    public RevieworaclePK getRevieworaclePK() {
        return revieworaclePK;
    }

    public void setRevieworaclePK(RevieworaclePK revieworaclePK) {
        this.revieworaclePK = revieworaclePK;
    }

    public Double getAspectconfidence() {
        return aspectconfidence;
    }

    public void setAspectconfidence(Double aspectconfidence) {
        this.aspectconfidence = aspectconfidence;
    }

    public String getPolarity() {
        return polarity;
    }

    public void setPolarity(String polarity) {
        this.polarity = polarity;
    }

    public Double getPolarityconfidence() {
        return polarityconfidence;
    }

    public void setPolarityconfidence(Double polarityconfidence) {
        this.polarityconfidence = polarityconfidence;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (revieworaclePK != null ? revieworaclePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Revieworacle)) {
            return false;
        }
        Revieworacle other = (Revieworacle) object;
        if ((this.revieworaclePK == null && other.revieworaclePK != null) || (this.revieworaclePK != null && !this.revieworaclePK.equals(other.revieworaclePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.Revieworacle[ revieworaclePK=" + revieworaclePK + " ]";
    }
    
}
