/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "user_review_hotel_aspect")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserReviewHotelAspect.findAll", query = "SELECT u FROM UserReviewHotelAspect u"),
    @NamedQuery(name = "UserReviewHotelAspect.findByAspectid", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.aspectid = :aspectid"),
    @NamedQuery(name = "UserReviewHotelAspect.findByIduser", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.iduser = :iduser"),
    @NamedQuery(name = "UserReviewHotelAspect.findByExtractrule", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.extractrule = :extractrule"),
    @NamedQuery(name = "UserReviewHotelAspect.findByLemmatarget", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.lemmatarget = :lemmatarget"),
    @NamedQuery(name = "UserReviewHotelAspect.findByPostagtarget", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.postagtarget = :postagtarget"),
    @NamedQuery(name = "UserReviewHotelAspect.findByWordformtarget", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.wordformtarget = :wordformtarget"),
    @NamedQuery(name = "UserReviewHotelAspect.findByPosbegintarget", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.posbegintarget = :posbegintarget"),
    @NamedQuery(name = "UserReviewHotelAspect.findByPosendtarget", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.posendtarget = :posendtarget"),
    @NamedQuery(name = "UserReviewHotelAspect.findByLemmaopinion", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.lemmaopinion = :lemmaopinion"),
    @NamedQuery(name = "UserReviewHotelAspect.findByPostagopinion", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.postagopinion = :postagopinion"),
    @NamedQuery(name = "UserReviewHotelAspect.findByWordformopinion", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.wordformopinion = :wordformopinion"),
    @NamedQuery(name = "UserReviewHotelAspect.findByPosbeginopinion", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.posbeginopinion = :posbeginopinion"),
    @NamedQuery(name = "UserReviewHotelAspect.findByPosendopinion", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.posendopinion = :posendopinion"),
    @NamedQuery(name = "UserReviewHotelAspect.findByProcessed", query = "SELECT u FROM UserReviewHotelAspect u WHERE u.processed = :processed")})
public class UserReviewHotelAspect implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "aspectid")
    private Integer aspectid;
    @Column(name = "iduser")
    private String iduser;
    @Column(name = "extractrule")
    private String extractrule;
    @Column(name = "lemmatarget")
    private String lemmatarget;
    @Column(name = "postagtarget")
    private String postagtarget;
    @Column(name = "wordformtarget")
    private String wordformtarget;
    @Column(name = "posbegintarget")
    private Integer posbegintarget;
    @Column(name = "posendtarget")
    private Integer posendtarget;
    @Column(name = "lemmaopinion")
    private String lemmaopinion;
    @Column(name = "postagopinion")
    private String postagopinion;
    @Column(name = "wordformopinion")
    private String wordformopinion;
    @Column(name = "posbeginopinion")
    private Integer posbeginopinion;
    @Column(name = "posendopinion")
    private Integer posendopinion;
    @Column(name = "processed")
    private Boolean processed;
    @JoinColumn(name = "id_review_hotel", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ReviewHotel idReviewHotel;

    public UserReviewHotelAspect() {
    }

    public UserReviewHotelAspect(Integer aspectid) {
        this.aspectid = aspectid;
    }

    public Integer getAspectid() {
        return aspectid;
    }

    public void setAspectid(Integer aspectid) {
        this.aspectid = aspectid;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getExtractrule() {
        return extractrule;
    }

    public void setExtractrule(String extractrule) {
        this.extractrule = extractrule;
    }

    public String getLemmatarget() {
        return lemmatarget;
    }

    public void setLemmatarget(String lemmatarget) {
        this.lemmatarget = lemmatarget;
    }

    public String getPostagtarget() {
        return postagtarget;
    }

    public void setPostagtarget(String postagtarget) {
        this.postagtarget = postagtarget;
    }

    public String getWordformtarget() {
        return wordformtarget;
    }

    public void setWordformtarget(String wordformtarget) {
        this.wordformtarget = wordformtarget;
    }

    public Integer getPosbegintarget() {
        return posbegintarget;
    }

    public void setPosbegintarget(Integer posbegintarget) {
        this.posbegintarget = posbegintarget;
    }

    public Integer getPosendtarget() {
        return posendtarget;
    }

    public void setPosendtarget(Integer posendtarget) {
        this.posendtarget = posendtarget;
    }

    public String getLemmaopinion() {
        return lemmaopinion;
    }

    public void setLemmaopinion(String lemmaopinion) {
        this.lemmaopinion = lemmaopinion;
    }

    public String getPostagopinion() {
        return postagopinion;
    }

    public void setPostagopinion(String postagopinion) {
        this.postagopinion = postagopinion;
    }

    public String getWordformopinion() {
        return wordformopinion;
    }

    public void setWordformopinion(String wordformopinion) {
        this.wordformopinion = wordformopinion;
    }

    public Integer getPosbeginopinion() {
        return posbeginopinion;
    }

    public void setPosbeginopinion(Integer posbeginopinion) {
        this.posbeginopinion = posbeginopinion;
    }

    public Integer getPosendopinion() {
        return posendopinion;
    }

    public void setPosendopinion(Integer posendopinion) {
        this.posendopinion = posendopinion;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public ReviewHotel getIdReviewHotel() {
        return idReviewHotel;
    }

    public void setIdReviewHotel(ReviewHotel idReviewHotel) {
        this.idReviewHotel = idReviewHotel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aspectid != null ? aspectid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserReviewHotelAspect)) {
            return false;
        }
        UserReviewHotelAspect other = (UserReviewHotelAspect) object;
        if ((this.aspectid == null && other.aspectid != null) || (this.aspectid != null && !this.aspectid.equals(other.aspectid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.UserReviewHotelAspect[ aspectid=" + aspectid + " ]";
    }
    
}
