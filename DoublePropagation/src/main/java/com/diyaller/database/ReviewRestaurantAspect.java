/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "review_restaurant_aspect")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReviewRestaurantAspect.findAll", query = "SELECT r FROM ReviewRestaurantAspect r"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByAspectid", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.aspectid = :aspectid"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByExtractrule", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.extractrule = :extractrule"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByLemmatarget", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.lemmatarget = :lemmatarget"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByPostagtarget", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.postagtarget = :postagtarget"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByWordformtarget", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.wordformtarget = :wordformtarget"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByPosbegintarget", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.posbegintarget = :posbegintarget"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByPosendtarget", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.posendtarget = :posendtarget"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByLemmaopinion", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.lemmaopinion = :lemmaopinion"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByPostagopinion", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.postagopinion = :postagopinion"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByWordformopinion", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.wordformopinion = :wordformopinion"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByPosbeginopinion", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.posbeginopinion = :posbeginopinion"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByPosendopinion", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.posendopinion = :posendopinion"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByProcessed", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.processed = :processed"),
    @NamedQuery(name = "ReviewRestaurantAspect.findByPolarity", query = "SELECT r FROM ReviewRestaurantAspect r WHERE r.polarity = :polarity")})
public class ReviewRestaurantAspect implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "aspectid")
    private Integer aspectid;
    @Column(name = "extractrule")
    private String extractrule;
    @Column(name = "lemmatarget")
    private String lemmatarget;
    @Column(name = "postagtarget")
    private String postagtarget;
    @Column(name = "wordformtarget")
    private String wordformtarget;
    @Column(name = "posbegintarget")
    private Integer posbegintarget;
    @Column(name = "posendtarget")
    private Integer posendtarget;
    @Column(name = "lemmaopinion")
    private String lemmaopinion;
    @Column(name = "postagopinion")
    private String postagopinion;
    @Column(name = "wordformopinion")
    private String wordformopinion;
    @Column(name = "posbeginopinion")
    private Integer posbeginopinion;
    @Column(name = "posendopinion")
    private Integer posendopinion;
    @Column(name = "processed")
    private Boolean processed;
    @Column(name = "polarity")
    private Integer polarity;
    @JoinColumn(name = "id_review_restaurant", referencedColumnName = "id")
    @ManyToOne
    private ReviewRestaurant idReviewRestaurant;

    public ReviewRestaurantAspect() {
    }

    public ReviewRestaurantAspect(Integer aspectid) {
        this.aspectid = aspectid;
    }

    public Integer getAspectid() {
        return aspectid;
    }

    public void setAspectid(Integer aspectid) {
        this.aspectid = aspectid;
    }

    public String getExtractrule() {
        return extractrule;
    }

    public void setExtractrule(String extractrule) {
        this.extractrule = extractrule;
    }

    public String getLemmatarget() {
        return lemmatarget;
    }

    public void setLemmatarget(String lemmatarget) {
        this.lemmatarget = lemmatarget;
    }

    public String getPostagtarget() {
        return postagtarget;
    }

    public void setPostagtarget(String postagtarget) {
        this.postagtarget = postagtarget;
    }

    public String getWordformtarget() {
        return wordformtarget;
    }

    public void setWordformtarget(String wordformtarget) {
        this.wordformtarget = wordformtarget;
    }

    public Integer getPosbegintarget() {
        return posbegintarget;
    }

    public void setPosbegintarget(Integer posbegintarget) {
        this.posbegintarget = posbegintarget;
    }

    public Integer getPosendtarget() {
        return posendtarget;
    }

    public void setPosendtarget(Integer posendtarget) {
        this.posendtarget = posendtarget;
    }

    public String getLemmaopinion() {
        return lemmaopinion;
    }

    public void setLemmaopinion(String lemmaopinion) {
        this.lemmaopinion = lemmaopinion;
    }

    public String getPostagopinion() {
        return postagopinion;
    }

    public void setPostagopinion(String postagopinion) {
        this.postagopinion = postagopinion;
    }

    public String getWordformopinion() {
        return wordformopinion;
    }

    public void setWordformopinion(String wordformopinion) {
        this.wordformopinion = wordformopinion;
    }

    public Integer getPosbeginopinion() {
        return posbeginopinion;
    }

    public void setPosbeginopinion(Integer posbeginopinion) {
        this.posbeginopinion = posbeginopinion;
    }

    public Integer getPosendopinion() {
        return posendopinion;
    }

    public void setPosendopinion(Integer posendopinion) {
        this.posendopinion = posendopinion;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public Integer getPolarity() {
        return polarity;
    }

    public void setPolarity(Integer polarity) {
        this.polarity = polarity;
    }

    public ReviewRestaurant getIdReviewRestaurant() {
        return idReviewRestaurant;
    }

    public void setIdReviewRestaurant(ReviewRestaurant idReviewRestaurant) {
        this.idReviewRestaurant = idReviewRestaurant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aspectid != null ? aspectid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReviewRestaurantAspect)) {
            return false;
        }
        ReviewRestaurantAspect other = (ReviewRestaurantAspect) object;
        if ((this.aspectid == null && other.aspectid != null) || (this.aspectid != null && !this.aspectid.equals(other.aspectid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.ReviewRestaurantAspect[ aspectid=" + aspectid + " ]";
    }
    
}
