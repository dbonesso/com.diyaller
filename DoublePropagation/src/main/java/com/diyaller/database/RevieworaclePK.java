/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author bonesso
 */
@Embeddable
public class RevieworaclePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "hotel")
    private String hotel;
    @Basic(optional = false)
    @Column(name = "author")
    private String author;
    @Basic(optional = false)
    @Column(name = "sentence")
    private String sentence;
    @Basic(optional = false)
    @Column(name = "aspect")
    private String aspect;

    public RevieworaclePK() {
    }

    public RevieworaclePK(String id, String hotel, String author, String sentence, String aspect) {
        this.id = id;
        this.hotel = hotel;
        this.author = author;
        this.sentence = sentence;
        this.aspect = aspect;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getAspect() {
        return aspect;
    }

    public void setAspect(String aspect) {
        this.aspect = aspect;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        hash += (hotel != null ? hotel.hashCode() : 0);
        hash += (author != null ? author.hashCode() : 0);
        hash += (sentence != null ? sentence.hashCode() : 0);
        hash += (aspect != null ? aspect.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RevieworaclePK)) {
            return false;
        }
        RevieworaclePK other = (RevieworaclePK) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        if ((this.hotel == null && other.hotel != null) || (this.hotel != null && !this.hotel.equals(other.hotel))) {
            return false;
        }
        if ((this.author == null && other.author != null) || (this.author != null && !this.author.equals(other.author))) {
            return false;
        }
        if ((this.sentence == null && other.sentence != null) || (this.sentence != null && !this.sentence.equals(other.sentence))) {
            return false;
        }
        if ((this.aspect == null && other.aspect != null) || (this.aspect != null && !this.aspect.equals(other.aspect))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.RevieworaclePK[ id=" + id + ", hotel=" + hotel + ", author=" + author + ", sentence=" + sentence + ", aspect=" + aspect + " ]";
    }
    
}
