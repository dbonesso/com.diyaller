/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "matrix_hotel_aspect")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MatrixHotelAspect.findAll", query = "SELECT m FROM MatrixHotelAspect m"),
    @NamedQuery(name = "MatrixHotelAspect.findById", query = "SELECT m FROM MatrixHotelAspect m WHERE m.id = :id"),
    @NamedQuery(name = "MatrixHotelAspect.findByAspectvalue", query = "SELECT m FROM MatrixHotelAspect m WHERE m.aspectvalue = :aspectvalue"),
    @NamedQuery(name = "MatrixHotelAspect.findByValuepolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.valuepolarity = :valuepolarity"),
    @NamedQuery(name = "MatrixHotelAspect.findByAspectlocation", query = "SELECT m FROM MatrixHotelAspect m WHERE m.aspectlocation = :aspectlocation"),
    @NamedQuery(name = "MatrixHotelAspect.findByLocationpolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.locationpolarity = :locationpolarity"),
    @NamedQuery(name = "MatrixHotelAspect.findByAspectroom", query = "SELECT m FROM MatrixHotelAspect m WHERE m.aspectroom = :aspectroom"),
    @NamedQuery(name = "MatrixHotelAspect.findByRoompolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.roompolarity = :roompolarity"),
    @NamedQuery(name = "MatrixHotelAspect.findByAspectcleanliness", query = "SELECT m FROM MatrixHotelAspect m WHERE m.aspectcleanliness = :aspectcleanliness"),
    @NamedQuery(name = "MatrixHotelAspect.findByCleanlinesspolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.cleanlinesspolarity = :cleanlinesspolarity"),
    @NamedQuery(name = "MatrixHotelAspect.findByAspectsleepquality", query = "SELECT m FROM MatrixHotelAspect m WHERE m.aspectsleepquality = :aspectsleepquality"),
    @NamedQuery(name = "MatrixHotelAspect.findBySleepqualitypolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.sleepqualitypolarity = :sleepqualitypolarity"),
    @NamedQuery(name = "MatrixHotelAspect.findByAspectservice", query = "SELECT m FROM MatrixHotelAspect m WHERE m.aspectservice = :aspectservice"),
    @NamedQuery(name = "MatrixHotelAspect.findByServicepolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.servicepolarity = :servicepolarity"),
    @NamedQuery(name = "MatrixHotelAspect.findByAspectfacility", query = "SELECT m FROM MatrixHotelAspect m WHERE m.aspectfacility = :aspectfacility"),
    @NamedQuery(name = "MatrixHotelAspect.findByFacilitypolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.facilitypolarity = :facilitypolarity"),
    @NamedQuery(name = "MatrixHotelAspect.findByAspectfoodquality", query = "SELECT m FROM MatrixHotelAspect m WHERE m.aspectfoodquality = :aspectfoodquality"),
    @NamedQuery(name = "MatrixHotelAspect.findByFoodqualitypolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.foodqualitypolarity = :foodqualitypolarity"),
    @NamedQuery(name = "MatrixHotelAspect.findByOverall", query = "SELECT m FROM MatrixHotelAspect m WHERE m.overall = :overall"),
    @NamedQuery(name = "MatrixHotelAspect.findByOverallpolarity", query = "SELECT m FROM MatrixHotelAspect m WHERE m.overallpolarity = :overallpolarity")})
public class MatrixHotelAspect implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "aspectvalue")
    private Double aspectvalue;
    @Column(name = "valuepolarity")
    private Double valuepolarity;
    @Column(name = "aspectlocation")
    private Double aspectlocation;
    @Column(name = "locationpolarity")
    private Double locationpolarity;
    @Column(name = "aspectroom")
    private Double aspectroom;
    @Column(name = "roompolarity")
    private Double roompolarity;
    @Column(name = "aspectcleanliness")
    private Double aspectcleanliness;
    @Column(name = "cleanlinesspolarity")
    private Double cleanlinesspolarity;
    @Column(name = "aspectsleepquality")
    private Double aspectsleepquality;
    @Column(name = "sleepqualitypolarity")
    private Double sleepqualitypolarity;
    @Column(name = "aspectservice")
    private Double aspectservice;
    @Column(name = "servicepolarity")
    private Double servicepolarity;
    @Column(name = "aspectfacility")
    private Double aspectfacility;
    @Column(name = "facilitypolarity")
    private Double facilitypolarity;
    @Column(name = "aspectfoodquality")
    private Double aspectfoodquality;
    @Column(name = "foodqualitypolarity")
    private Double foodqualitypolarity;
    @Column(name = "overall")
    private Double overall;
    @Column(name = "overallpolarity")
    private Double overallpolarity;
    @JoinColumn(name = "id_review_hotel", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ReviewHotel idReviewHotel;

    public MatrixHotelAspect() {
    }

    public MatrixHotelAspect(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAspectvalue() {
        return aspectvalue;
    }

    public void setAspectvalue(Double aspectvalue) {
        this.aspectvalue = aspectvalue;
    }

    public Double getValuepolarity() {
        return valuepolarity;
    }

    public void setValuepolarity(Double valuepolarity) {
        this.valuepolarity = valuepolarity;
    }

    public Double getAspectlocation() {
        return aspectlocation;
    }

    public void setAspectlocation(Double aspectlocation) {
        this.aspectlocation = aspectlocation;
    }

    public Double getLocationpolarity() {
        return locationpolarity;
    }

    public void setLocationpolarity(Double locationpolarity) {
        this.locationpolarity = locationpolarity;
    }

    public Double getAspectroom() {
        return aspectroom;
    }

    public void setAspectroom(Double aspectroom) {
        this.aspectroom = aspectroom;
    }

    public Double getRoompolarity() {
        return roompolarity;
    }

    public void setRoompolarity(Double roompolarity) {
        this.roompolarity = roompolarity;
    }

    public Double getAspectcleanliness() {
        return aspectcleanliness;
    }

    public void setAspectcleanliness(Double aspectcleanliness) {
        this.aspectcleanliness = aspectcleanliness;
    }

    public Double getCleanlinesspolarity() {
        return cleanlinesspolarity;
    }

    public void setCleanlinesspolarity(Double cleanlinesspolarity) {
        this.cleanlinesspolarity = cleanlinesspolarity;
    }

    public Double getAspectsleepquality() {
        return aspectsleepquality;
    }

    public void setAspectsleepquality(Double aspectsleepquality) {
        this.aspectsleepquality = aspectsleepquality;
    }

    public Double getSleepqualitypolarity() {
        return sleepqualitypolarity;
    }

    public void setSleepqualitypolarity(Double sleepqualitypolarity) {
        this.sleepqualitypolarity = sleepqualitypolarity;
    }

    public Double getAspectservice() {
        return aspectservice;
    }

    public void setAspectservice(Double aspectservice) {
        this.aspectservice = aspectservice;
    }

    public Double getServicepolarity() {
        return servicepolarity;
    }

    public void setServicepolarity(Double servicepolarity) {
        this.servicepolarity = servicepolarity;
    }

    public Double getAspectfacility() {
        return aspectfacility;
    }

    public void setAspectfacility(Double aspectfacility) {
        this.aspectfacility = aspectfacility;
    }

    public Double getFacilitypolarity() {
        return facilitypolarity;
    }

    public void setFacilitypolarity(Double facilitypolarity) {
        this.facilitypolarity = facilitypolarity;
    }

    public Double getAspectfoodquality() {
        return aspectfoodquality;
    }

    public void setAspectfoodquality(Double aspectfoodquality) {
        this.aspectfoodquality = aspectfoodquality;
    }

    public Double getFoodqualitypolarity() {
        return foodqualitypolarity;
    }

    public void setFoodqualitypolarity(Double foodqualitypolarity) {
        this.foodqualitypolarity = foodqualitypolarity;
    }

    public Double getOverall() {
        return overall;
    }

    public void setOverall(Double overall) {
        this.overall = overall;
    }

    public Double getOverallpolarity() {
        return overallpolarity;
    }

    public void setOverallpolarity(Double overallpolarity) {
        this.overallpolarity = overallpolarity;
    }

    public ReviewHotel getIdReviewHotel() {
        return idReviewHotel;
    }

    public void setIdReviewHotel(ReviewHotel idReviewHotel) {
        this.idReviewHotel = idReviewHotel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MatrixHotelAspect)) {
            return false;
        }
        MatrixHotelAspect other = (MatrixHotelAspect) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.MatrixHotelAspect[ id=" + id + " ]";
    }
    
}
