package com.diyaller.database;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.persistence.EntityManager;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Dataset from http://times.cs.uiuc.edu/~wang296/Data/
 * Hongning Wang, Yue Lu and ChengXiang Zhai. Latent Aspect Rating Analysis without Aspect Keyword Supervision.
 * The 17th ACM SIGKDD Conference on Knowledge Discovery and Data Mining (KDD'2011), P618-626, 2011.
 * The code is based in //http://crunchify.com/how-to-read-json-object-from-file-in-java/
 */
//verify with occurs in the file 208210.json
public class ImportWangDataSet {


    private int number = 0;
    private int Totalnumber = 0;

    private int countRows = 0;

    //EntityManager em
    public void loadAndSave() throws IOException {
        Files.walk(Paths.get("/home/bonesso/Dropbox/Dataset/json/")).forEach(filePath -> {
            if (Files.isRegularFile(filePath)) {
                //Start(filePath.toString());
            }
        });


    }
    
    
        

    private void Start(String path, EntityManager em) {

        //em.getTransaction().begin();

        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(new FileReader(path));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = (JSONObject) obj;

        JSONArray reviewList = (JSONArray) jsonObject.get("Reviews");
        Iterator ireview = reviewList.iterator();


        MerchantHotel hotel = new MerchantHotel();
        JSONObject hotelJSON = (JSONObject) jsonObject.get("HotelInfo");
        hotel.setMerchantid((String) hotelJSON.get("HotelID"));
        hotel.setName((String) hotelJSON.get("Name"));
        hotel.setUrl((String) hotelJSON.get("HotelURL"));
        this.number = this.number + 1;
        if (em.find(MerchantHotel.class, hotel.getMerchantid()) == null) {
            em.persist(hotel);
        }
        System.out.println("Save hotel: " + hotel.getName() + " total: " + this.number);
        //em.getTransaction().commit();
        ReviewHotel review;
        RatingHotel rating;


        int service = 0;
        int cleanliness = 0;
        double overall = 0.0;
        int value = 0;
        int sleepquality = 0;
        int rooms = 0;
        int location = 0;

        while (ireview.hasNext()) {
            JSONObject item = (JSONObject) ireview.next();
            review = new ReviewHotel();

            review.setAuthor((String) item.get("Author"));
            review.setContent((String) item.get("Content"));
            review.setDate((String) item.get("Date"));
            review.setId((String) item.get("ReviewID"));
            review.setTitle((String) item.get("Title"));
            review.setMerchantid(hotel);

            System.out.println("Review: " + review.getId() + " total: " + this.Totalnumber);
            if (em.find(ReviewHotel.class, review.getId()) == null) {
                em.getTransaction().begin();
                em.persist(review);
                service = 0;
                cleanliness = 0;
                overall = 0.0;
                value = 0;
                sleepquality = 0;
                rooms = 0;
                location = 0;

                JSONObject subitem = (JSONObject) item.get("Ratings");
                rating = new RatingHotel();

                if (subitem.get("Service") != null)
                    service = Integer.parseInt(subitem.get("Service").toString());

                if (subitem.get("Cleanliness") != null)
                    cleanliness = Integer.parseInt(subitem.get("Cleanliness").toString());

                if (subitem.get("Overall") != null)
                    overall = Double.parseDouble(subitem.get("Overall").toString());

                if (subitem.get("Value") != null)
                    value = Integer.parseInt(subitem.get("Value").toString());

                if (subitem.get("Sleep Quality") != null)
                    sleepquality = Integer.parseInt(subitem.get("Sleep Quality").toString());

                if (subitem.get("Rooms") != null)
                    rooms = Integer.parseInt(subitem.get("Rooms").toString());

                if (subitem.get("Location") != null)
                    location = Integer.parseInt(subitem.get("Location").toString());

                rating.setService(service);
                rating.setCleanliness(cleanliness);
                rating.setOverall(overall);
                rating.setValue(value);
                rating.setSleepquality(sleepquality);
                rating.setRooms(rooms);
                rating.setLocation(location);
                rating.setIdReview(review);
                em.persist(rating);
                em.getTransaction().commit();
                this.Totalnumber = this.Totalnumber + 1;

            } else {
                System.out.println("Achou um repetido");
            }


        }


    }
    
    
    
     public void ImportHotelData(EntityManager em, String datafile) {

        File f = new File("/home/bonesso/Dropbox/DDP/wang-json/" + datafile);
        if(f.exists() && !f.isDirectory()) { 
        

        JSONParser parser = new JSONParser();
        Object obj = null;
            try {

                obj = parser.parse(new FileReader("/home/bonesso/Dropbox/DDP/wang-json/" + datafile));
                JSONObject jsonObject = (JSONObject) obj;

                JSONArray reviewList = (JSONArray) jsonObject.get("Reviews");
                Iterator ireview = reviewList.iterator();

                em.getTransaction().begin();
                MerchantHotel hotel = new MerchantHotel();
                JSONObject hotelJSON = (JSONObject) jsonObject.get("HotelInfo");
                hotel.setMerchantid((String) hotelJSON.get("HotelID"));
                hotel.setName((String) hotelJSON.get("Name"));
                hotel.setUrl((String) hotelJSON.get("HotelURL"));
                this.number = this.number + 1;
                if (em.find(MerchantHotel.class, hotel.getMerchantid()) == null) {
                    em.persist(hotel);
                }
                //System.out.println("Save hotel: " + hotel.getName() + " total: " + this.number);
                em.getTransaction().commit();
                ReviewHotel review;
                RatingHotel rating;

                int service = 0;
                int cleanliness = 0;
                double overall = 0.0;
                int value = 0;
                int sleepquality = 0;
                int rooms = 0;
                int location = 0;

                while (ireview.hasNext()) {
                    JSONObject item = (JSONObject) ireview.next();
                    review = new ReviewHotel();

                    review.setAuthor((String) item.get("Author"));
                    review.setContent((String) item.get("Content"));
                    review.setDate((String) item.get("Date"));
                    review.setId((String) item.get("ReviewID"));
                    review.setTitle((String) item.get("Title"));
                    review.setMerchantid(hotel);

                   // System.out.println("Review: " + review.getId() + " total: " + this.Totalnumber);
                    if (em.find(ReviewHotel.class, review.getId()) == null) {
                        em.getTransaction().begin();
                        em.persist(review);
                        service = 0;
                        cleanliness = 0;
                        overall = 0.0;
                        value = 0;
                        sleepquality = 0;
                        rooms = 0;
                        location = 0;

                        JSONObject subitem = (JSONObject) item.get("Ratings");
                        rating = new RatingHotel();

                        if (subitem.get("Service") != null) {
                            service = Integer.parseInt(subitem.get("Service").toString());
                        }

                        if (subitem.get("Cleanliness") != null) {
                            cleanliness = Integer.parseInt(subitem.get("Cleanliness").toString());
                        }

                        if (subitem.get("Overall") != null) {
                            overall = Double.parseDouble(subitem.get("Overall").toString());
                        }

                        if (subitem.get("Value") != null) {
                            value = Integer.parseInt(subitem.get("Value").toString());
                        }

                        if (subitem.get("Sleep Quality") != null) {
                            sleepquality = Integer.parseInt(subitem.get("Sleep Quality").toString());
                        }

                        if (subitem.get("Rooms") != null) {
                            rooms = Integer.parseInt(subitem.get("Rooms").toString());
                        }

                        if (subitem.get("Location") != null) {
                            location = Integer.parseInt(subitem.get("Location").toString());
                        }

                        rating.setService(service);
                        rating.setCleanliness(cleanliness);
                        rating.setOverall(overall);
                        rating.setValue(value);
                        rating.setSleepquality(sleepquality);
                        rating.setRooms(rooms);
                        rating.setLocation(location);
                        rating.setIdReview(review);
                        em.persist(rating);
                        em.getTransaction().commit();
                        this.Totalnumber = this.Totalnumber + 1;

                    } else {
                        System.out.println("Achou um repetido");
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        
            
        } else{
        
            System.out.println("File does not exist !!!");
        }
         
         
        

    }



    

}
