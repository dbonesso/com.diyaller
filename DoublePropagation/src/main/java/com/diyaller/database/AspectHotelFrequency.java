/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "aspect_hotel_frequency")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AspectHotelFrequency.findAll", query = "SELECT a FROM AspectHotelFrequency a"),
    @NamedQuery(name = "AspectHotelFrequency.findByAspectid", query = "SELECT a FROM AspectHotelFrequency a WHERE a.aspectid = :aspectid"),
    @NamedQuery(name = "AspectHotelFrequency.findByAspect", query = "SELECT a FROM AspectHotelFrequency a WHERE a.aspect = :aspect"),
    @NamedQuery(name = "AspectHotelFrequency.findByFrequency", query = "SELECT a FROM AspectHotelFrequency a WHERE a.frequency = :frequency")})
public class AspectHotelFrequency implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "aspectid")
    private Integer aspectid;
    @Column(name = "aspect")
    private String aspect;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "frequency")
    private Double frequency;

    public AspectHotelFrequency() {
    }

    public AspectHotelFrequency(Integer aspectid) {
        this.aspectid = aspectid;
    }

    public Integer getAspectid() {
        return aspectid;
    }

    public void setAspectid(Integer aspectid) {
        this.aspectid = aspectid;
    }

    public String getAspect() {
        return aspect;
    }

    public void setAspect(String aspect) {
        this.aspect = aspect;
    }

    public Double getFrequency() {
        return frequency;
    }

    public void setFrequency(Double frequency) {
        this.frequency = frequency;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aspectid != null ? aspectid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AspectHotelFrequency)) {
            return false;
        }
        AspectHotelFrequency other = (AspectHotelFrequency) object;
        if ((this.aspectid == null && other.aspectid != null) || (this.aspectid != null && !this.aspectid.equals(other.aspectid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.AspectHotelFrequency[ aspectid=" + aspectid + " ]";
    }
    
}
