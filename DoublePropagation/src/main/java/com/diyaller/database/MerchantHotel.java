/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "merchant_hotel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MerchantHotel.findAll", query = "SELECT m FROM MerchantHotel m"),
    @NamedQuery(name = "MerchantHotel.findByName", query = "SELECT m FROM MerchantHotel m WHERE m.name = :name"),
    @NamedQuery(name = "MerchantHotel.findByUrl", query = "SELECT m FROM MerchantHotel m WHERE m.url = :url"),
    @NamedQuery(name = "MerchantHotel.findByMerchantid", query = "SELECT m FROM MerchantHotel m WHERE m.merchantid = :merchantid")})
public class MerchantHotel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "name")
    private String name;
    @Column(name = "url")
    private String url;
    @Id
    @Basic(optional = false)
    @Column(name = "merchantid")
    private String merchantid;
    @OneToMany(mappedBy = "merchantid")
    private Collection<ReviewHotel> reviewHotelCollection;

    public MerchantHotel() {
    }

    public MerchantHotel(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    @XmlTransient
    public Collection<ReviewHotel> getReviewHotelCollection() {
        return reviewHotelCollection;
    }

    public void setReviewHotelCollection(Collection<ReviewHotel> reviewHotelCollection) {
        this.reviewHotelCollection = reviewHotelCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (merchantid != null ? merchantid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MerchantHotel)) {
            return false;
        }
        MerchantHotel other = (MerchantHotel) object;
        if ((this.merchantid == null && other.merchantid != null) || (this.merchantid != null && !this.merchantid.equals(other.merchantid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.MerchantHotel[ merchantid=" + merchantid + " ]";
    }
    
}
