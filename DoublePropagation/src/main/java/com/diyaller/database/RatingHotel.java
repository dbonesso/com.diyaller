/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "rating_hotel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RatingHotel.findAll", query = "SELECT r FROM RatingHotel r"),
    @NamedQuery(name = "RatingHotel.findByService", query = "SELECT r FROM RatingHotel r WHERE r.service = :service"),
    @NamedQuery(name = "RatingHotel.findByCleanliness", query = "SELECT r FROM RatingHotel r WHERE r.cleanliness = :cleanliness"),
    @NamedQuery(name = "RatingHotel.findByOverall", query = "SELECT r FROM RatingHotel r WHERE r.overall = :overall"),
    @NamedQuery(name = "RatingHotel.findByValue", query = "SELECT r FROM RatingHotel r WHERE r.value = :value"),
    @NamedQuery(name = "RatingHotel.findBySleepquality", query = "SELECT r FROM RatingHotel r WHERE r.sleepquality = :sleepquality"),
    @NamedQuery(name = "RatingHotel.findByRooms", query = "SELECT r FROM RatingHotel r WHERE r.rooms = :rooms"),
    @NamedQuery(name = "RatingHotel.findByLocation", query = "SELECT r FROM RatingHotel r WHERE r.location = :location"),
    @NamedQuery(name = "RatingHotel.findById", query = "SELECT r FROM RatingHotel r WHERE r.id = :id")})
public class RatingHotel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "service")
    private Integer service;
    @Column(name = "cleanliness")
    private Integer cleanliness;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "overall")
    private Double overall;
    @Column(name = "value")
    private Integer value;
    @Column(name = "sleepquality")
    private Integer sleepquality;
    @Column(name = "rooms")
    private Integer rooms;
    @Column(name = "location")
    private Integer location;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "id_review", referencedColumnName = "id")
    @ManyToOne
    private ReviewHotel idReview;

    public RatingHotel() {
    }

    public RatingHotel(Integer id) {
        this.id = id;
    }

    public Integer getService() {
        return service;
    }

    public void setService(Integer service) {
        this.service = service;
    }

    public Integer getCleanliness() {
        return cleanliness;
    }

    public void setCleanliness(Integer cleanliness) {
        this.cleanliness = cleanliness;
    }

    public Double getOverall() {
        return overall;
    }

    public void setOverall(Double overall) {
        this.overall = overall;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getSleepquality() {
        return sleepquality;
    }

    public void setSleepquality(Integer sleepquality) {
        this.sleepquality = sleepquality;
    }

    public Integer getRooms() {
        return rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ReviewHotel getIdReview() {
        return idReview;
    }

    public void setIdReview(ReviewHotel idReview) {
        this.idReview = idReview;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RatingHotel)) {
            return false;
        }
        RatingHotel other = (RatingHotel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.RatingHotel[ id=" + id + " ]";
    }
    
}
