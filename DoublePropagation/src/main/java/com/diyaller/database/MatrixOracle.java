/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "matrix_oracle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MatrixOracle.findAll", query = "SELECT m FROM MatrixOracle m"),
    @NamedQuery(name = "MatrixOracle.findById", query = "SELECT m FROM MatrixOracle m WHERE m.id = :id"),
    @NamedQuery(name = "MatrixOracle.findByAspectvalue", query = "SELECT m FROM MatrixOracle m WHERE m.aspectvalue = :aspectvalue"),
    @NamedQuery(name = "MatrixOracle.findByValuepolarity", query = "SELECT m FROM MatrixOracle m WHERE m.valuepolarity = :valuepolarity"),
    @NamedQuery(name = "MatrixOracle.findByAspectlocation", query = "SELECT m FROM MatrixOracle m WHERE m.aspectlocation = :aspectlocation"),
    @NamedQuery(name = "MatrixOracle.findByLocationpolarity", query = "SELECT m FROM MatrixOracle m WHERE m.locationpolarity = :locationpolarity"),
    @NamedQuery(name = "MatrixOracle.findByAspectroom", query = "SELECT m FROM MatrixOracle m WHERE m.aspectroom = :aspectroom"),
    @NamedQuery(name = "MatrixOracle.findByRoompolarity", query = "SELECT m FROM MatrixOracle m WHERE m.roompolarity = :roompolarity"),
    @NamedQuery(name = "MatrixOracle.findByAspectcleanliness", query = "SELECT m FROM MatrixOracle m WHERE m.aspectcleanliness = :aspectcleanliness"),
    @NamedQuery(name = "MatrixOracle.findByCleanlinesspolarity", query = "SELECT m FROM MatrixOracle m WHERE m.cleanlinesspolarity = :cleanlinesspolarity"),
    @NamedQuery(name = "MatrixOracle.findByAspectsleepquality", query = "SELECT m FROM MatrixOracle m WHERE m.aspectsleepquality = :aspectsleepquality"),
    @NamedQuery(name = "MatrixOracle.findBySleepqualitypolarity", query = "SELECT m FROM MatrixOracle m WHERE m.sleepqualitypolarity = :sleepqualitypolarity"),
    @NamedQuery(name = "MatrixOracle.findByAspectservice", query = "SELECT m FROM MatrixOracle m WHERE m.aspectservice = :aspectservice"),
    @NamedQuery(name = "MatrixOracle.findByServicepolarity", query = "SELECT m FROM MatrixOracle m WHERE m.servicepolarity = :servicepolarity"),
    @NamedQuery(name = "MatrixOracle.findByAspectfacility", query = "SELECT m FROM MatrixOracle m WHERE m.aspectfacility = :aspectfacility"),
    @NamedQuery(name = "MatrixOracle.findByFacilitypolarity", query = "SELECT m FROM MatrixOracle m WHERE m.facilitypolarity = :facilitypolarity"),
    @NamedQuery(name = "MatrixOracle.findByAspectfoodquality", query = "SELECT m FROM MatrixOracle m WHERE m.aspectfoodquality = :aspectfoodquality"),
    @NamedQuery(name = "MatrixOracle.findByFoodqualitypolarity", query = "SELECT m FROM MatrixOracle m WHERE m.foodqualitypolarity = :foodqualitypolarity"),
    @NamedQuery(name = "MatrixOracle.findByOverall", query = "SELECT m FROM MatrixOracle m WHERE m.overall = :overall"),
    @NamedQuery(name = "MatrixOracle.findByOverallpolarity", query = "SELECT m FROM MatrixOracle m WHERE m.overallpolarity = :overallpolarity")})
public class MatrixOracle implements Serializable {

    @Basic(optional = false)
    @Column(name = "author")
    private String author;

    @Basic(optional = false)
    @Column(name = "hotel")
    private String hotel;

    @Column(name = "valuepolarity")
    private Double valuepolarity;

    @Column(name = "locationpolarity")
    private Double locationpolarity;
    @Column(name = "roompolarity")
    private Double roompolarity;
    @Column(name = "cleanlinesspolarity")
    private Double cleanlinesspolarity;
    @Column(name = "sleepqualitypolarity")
    private Double sleepqualitypolarity;
    @Column(name = "servicepolarity")
    private Double servicepolarity;
    @Column(name = "facilitypolarity")
    private Double facilitypolarity;
    @Column(name = "foodqualitypolarity")
    private Double foodqualitypolarity;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "aspectvalue")
    private Double aspectvalue;
    @Column(name = "aspectlocation")
    private Double aspectlocation;
    @Column(name = "aspectroom")
    private Double aspectroom;
    @Column(name = "aspectcleanliness")
    private Double aspectcleanliness;
    @Column(name = "aspectsleepquality")
    private Double aspectsleepquality;
    @Column(name = "aspectservice")
    private Double aspectservice;
    @Column(name = "aspectfacility")
    private Double aspectfacility;
    @Column(name = "aspectfoodquality")
    private Double aspectfoodquality;
    @Column(name = "overall")
    private Double overall;
    @Column(name = "overallpolarity")
    private Double overallpolarity;

    public MatrixOracle() {
    }

    public MatrixOracle(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getAspectvalue() {
        return aspectvalue;
    }

    public void setAspectvalue(Double aspectvalue) {
        this.aspectvalue = aspectvalue;
    }


    public Double getAspectlocation() {
        return aspectlocation;
    }

    public void setAspectlocation(Double aspectlocation) {
        this.aspectlocation = aspectlocation;
    }


    public Double getAspectroom() {
        return aspectroom;
    }

    public void setAspectroom(Double aspectroom) {
        this.aspectroom = aspectroom;
    }


    public Double getAspectcleanliness() {
        return aspectcleanliness;
    }

    public void setAspectcleanliness(Double aspectcleanliness) {
        this.aspectcleanliness = aspectcleanliness;
    }


    public Double getAspectsleepquality() {
        return aspectsleepquality;
    }

    public void setAspectsleepquality(Double aspectsleepquality) {
        this.aspectsleepquality = aspectsleepquality;
    }


    public Double getAspectservice() {
        return aspectservice;
    }

    public void setAspectservice(Double aspectservice) {
        this.aspectservice = aspectservice;
    }


    public Double getAspectfacility() {
        return aspectfacility;
    }

    public void setAspectfacility(Double aspectfacility) {
        this.aspectfacility = aspectfacility;
    }


    public Double getAspectfoodquality() {
        return aspectfoodquality;
    }

    public void setAspectfoodquality(Double aspectfoodquality) {
        this.aspectfoodquality = aspectfoodquality;
    }


    public Double getOverall() {
        return overall;
    }

    public void setOverall(Double overall) {
        this.overall = overall;
    }

    public Double getOverallpolarity() {
        return overallpolarity;
    }

    public void setOverallpolarity(Double overallpolarity) {
        this.overallpolarity = overallpolarity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MatrixOracle)) {
            return false;
        }
        MatrixOracle other = (MatrixOracle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.MatrixOracle[ id=" + id + " ]";
    }

    public Double getLocationpolarity() {
        return locationpolarity;
    }

    public void setLocationpolarity(Double locationpolarity) {
        this.locationpolarity = locationpolarity;
    }

    public Double getRoompolarity() {
        return roompolarity;
    }

    public void setRoompolarity(Double roompolarity) {
        this.roompolarity = roompolarity;
    }

    public Double getCleanlinesspolarity() {
        return cleanlinesspolarity;
    }

    public void setCleanlinesspolarity(Double cleanlinesspolarity) {
        this.cleanlinesspolarity = cleanlinesspolarity;
    }

    public Double getSleepqualitypolarity() {
        return sleepqualitypolarity;
    }

    public void setSleepqualitypolarity(Double sleepqualitypolarity) {
        this.sleepqualitypolarity = sleepqualitypolarity;
    }

    public Double getServicepolarity() {
        return servicepolarity;
    }

    public void setServicepolarity(Double servicepolarity) {
        this.servicepolarity = servicepolarity;
    }

    public Double getFacilitypolarity() {
        return facilitypolarity;
    }

    public void setFacilitypolarity(Double facilitypolarity) {
        this.facilitypolarity = facilitypolarity;
    }

    public Double getFoodqualitypolarity() {
        return foodqualitypolarity;
    }

    public void setFoodqualitypolarity(Double foodqualitypolarity) {
        this.foodqualitypolarity = foodqualitypolarity;
    }

    public Double getValuepolarity() {
        return valuepolarity;
    }

    public void setValuepolarity(Double valuepolarity) {
        this.valuepolarity = valuepolarity;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    
}
