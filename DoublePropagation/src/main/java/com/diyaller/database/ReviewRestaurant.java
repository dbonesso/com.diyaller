/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diyaller.database;

import com.diyaller.doublepropagation.AbstractReview;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bonesso
 */
@Entity
@Table(name = "review_restaurant")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReviewRestaurant.findAll", query = "SELECT r FROM ReviewRestaurant r"),
    @NamedQuery(name = "ReviewRestaurant.findByTitle", query = "SELECT r FROM ReviewRestaurant r WHERE r.title = :title"),
    @NamedQuery(name = "ReviewRestaurant.findByAuthor", query = "SELECT r FROM ReviewRestaurant r WHERE r.author = :author"),
    @NamedQuery(name = "ReviewRestaurant.findByContent", query = "SELECT r FROM ReviewRestaurant r WHERE r.content = :content"),
    @NamedQuery(name = "ReviewRestaurant.findByDate", query = "SELECT r FROM ReviewRestaurant r WHERE r.date = :date"),
    @NamedQuery(name = "ReviewRestaurant.findById", query = "SELECT r FROM ReviewRestaurant r WHERE r.id = :id")})
public class ReviewRestaurant implements Serializable,AbstractReview {

    private static final long serialVersionUID = 1L;
    @Column(name = "title")
    private String title;
    @Column(name = "author")
    private String author;
    @Column(name = "content")
    private String content;
    @Column(name = "date")
    private String date;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @OneToMany(mappedBy = "idReviewRestaurant")
    private Collection<ReviewRestaurantAspect> reviewRestaurantAspectCollection;
    @OneToMany(mappedBy = "idReviewRestaurant")
    private Collection<ReviewRestaurantAspectGold> reviewRestaurantAspectGoldCollection;

    public ReviewRestaurant() {
    }

    public ReviewRestaurant(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<ReviewRestaurantAspect> getReviewRestaurantAspectCollection() {
        return reviewRestaurantAspectCollection;
    }

    public void setReviewRestaurantAspectCollection(Collection<ReviewRestaurantAspect> reviewRestaurantAspectCollection) {
        this.reviewRestaurantAspectCollection = reviewRestaurantAspectCollection;
    }

    @XmlTransient
    public Collection<ReviewRestaurantAspectGold> getReviewRestaurantAspectGoldCollection() {
        return reviewRestaurantAspectGoldCollection;
    }

    public void setReviewRestaurantAspectGoldCollection(Collection<ReviewRestaurantAspectGold> reviewRestaurantAspectGoldCollection) {
        this.reviewRestaurantAspectGoldCollection = reviewRestaurantAspectGoldCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReviewRestaurant)) {
            return false;
        }
        ReviewRestaurant other = (ReviewRestaurant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.diyaller.database.ReviewRestaurant[ id=" + id + " ]";
    }
    
}
