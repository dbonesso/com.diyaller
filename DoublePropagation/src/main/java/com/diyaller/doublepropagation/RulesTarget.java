package com.diyaller.doublepropagation;

import com.diyaller.utils.WordNetJAWS;
import java.io.IOException;
import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.EnglishStemmer;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by dbonesso on 06/11/15.
 * mod adjunct modifier stanford -> amod
 * pnmod post nominal modifier
 * subj subject of verb
 * s surface subject
 * obj object of verb stanford -> amod
 * obj2 sencond object of ditransitive verbs
 * desc description
 */
public class RulesTarget {
    private Map<String,TargetOpinion> targetAndOpinion;
    private Map<String,String> featuresSet;
    private Map<String,String> opinionSet;
    private SnowballProgram stemmer;
    private Map<String, String> relationsTypes;
    private ListOfRelations relations;
    private WordNetJAWS wordNet;


    public RulesTarget(){
        this.opinionSet = new HashMap<String,String>();
        this.featuresSet =  new HashMap<String,String>();
        //given by depency parsing
        this.relations =  new ListOfRelations();
        this.stemmer = new EnglishStemmer();
        this.targetAndOpinion = new HashMap<String,TargetOpinion>();
        this.relationsTypes = new Hashtable<>();
        this.relationsTypes.put("amod","amod");//Adjectival Modifier
        this.relationsTypes.put("nn","nn");//Nominal Compund Modifier
        this.relationsTypes.put("nmod","nmod");// pnmod
        this.relationsTypes.put("dobj","dobj");//Direct Object
        this.relationsTypes.put("nsubj","nsubj");//Nominal Subjective
        this.relationsTypes.put("conj","conj");//Conjuction
        
            this.wordNet = new WordNetJAWS();
            
            //mod the relationship between a word and its adjunct modifier -> stanford (amod)
            //pnmod post nominal modifier -> nmod
            //subj  subject of verbs  -> stanford (nsubj)
            //s     surface subject
            //obj   object of verbs
            //obj2  second object of ditransitive verbs
            //desc  description
        

    }





    public void  firstRules(ListOfRelations relations){



        this.relations.setRelations(relations.getRelations());

           for(TargetOpinion ot1 :  this.checkByRule_1_1())
           {
            if (ot1 != null) {

                if (!this.getFeaturesSet().containsKey(ot1.getTargetLemma())) {
                    if(this.wordNet.checkNoun(ot1.getTargetWord()))
                        this.getFeaturesSet().put(ot1.getTargetLemma(), ot1.getTargetWord() + "->" + ot1.getTargetPosTag() );
                }
                if (!ot1.getOpinionWord().equals("none") && !ot1.getTargetWord().equals("none"))
                   if (!this.targetAndOpinion.containsKey(ot1.getKey()))
                    if(this.wordNet.checkNoun(ot1.getTargetWord()) && this.wordNet.checkAdjective(ot1.getOpinionWord()))
                        this.targetAndOpinion.put(ot1.getKey(), ot1);
            }
           }


        for (TargetOpinion ot2 : this.checkByRule_1_2()){
            if (ot2 !=null)
            {

                if (!this.getFeaturesSet().containsKey(ot2.getTargetLemma()))
                {
                  if(this.wordNet.checkNoun(ot2.getTargetWord()))
                    this.getFeaturesSet().put(ot2.getTargetLemma(),ot2.getTargetWord() + "->" + ot2.getTargetPosTag());
                }
                if(!ot2.getOpinionWord().equals("none") && !ot2.getTargetWord().equals("none"))
                   if  (!this.targetAndOpinion.containsKey(ot2.getKey()))
                    if(this.wordNet.checkNoun(ot2.getTargetWord()) && this.wordNet.checkAdjective(ot2.getOpinionWord()))
                       this.targetAndOpinion.put(ot2.getKey(),ot2);
            }
        }



        for(TargetOpinion ot3 :  this.checkByRule_4_1()) {
            if (ot3 != null) {

                if (!this.getOpinionSet().containsKey(ot3.getOpinionLemma())) {
                    if(this.wordNet.checkAdjective(ot3.getOpinionWord()))
                       this.getOpinionSet().put(ot3.getOpinionLemma(), ot3.getOpinionWord() + "->" + ot3.getOpinionPosTag());
                }
                if (!ot3.getOpinionWord().equals("none") && !ot3.getTargetWord().equals("none"))
                  if (!this.targetAndOpinion.containsKey(ot3.getKey()))
                    if(this.wordNet.checkNoun(ot3.getTargetWord()) && this.wordNet.checkAdjective(ot3.getOpinionWord()))
                        this.targetAndOpinion.put(ot3.getKey(), ot3);
            }
        }


        for(TargetOpinion ot4 : this.checkByRule_4_2())
        {
            if (ot4 !=null) {
                if (!this.getOpinionSet().containsKey(ot4.getOpinionLemma()))
                {
                    if(this.wordNet.checkAdjective(ot4.getOpinionWord()))
                       this.getOpinionSet().put(ot4.getOpinionLemma(), ot4.getOpinionWord() + "->" + ot4.getOpinionPosTag());
                }
                if(!ot4.getOpinionWord().equals("none") && !ot4.getTargetWord().equals("none"))
                  if  (!this.targetAndOpinion.containsKey(ot4.getKey()))
                    if(this.wordNet.checkNoun(ot4.getTargetWord()) && this.wordNet.checkAdjective(ot4.getOpinionWord()))
                          this.targetAndOpinion.put(ot4.getKey(),ot4);
            }
        }


        this.relations.setRelations(relations.getRelations());

        for (TargetOpinion ot5 :  this.checkByRule_1_1_extended())
        {
            if (ot5 != null) {
                if (!this.getFeaturesSet().containsKey(ot5.getTargetLemma())) {
                    if(this.wordNet.checkNoun(ot5.getTargetWord()))
                       this.getFeaturesSet().put(ot5.getTargetLemma(), ot5.getTargetWord() + "->" + ot5.getTargetPosTag());
                }
                if (!ot5.getOpinionWord().equals("none") && !ot5.getTargetWord().equals("none"))
                  if (!this.targetAndOpinion.containsKey(ot5.getKey()))
                    if(this.wordNet.checkNoun(ot5.getTargetWord()) && this.wordNet.checkAdjective(ot5.getOpinionWord()))
                        this.targetAndOpinion.put(ot5.getKey(), ot5);
            }
        }



    }

    public void  secondRules(ListOfRelations relations){



        this.relations.setRelations(relations.getRelations());


        TargetOpinion ot1 =  this.checkByRule_2_1();
        if (ot1 !=null)
        {

            if (!this.getOpinionSet().containsKey(ot1.getOpinionLemma()))
            {
                if(this.wordNet.checkAdjective(ot1.getOpinionWord()))
                   this.getOpinionSet().put(ot1.getOpinionLemma(),ot1.getOpinionWord() + "->" + ot1.getOpinionPosTag());
            }
            if(!ot1.getOpinionWord().equals("none")  && !ot1.getTargetWord().equals("none"))
               if  (!this.targetAndOpinion.containsKey(ot1.getKey()))
                if(this.wordNet.checkNoun(ot1.getTargetWord()) && this.wordNet.checkAdjective(ot1.getOpinionWord()))
                    this.targetAndOpinion.put(ot1.getKey(),ot1);
        }


        TargetOpinion ot2 =  this.checkByRule_2_2();
        if (ot2 !=null)
        {

            if (!this.getOpinionSet().containsKey(ot2.getOpinionLemma()))
            {
                if(this.wordNet.checkAdjective(ot2.getOpinionWord()))
                     this.getOpinionSet().put(ot2.getOpinionLemma(), ot2.getOpinionWord() + "->" + ot2.getOpinionPosTag());
            }
            if(!ot2.getOpinionWord().equals("none")  && !ot2.getTargetWord().equals("none"))
               if  (!this.targetAndOpinion.containsKey(ot2.getKey()))
                if(this.wordNet.checkNoun(ot2.getTargetWord()) && this.wordNet.checkAdjective(ot2.getOpinionWord()))
                    this.targetAndOpinion.put(ot2.getKey(),ot2);
        }



        TargetOpinion ot3 =  this.checkByRule_3_1();
        if (ot3 !=null)
        {

            if (!this.getFeaturesSet().containsKey(ot3.getTargetLemma()))
            {
                if(this.wordNet.checkNoun(ot3.getTargetWord()))
                   this.getFeaturesSet().put(ot3.getTargetLemma(),ot3.getTargetWord() + "->" + ot3.getTargetPosTag());
            }
            if(!ot3.getOpinionWord().equals("none")  && !ot3.getTargetWord().equals("none"))
               if  (!this.targetAndOpinion.containsKey(ot3.getKey()))
                if(this.wordNet.checkNoun(ot3.getTargetWord()) && this.wordNet.checkAdjective(ot3.getOpinionWord()))
                    this.targetAndOpinion.put(ot3.getKey(),ot3);
        }



        TargetOpinion ot4 =  this.checkByRule_3_2();
        if (ot4 !=null)
        {

            if (!this.getFeaturesSet().containsKey(ot4.getTargetLemma()))
            {
                if(this.wordNet.checkNoun(ot4.getTargetWord()))
                    this.getFeaturesSet().put(ot4.getTargetLemma(), ot4.getTargetWord() + "->" + ot4.getTargetPosTag());
            }
            if(!ot4.getOpinionWord().equals("none")  && !ot4.getTargetWord().equals("none"))
               if  (!this.targetAndOpinion.containsKey(ot4.getKey()))
                if(this.wordNet.checkNoun(ot4.getTargetWord()) && this.wordNet.checkAdjective(ot4.getOpinionWord()))
                    this.targetAndOpinion.put(ot4.getKey(),ot4);
        }


    }





    /**
     * R_1_1 the opinion word is know.
     * O→ O-Dep →Ts.t. O ∈{O}, O-Dep ∈ {MR}, POS(T) ∈{NN}
     * @param relations
     * @return
     */
    private List<TargetOpinion> checkByRule_1_1() {
        TargetOpinion ot;

        List<TargetOpinion> l = new ArrayList<>();
        for(Relations relation : this.relations.getRelations()) {
               if(     relationsTypes.containsKey(relation.getRelation()) &&
                       this.getOpinionSet().containsKey(relation.getDependent().getLemma()) &&
                       relation.getGovernor().getPostag().toUpperCase().equals("NN")

                       )

               {
                   ot = new TargetOpinion();                   
                   ot.setOpinionLemma(relation.getDependent().getLemma());
                   ot.setOpinionPosTag(relation.getDependent().getPostag());
                   ot.setOpinionWord(relation.getDependent().getWordForm());
                   ot.setOpinionPosEnd(relation.getDependent().getEnd());
                   ot.setOpinionPosBegin(relation.getDependent().getStart());
                   ot.setRelation(relation.getRelation());
                   ot.setTargetLemma(relation.getGovernor().getLemma());
                   ot.setTargetPosTag(relation.getGovernor().getPostag());
                   ot.setTargetPosEnd(relation.getGovernor().getEnd());
                   ot.setTargetPosBegin(relation.getGovernor().getStart());
                   ot.setTargetWord(relation.getGovernor().getWordForm());
                   ot.setExtractedTarget(relation.getGovernor().getWordForm());
                   ot.setId(relation.getTextId());
                   ot.setRule("R1_1");                   
                   relation.setProcessed(true);
                   ot.setSentence(relation.getSentence());
                   l.add(ot);


               }

            }
        return l;

    }

    /**
     * R_1_1_extendend the opinion word is know.
     * O→ O-Dep →Ts.t. O ∈{O}, O-Dep ∈ {MR}, POS(T) ∈{NN}
     * @param relations
     * @return
     */
    private List<TargetOpinion> checkByRule_1_1_extended() {
        TargetOpinion ot;
        List<TargetOpinion> l = new ArrayList<>();
        for(Relations relation : this.relations.getRelations()) {
            if(     relationsTypes.containsKey(relation.getRelation()) &&
                    this.getOpinionSet().containsKey(relation.getGovernor().getLemma()) &&
                    relation.getDependent().getPostag().toUpperCase().equals("NN")

                    )

            {
                ot = new TargetOpinion();                
                ot.setOpinionLemma(relation.getGovernor().getLemma());
                ot.setOpinionPosTag(relation.getGovernor().getPostag());
                ot.setOpinionWord(relation.getGovernor().getWordForm());
                ot.setOpinionPosEnd(relation.getGovernor().getEnd());
                ot.setOpinionPosBegin(relation.getGovernor().getStart());
                ot.setRelation(relation.getRelation());
                ot.setTargetLemma(relation.getDependent().getLemma());
                ot.setTargetPosTag(relation.getDependent().getPostag());
                ot.setTargetPosEnd(relation.getDependent().getEnd());
                ot.setTargetPosBegin(relation.getDependent().getStart());
                ot.setTargetWord(relation.getDependent().getWordForm());
                ot.setExtractedTarget(relation.getDependent().getWordForm());
                ot.setId(relation.getTextId());
                ot.setRule("R1_1_extended");                
                relation.setProcessed(true);
                ot.setSentence(relation.getSentence());
                l.add(ot);

            }
        }
        return l;

    }


    /**
     * R_1_2
     * O→ O-Dep →H← T-Dep ←Ts.t. O ∈ {O}, O/T-Dep∈{MR}, POS(T) ∈{NN}
     * @param relations
     * @return
     */
    private List<TargetOpinion> checkByRule_1_2() {
        TargetOpinion ot;
        List<TargetOpinion> l = new ArrayList<>();
        for(Relations relation : this.relations.getRelations()) {
            if ( (relation.getDependent().getPostag().toUpperCase().equals("JJ") ||  relation.getDependent().getPostag().toUpperCase().equals("JJS") || relation.getDependent().getPostag().toUpperCase().equals("JJR"))
                    && this.getOpinionSet().containsKey(relation.getDependent().getLemma())
                    && relationsTypes.containsKey(relation.getRelation())

                    ) {
                ot = new TargetOpinion();                
                ot.setOpinionLemma(relation.getDependent().getLemma());
                ot.setOpinionPosTag(relation.getDependent().getPostag());
                ot.setOpinionPosEnd(relation.getDependent().getEnd());
                ot.setOpinionPosBegin(relation.getDependent().getStart());
                ot.setOpinionWord(relation.getDependent().getWordForm());
                ot.setRelation(relation.getRelation());                
                for(Relations relation2 : this.relations.getRelations()) {
                    if ( relation2.getDependent().getPostag().toUpperCase().equals("NN")
                            &&  relation.getGovernor().getLemma().equals(relation2.getGovernor().getLemma())
                            && relationsTypes.containsKey(relation2.getRelation()))
                    {
                        ot.setRelation(relation2.getRelation());
                        ot.setTargetLemma(relation2.getDependent().getLemma());
                        ot.setTargetPosTag(relation2.getDependent().getPostag());
                        ot.setTargetPosEnd(relation2.getDependent().getEnd());
                        ot.setTargetPosBegin(relation2.getDependent().getStart());
                        ot.setTargetWord(relation2.getDependent().getWordForm());
                        ot.setExtractedTarget(relation2.getDependent().getWordForm());
                        ot.setId(relation2.getTextId());
                        ot.setRule("R1_2");                        
                        ot.setId(relation2.getTextId());
                        relation.setProcessed(true);
                        ot.setSentence(relation.getSentence());
                        l.add(ot);
                    }
                }

            }
        }



       return l;

    }

    /**
     * R_2_1 same as R1_1 the target word in know.
     * O→ O-Dep →Ts.t. T ∈{T}, O-Dep ∈ {MR}, POS(O) ∈{JJ}
     * @param relations
     * @return
     */
    private TargetOpinion checkByRule_2_1() {
        for(Relations relation : this.relations.getRelations()) {
            if (relationsTypes.containsKey(relation.getRelation()) &&
                    this.getFeaturesSet().containsKey(relation.getGovernor().getLemma()) &&
                    (relation.getDependent().getPostag().toUpperCase().equals("JJ") ||  relation.getDependent().getPostag().toUpperCase().equals("JJS") || relation.getDependent().getPostag().toUpperCase().equals("JJR"))
              )

            {
                TargetOpinion ot = new TargetOpinion();                
                ot.setOpinionLemma(relation.getDependent().getLemma());
                ot.setOpinionPosTag(relation.getDependent().getPostag());
                ot.setOpinionWord(relation.getDependent().getWordForm());
                ot.setOpinionPosEnd(relation.getDependent().getEnd());
                ot.setOpinionPosBegin(relation.getDependent().getStart());
                ot.setRelation(relation.getRelation());
                ot.setTargetLemma(relation.getGovernor().getLemma());
                ot.setTargetPosTag(relation.getGovernor().getPostag());
                ot.setTargetPosEnd(relation.getGovernor().getEnd());
                ot.setTargetPosBegin(relation.getGovernor().getStart());
                ot.setTargetWord(relation.getGovernor().getWordForm());
                ot.setExtractedTarget(relation.getGovernor().getWordForm());
                ot.setId(relation.getTextId());                
                ot.setRule("R2_1");
                ot.setSentence(relation.getSentence());
                return ot;

            }
        }
        return null;

    }


    /**
     * R_2_2 same of R_1_2 the target word is know
     * O→ O-Dep →H← T-Dep ←Ts.t. T ∈ {T}, O/T-Dep ∈{MR}, POS(O) ∈{JJ}
     * @param relations
     * @return
     */
    private TargetOpinion checkByRule_2_2() {
        String word = "";
        TargetOpinion ot = new TargetOpinion();
        for(Relations relation : this.relations.getRelations()) {
            
            if (relation.getDependent().getPostag().toUpperCase().equals("NN")
                    &&  relationsTypes.containsKey(relation.getRelation())
                    && this.getFeaturesSet().containsKey(relation.getDependent().getLemma())) {
                
                ot.setRelation(relation.getRelation());                
                ot.setTargetLemma(relation.getDependent().getLemma());
                ot.setTargetPosTag(relation.getDependent().getPostag());
                ot.setTargetPosEnd(relation.getDependent().getEnd());
                ot.setTargetPosBegin(relation.getDependent().getStart());
                ot.setTargetWord(relation.getDependent().getWordForm());
                ot.setExtractedTarget(relation.getDependent().getWordForm());
                ot.setId(relation.getTextId());
                ot.setRule("R2_2");                                
                if (word.equals("")) {
                    word = relation.getGovernor().getLemma();
                }


            }
        }

        for(Relations relation : this.relations.getRelations()) {
            if ( (relation.getDependent().getPostag().toUpperCase().equals("JJ") ||  relation.getDependent().getPostag().toUpperCase().equals("JJS") || relation.getDependent().getPostag().toUpperCase().equals("JJR"))
                  &&  word.equals(relation.getGovernor().getLemma())
                  &&  relationsTypes.containsKey(relation.getRelation())
                    ) {
                ot.setOpinionLemma(relation.getDependent().getLemma());
                ot.setOpinionPosTag(relation.getDependent().getPostag());
                ot.setOpinionPosEnd(relation.getDependent().getEnd());
                ot.setOpinionPosBegin(relation.getDependent().getStart());
                ot.setOpinionWord(relation.getDependent().getWordForm());
                ot.setSentence(relation.getSentence());
                return ot;
            }
        }
        return null;
    }

    /**
     * R_3_1
     * Ti(j) →Ti(j)-Dep→Tj(i) s.t. Tj(i) ∈{T},Ti(j)- Dep∈{CONJ},POS(Ti(j)) ∈{NN}
     * Ti(j) you want.
     * Ti(j)->Dep conj
     * Ti(j)->NN
     * TODO Maybe this will be a list of extracted aspects.
     * The opinion was extracted in the last step
     * @param relations
     * @return
     */
    private TargetOpinion checkByRule_3_1() {
        TargetOpinion ot = new TargetOpinion();
        for(Relations relation : this.relations.getRelations()) {
            if ( relation.getRelation().toUpperCase().equals("CONJ")
                    &&  relation.getDependent().getPostag().toUpperCase().equals("NN")
                    && this.getFeaturesSet().containsKey(relation.getGovernor().getLemma()) ) {
                    ot.setOpinionLemma("none");
                    ot.setOpinionPosTag("none");
                    ot.setOpinionWord("none");
                    ot.setOpinionPosEnd(0);
                    ot.setOpinionPosBegin(0);
                    ot.setRelation(relation.getRelation());
                    ot.setTargetLemma(relation.getDependent().getLemma());
                    ot.setTargetPosTag(relation.getDependent().getPostag());
                    ot.setTargetPosEnd(relation.getDependent().getEnd());
                    ot.setTargetPosBegin(relation.getDependent().getStart());
                    ot.setTargetWord(relation.getDependent().getWordForm());
                    ot.setExtractedTarget(relation.getDependent().getWordForm());
                    ot.setId(relation.getTextId());
                    ot.setRule("R3_1");
                    ot.setSentence(relation.getSentence());
                    return ot;

            }
        }
        return null;

    }

    /**
     * R_3_2
     * Ti →Ti-Dep→H←Tj-Dep←Tj s.t. Ti ∈ {T},Ti-Dep==Tj-Dep,POS(Tj) ∈{NN}
     * @param relations
     * @return
     * *
     */

    private TargetOpinion checkByRule_3_2() {
        TargetOpinion ot1 = new TargetOpinion();
        TargetOpinion ot2 = new TargetOpinion();
        String word = "";

        for(Relations relation : this.relations.getRelations()) {
            if (relation.getDependent().getPostag().toUpperCase().equals("NN")
                    && this.getFeaturesSet().containsKey(relation.getDependent().getLemma())
                    &&  relationsTypes.containsKey(relation.getRelation())
               ){

                ot1.setRelation(relation.getRelation());
                ot1.setTargetLemma(relation.getDependent().getLemma());
                ot1.setTargetPosTag(relation.getDependent().getPostag());
                ot1.setTargetPosEnd(relation.getDependent().getEnd());
                ot1.setTargetPosBegin(relation.getDependent().getStart());
                ot1.setTargetWord(relation.getDependent().getWordForm());
                ot1.setExtractedTarget(relation.getDependent().getWordForm());
                ot1.setId(relation.getTextId());                
                if (word.equals("")) {
                    word = relation.getGovernor().getLemma();
                }

            }
        }

        for(Relations relation : this.relations.getRelations()) {
            if ( relation.getDependent().getPostag().toUpperCase().equals("NN")
                    &&  relation.getGovernor().getLemma().equals(word)
                 && !this.getFeaturesSet().containsKey(relation.getDependent().getLemma())
                    && this.compareEquivalentRelations(ot1.getRelation(),relation.getRelation()) == true
                    ) {
                ot2.setOpinionLemma("none");
                ot2.setOpinionPosTag("none");
                ot2.setOpinionWord("none");
                ot2.setOpinionPosEnd(0);
                ot2.setOpinionPosBegin(0);
                ot2.setRelation(relation.getRelation());
                ot2.setTargetLemma(relation.getDependent().getLemma());
                ot2.setTargetPosTag(relation.getDependent().getPostag());
                ot2.setTargetPosEnd(relation.getDependent().getEnd());
                ot2.setTargetPosBegin(relation.getDependent().getStart());
                ot2.setTargetWord(relation.getDependent().getWordForm());
                ot2.setExtractedTarget(relation.getDependent().getWordForm());
                ot2.setId(relation.getTextId());
                ot2.setRule("R3_2");                
                relation.setProcessed(true);
                ot2.setSentence(relation.getSentence());
                return ot2;

            }
        }
        return null;

    }



    /**
     * R_4_1
     * Oi(j) →Oi(j)-Dep→Oj(i) s.t. Oj(i) ∈{O}, Oi(j)-Dep∈{CONJ},POS(Oi(j)) ∈{JJ}
     * @param relations
     * @return
     */
     private List<TargetOpinion> checkByRule_4_1() {
        TargetOpinion ot = new TargetOpinion();
         List<TargetOpinion> l = new ArrayList<>();
        for(Relations relation : this.relations.getRelations()) {
            if(     relation.getRelation().toUpperCase().equals("CONJ") &&
                    this.getOpinionSet().containsKey(relation.getGovernor().getLemma())  &&
                    relationsTypes.containsKey(relation.getRelation()) &&
                    (relation.getDependent().getPostag().toUpperCase().equals("JJ") ||  relation.getDependent().getPostag().toUpperCase().equals("JJS") || relation.getDependent().getPostag().toUpperCase().equals("JJR"))

              )
            {

                ot.setOpinionLemma(relation.getDependent().getLemma());
                ot.setOpinionPosTag(relation.getDependent().getPostag());
                ot.setOpinionWord(relation.getDependent().getWordForm());
                ot.setOpinionPosEnd(relation.getDependent().getEnd());
                ot.setOpinionPosBegin(relation.getDependent().getStart());
                ot.setRelation(relation.getRelation());
                ot.setTargetLemma("none");
                ot.setTargetPosTag("none");
                ot.setTargetPosEnd(0);
                ot.setTargetPosBegin(0);
                ot.setTargetWord("none");
                ot.setExtractedTarget("none");
                ot.setId(relation.getTextId());
                ot.setRule("R4_1");                
                relation.setProcessed(true);
                ot.setSentence(relation.getSentence());
                l.add(ot);
            }
        }
        return l;

    }

    /**
     * R_4_2
     * Oi →Oi-Dep→H←Oj-Dep←Oj s.t. Oi ∈ {O},Oi-Dep==Oj-Dep,POS(Oj) ∈{JJ}
     * @param relations
     * @return
     * TODO check for amod = pnmod (I don't know what's pnmod) and I nedd use this Oi-Dep == Oj-Dep
     */
    private List<TargetOpinion> checkByRule_4_2() {

        TargetOpinion ot1 = new TargetOpinion();
        TargetOpinion ot2 = new TargetOpinion();
        List<TargetOpinion> l = new ArrayList<>();

        for(Relations relation : this.relations.getRelations()) {

            if (( relation.getDependent().getPostag().toUpperCase().equals("JJ") ||
                  relation.getDependent().getPostag().toUpperCase().equals("JJS") ||
                  relation.getDependent().getPostag().toUpperCase().equals("JJR"))
                  && this.getOpinionSet().containsKey(relation.getDependent().getLemma())
                  && relationsTypes.containsKey(relation.getRelation())
                  && relation.getGovernor().getPostag().toUpperCase().equals("NN")

                ) {
                    ot1.setRelation(relation.getRelation());
                    ot1.setOpinionLemma(relation.getDependent().getLemma());
                    ot1.setOpinionWord(relation.getDependent().getWordForm());
                    ot1.setOpinionPosTag(relation.getDependent().getPostag());
                    ot1.setOpinionPosEnd(relation.getDependent().getEnd());
                    ot1.setOpinionPosBegin(relation.getDependent().getStart());
                    ot1.setExtractedOpinion(relation.getDependent().getWordForm());
                    ot1.setRule("R4_2");
                    ot1.setSentence(relation.getSentence());
                    for(Relations relation2 : this.relations.getRelations())
                   {
                        if (    this.compareEquivalentRelations(relation.getRelation(),relation2.getRelation()) == true
                                && ( relation2.getDependent().getPostag().toUpperCase().equals("JJ") ||  relation2.getDependent().getPostag().toUpperCase().equals("JJS") || relation2.getDependent().getPostag().toUpperCase().equals("JJR"))
                                &&  relation.getGovernor().getLemma().equals(relation2.getGovernor().getLemma())
                                && !this.getOpinionSet().containsKey(relation2.getDependent().getLemma())
                                )
                        {
                            ot2.setOpinionLemma(relation2.getDependent().getLemma());
                            ot2.setOpinionPosTag(relation2.getDependent().getPostag());
                            ot2.setOpinionWord(relation2.getDependent().getWordForm());
                            ot2.setOpinionPosEnd(relation2.getDependent().getEnd());
                            ot2.setOpinionPosBegin(relation2.getDependent().getStart());
                            ot2.setRelation(relation2.getRelation());
                            ot2.setTargetLemma("none");
                            ot2.setTargetPosTag("none");
                            ot2.setTargetPosEnd(0);
                            ot2.setTargetPosBegin(0);
                            ot2.setTargetWord("none");
                            ot2.setExtractedTarget("none");
                            ot2.setId(relation2.getTextId());
                            ot2.setRule("R4_2");                            
                            relation2.setProcessed(true);
                            ot2.setSentence(relation.getSentence());
                            l.add(ot2);

                        }
                    }
            }
        }





        return l;

    }

    private String getSteammer(String word)
    {
        this.stemmer.setCurrent(word.toLowerCase().trim().toLowerCase());
        this.stemmer.stem();
        return stemmer.getCurrent();
    }


    public void printDictionary(){
        System.out.println("Opinion");
        for (String key : this.getOpinionSet().keySet()) {
            System.out.println(key);
        }

        System.out.println("Feature");
        for (String key : this.getFeaturesSet().keySet()) {
            System.out.println(key);
        }
    }


    private Boolean compareEquivalentRelations(String R1, String R2)
    {
        if(R1.toUpperCase().equals(R2.toUpperCase()))
        {
            return true;
        }

        if ((R1.toUpperCase().equals("AMOD") && R2.toUpperCase().equals("NMOD")) || (R1.toUpperCase().equals("NMOD") && R2.toUpperCase().equals("AMOD")))
        {
            return true;
        }

        if ((R1.toUpperCase().equals("DOBJ") && R2.toUpperCase().equals("NSUBJ")) || (R1.toUpperCase().equals("NSUBJ") && R2.toUpperCase().equals("DOBJ")))
        {
            return true;
        }

        return false;

    }


    public Map<String, String> getOpinionSet() {
        return opinionSet;
    }

    public Map<String, String> getFeaturesSet() {
        return featuresSet;
    }

    public Map<String, TargetOpinion> getTargetAndOpinion() {
        return targetAndOpinion;
    }


    public void setFeaturesSet(Map<String, String> featuresSet) {
        this.featuresSet = featuresSet;
    }

    public void setOpinionSet(Map<String, String> opinionSet) {
        this.opinionSet = opinionSet;
    }

    public void setTargetAndOpinion(Map<String, TargetOpinion> targetAndOpinion) {
        this.targetAndOpinion = targetAndOpinion;
    }
}

