package com.diyaller.doublepropagation;

import com.diyaller.database.ReviewHotelAspect;
import com.diyaller.database.ReviewHotel;
import com.diyaller.database.ReviewRestaurantAspect;
import com.diyaller.database.ReviewRestaurant;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.CoreMap;
import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.EnglishStemmer;


import javax.persistence.EntityManager;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by bonesso on 02/11/15.
 *
 * POSTAGS STANFORD
 * CC Coordinating conjunction
 * CD Cardinal number
 * DT Determiner
 * EX Existential there
 * FW Foreign word
 * IN Preposition or subordinating conjunction
 * JJ Adjective
 * JJR Adjective, comparative
 * JJS Adjective, superlative
 * LS List item marker
 * MD Modal
 * NN Noun, singular or mass
 * NNS Noun, plural
 * NNP Proper noun, singular
 * NNPS Proper noun, plural
 * PDT Predeterminer
 * POS Possessive ending
 * PRP Personal pronoun
 * PRP$ Possessive pronoun
 * RB Adverb
 * RBR Adverb, comparative
 * RBS Adverb, superlative
 * RP Particle
 * SYM Symbol
 * TO to
 * UH Interjection
 * VB Verb, base form
 * VBD Verb, past tense
 * VBG Verb, gerund or present participle
 * VBN Verb, past participle
 * VBP Verb, non­3rd person singular present
 * VBZ Verb, 3rd person singular present
 * WDT Wh­determiner
 * WP Wh­pronoun
 * WP$ Possessive wh­pronoun
 * WRB Wh­adverb
 */



public class DoublePropagation {
    private StanfordCoreNLP pipeline;
    private int totalReviews  = 0;
    private Map<String, Integer> opinionExpand;
    private Map<String, Integer> featureExpand;
    private final TokenizerFactory<CoreLabel> tokenizerFactory;
    private Map<String, String> opinionDictionary;
    private Map<String, String> featureDictionary;
    private EntityManager em;

    RulesTarget rulesTarget;




    private int countRelation = 0;
    private SnowballProgram stemmer;

    /**
     * Some notes to understand Rules
     * M = modifier
     * NP = noum phrase
     * S = subject
     * P = predicate
     * O = object
     * @param em
     * Liu, B., & Street, S. M. (n.d.). Opinion Observer : Analyzing and Comparing Opinions on the Web, 342–351.
     */
    public DoublePropagation(EntityManager em){
             Properties props = new Properties();
             props.put("annotators", "tokenize, ssplit, pos, lemma, parse");
             //props.put("nthreads", "4");
             this.pipeline = new StanfordCoreNLP(props);
             opinionDictionary = new HashMap<String, String>();
             featureDictionary = new HashMap<String, String>();
             tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "invertible=true");
             this.stemmer = new EnglishStemmer();
             this.em = em;
             this.totalReviews = 0;
             this.countRelation =0;
             this.rulesTarget = new RulesTarget();
             //load dictionaries
             this.loadOpinion("dic_hotel_opinion.txt");
             this.loadTarget("dic_hotel_target.txt");
             this.rulesTarget.setFeaturesSet(this.featureDictionary);
             this.rulesTarget.setOpinionSet(this.opinionDictionary);
             System.out.println("Atention the  this.rulesTarget.getTargetAndOpinion().clear() need to be called after each processed review");

       }


    private String getSteammer(String word)
    {
        this.stemmer.setCurrent(word.toLowerCase().trim().toLowerCase());
        this.stemmer.stem();
        return stemmer.getCurrent();
    }

    private StanfordCoreNLP getPipeline() {
        return pipeline;
    }


    private void runDDPRules(AbstractReview review, String rule) {
        ListOfRelations relations = new ListOfRelations();
        Annotation document = new Annotation(review.getContent());
        getPipeline().annotate(document);
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        SimpleDateFormat ft = new SimpleDateFormat ("hhmmss");
        for (CoreMap sentence : sentences) {
            SemanticGraph dependencies = sentence.get(SemanticGraphCoreAnnotations.BasicDependenciesAnnotation.class);            
            if(!dependencies.getRoots().isEmpty()) {
                for (IndexedWord iw : dependencies.descendants(dependencies.getFirstRoot())) {
                    List<SemanticGraphEdge> incomingEdges = dependencies.getIncomingEdgesSorted(iw);
                    for (SemanticGraphEdge edge : incomingEdges) {
                        Date dNow = new Date( );
                        Relations r = new Relations();
                        NewWord lemma = new NewWord(review.getId(),iw.word(),getSteammer(iw.word()),iw.tag(),iw.beginPosition(),iw.endPosition());
                        NewWord dependent = new NewWord(review.getId(),edge.getDependent().word(),getSteammer(edge.getDependent().word()), edge.getDependent().tag(), edge.getDependent().beginPosition(), edge.getDependent().endPosition());
                        NewWord governor = new NewWord(review.getId(),edge.getGovernor().word(),getSteammer(edge.getGovernor().word()), edge.getGovernor().tag(), edge.getGovernor().beginPosition(), edge.getGovernor().endPosition());
                        NewWord target = new NewWord(review.getId(),edge.getTarget().word(),getSteammer(edge.getTarget().word()),edge.getTarget().tag(),edge.getTarget().beginPosition(),edge.getTarget().endPosition());
                        r.setIdRelation(ft.format(dNow).toString());
                        r.setLemma(lemma);
                        r.setDependent(dependent);
                        r.setGovernor(governor);
                        r.setRelation(edge.getRelation().getShortName());
                        r.setTarget(target);
                        r.setTextId(review.getId());
                        r.setTestRule(rule);
                        r.setSentence(sentence.toString());                 
                        relations.addRelation(r);
                        //System.out.println(" Lemma-> " + lemma.getWordForm() +  " dependent-> " + dependent.getWordForm() + "(" + dependent.getPostag() +  ")" + "<-" + edge.getRelation().getShortName() + "->" + " governor-> " + governor.getWordForm() + "(" + dependent.getPostag() +  ")"  );
                    }

                }

            }
        }

        this.rulesTarget.firstRules(relations);
        this.rulesTarget.secondRules(relations);
    }




    public void processReviewRestaurant(List<ReviewRestaurant> reviews){
        this.loadOpinion("dic_restaurant_opinion.txt");
        this.loadTarget("dic_restaurant_target.txt");
        AbstractReview review;
        this.totalReviews  =  reviews.size();
        //System.out.println("Start extraction... ");
        int j = 0;
        for(AbstractReview r : reviews){
            this.runDDPRules(r,"");
            j +=1;
            //System.out.println("Extraction  : " + j  + " to: " + reviews.size());
        }

        this.featureDictionary = this.rulesTarget.getFeaturesSet();
        this.opinionDictionary = this.rulesTarget.getOpinionSet();
        this.saveOpinion("dic_restaurant_opinion.txt");
        this.saveTarget("dic_restaurant_target.txt");
        this.saveRelation();
        this.saveToDataBaseRestaurantExtractedAspect();
        //System.out.println("End extraction... ");
    }

    public void processReviewHotels(List<ReviewHotel> reviews){

        this.totalReviews  =  reviews.size();
        //System.out.println("Start extraction... ");
        this.loadOpinion("dic_hotel_opinion.txt");
        this.loadTarget("dic_hotel_target.txt");
        this.rulesTarget.setFeaturesSet(this.featureDictionary);
        this.rulesTarget.setOpinionSet(this.opinionDictionary);
        int total = reviews.size();
        int j = 0,i = 0;

        for (ReviewHotel h :  reviews)
        {
                //System.out.println("Extraction  : " + j + " to: " + total + " review id:  " + h.getId());
                this.runDDPRules(h, "");
                j ++;
                i++;
                if (i==100){
                    this.featureDictionary = this.rulesTarget.getFeaturesSet();
                    this.opinionDictionary = this.rulesTarget.getOpinionSet();
                    this.saveOpinion("dic_hotel_opinion.txt");
                    this.saveTarget("dic_hotel_target.txt");
                    this.saveRelation();
                    this.saveToDataBaseHotelExtractedAspect();
                    this.featureDictionary.clear();
                    this.opinionDictionary.clear();
                    this.loadOpinion("dic_hotel_opinion.txt");
                    this.loadTarget("dic_hotel_target.txt");
                    this.rulesTarget.setFeaturesSet(this.featureDictionary);
                    this.rulesTarget.setOpinionSet(this.opinionDictionary);
                    this.rulesTarget.getTargetAndOpinion().clear();
                    i = 0;
                }

        }


    }

    public void processSigleReviewHotels(ReviewHotel review){
        //System.out.println("Start extraction... ");
        this.runDDPRules(review, "");
        this.saveToDataBaseHotelExtractedAspect();
    }
    
    
    public List<ReviewHotelAspect> processGetReviewHotels(ReviewHotel review){        
        this.runDDPRules(review, "");        
        List<ReviewHotelAspect> lstreviewAspect = new ArrayList<>();
        
        for(TargetOpinion t : this.rulesTarget.getTargetAndOpinion().values() ) {           
            ReviewHotelAspect reviewaspect = new ReviewHotelAspect();
            ReviewHotel r = this.em.find(ReviewHotel.class, t.getId());
            reviewaspect.setIdReviewHotel(r);
            reviewaspect.setLemmatarget(t.getTargetLemma());
            reviewaspect.setPostagtarget(t.getTargetPosTag());
            reviewaspect.setWordformtarget(t.getTargetWord());
            reviewaspect.setPosbegintarget(t.getTargetPosBegin());
            reviewaspect.setPosendtarget(t.getTargetPosEnd());
            reviewaspect.setLemmaopinion(t.getOpinionLemma());
            reviewaspect.setPostagopinion(t.getOpinionPosTag());
            reviewaspect.setWordformopinion(t.getOpinionWord());
            reviewaspect.setPosbeginopinion(t.getOpinionPosBegin());
            reviewaspect.setPosendopinion(t.getOpinionPosEnd());       
            reviewaspect.setSentece(t.getSentence());
            reviewaspect.setExtractrule(t.getRule());
            lstreviewAspect.add(reviewaspect);
        }
        this.rulesTarget.getTargetAndOpinion().clear();
        return lstreviewAspect;     
         
    }



    /*
    * Create to test phrases
    *
    * */

    public void processRulesMoc(){
        List<String> l =  new ArrayList<String>();
        l.add("I like Cafe Noir dont get me wrong, it is jsut that the people who work there are evil and incompetent!!");
        l.add("Great for large groups and celebrations - our SUPER HAPPY waiter was the entertainment of the evening.");

        int id_sentence = 0;
        ReviewRestaurant review;
        for(String r : l){
            review = new ReviewRestaurant();

            review.setId(Integer.toString(id_sentence));
            review.setContent(r);
            this.runDDPRules(review, "");
            id_sentence += 1;
        }
        for (TargetOpinion t :  this.rulesTarget.getTargetAndOpinion().values()){
            //System.out.println(t + " Relation Number : " + countRelation);
            this.countRelation = countRelation + 1;
        }
        //this.rulesTarget.printDictionary();
        this.saveRelation();
        this.saveOpinion("dic_moc_opinion.txt");
        this.saveTarget("dic_moc_target.txt");
    }


    /**
     * Sample phrases from paper.
     * Qiu, Guang and Liu, Bing and Bu, Jiajun and Chen, C. (2011). Opinion word expansion and target extraction through double propagation. Computational Linguistics, 37(1), 9–27. http://doi.org/10.1162/coli_a_00034
     *
     * **/
    public void processRulesMOC(){
        List<String> l =  new ArrayList<String>();
        List<String> rule =  new ArrayList<String>();
        this.opinionDictionary.put(getSteammer("good"), getSteammer("good"));
        l.add("The phone has a good screen.");
        rule.add("R1_1");
        this.opinionDictionary.put(getSteammer("best"), getSteammer("best"));
        l.add("Ipod is the best mp3 player.");
        rule.add("R1_2");
         l.add("The phone has a good screen.");
         rule.add("R2_1");
         l.add("Ipod is the best mp3 player.");
         rule.add("R2_2");
         l.add("Does the player play dvd with audio and video?");
         this.featureDictionary.put("audio", "audio");
        rule.add("R3_1");
         l.add("Canon G3 has a great lens.");
         this.featureDictionary.put(getSteammer("lens"), getSteammer("lens"));
         rule.add("R3_2");
         this.opinionDictionary.put(getSteammer("amazing"), getSteammer("amazing"));
         l.add("The camera is amazing and easy to use.");
         rule.add("R4_1");
         this.opinionDictionary.put(getSteammer("sexy"), getSteammer("sexy"));
         l.add("If you want to buy a sexy,cool, accessory-available mp3 player, you can choose iPod.");
         rule.add("R4_2");
         Integer id_sentence = 0;
         ReviewRestaurant review;
         for(String r : l){
            review = new ReviewRestaurant();
            review.setId(Integer.toString(id_sentence));
            review.setContent(r);
            this.runDDPRules(review,rule.get(id_sentence));
            id_sentence += 1;
        }
        for (TargetOpinion t :  this.rulesTarget.getTargetAndOpinion().values()){
            //System.out.println(t + " Relation Number : " + countRelation);
            this.countRelation = countRelation + 1;
        }

        this.rulesTarget.printDictionary();
        this.saveOpinion("dic_rulesMOC_opinion.txt");
        this.saveTarget("dic_rulesMOC_target.txt");
    }


    /**
     * Sample phrases from paper.
     * Qiu, Guang and Liu, Bing and Bu, Jiajun and Chen, C. (2011). Opinion word expansion and target extraction through double propagation. Computational Linguistics, 37(1), 9–27. http://doi.org/10.1162/coli_a_00034
     *
     * **/
    public void processPhrase(String phrase){

        this.loadOpinion("dic_restaurant_opinion.txt");
        this.loadTarget("dic_restaurant_target.txt");
        List<String> l =  new ArrayList<String>();
        List<String> rule =  new ArrayList<String>();

        l.add(phrase);



        Integer id_sentence = 0;
        ReviewRestaurant review;
        for(String r : l){
            review = new ReviewRestaurant();
            review.setId(Integer.toString(id_sentence));
            review.setContent(r);
            this.runDDPRules(review,"");
            id_sentence += 1;
        }


        this.printExtractedAspect();

    }

    private void loadOpinion(String dicName){

        String f = "/home/bonesso/Dropbox/dictionary/" + dicName;

        try {
            try (BufferedReader br = new BufferedReader(new FileReader(f))) {
                String line;
                while ((line = br.readLine()) != null) {
                    String s = line.substring(0,line.indexOf("->"));
                    this.opinionDictionary.put(getSteammer(s), line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void loadTarget(String dicName){

        String f = "/home/bonesso/Dropbox/dictionary/" + dicName;

        try {
            try (BufferedReader br = new BufferedReader(new FileReader(f))) {
                String line;
                while ((line = br.readLine()) != null) {
                    String s = line.substring(0,line.indexOf("->"));
                    this.featureDictionary.put(getSteammer(s), line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }



    }


    private void saveOpinion(String dictionary){
        File file = new File ("/home/bonesso/Dropbox/dictionary/new/" + dictionary);
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(file);
            for(String s : this.opinionDictionary.values() ) {
                        printWriter.println(s);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        printWriter.close ();

    }

    private void saveTarget(String dictionary){
        Date dNow = new Date( );
        File file = new File ("/home/bonesso/Dropbox/dictionary/new" + dictionary);
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(file);
            for(String s : this.featureDictionary.values() ) {
                        printWriter.println(s);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        printWriter.close ();

    }


    private void saveRelation(){

        File file = new File ("/home/com.diyaller/dictionary/relation.txt");
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(file);
            for(TargetOpinion s : this.rulesTarget.getTargetAndOpinion().values() ) {
                printWriter.println (s.toString() + "->" + s.getKey() );
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        printWriter.close();

    }





    private void printExtractedAspect(){




        for(TargetOpinion t : this.rulesTarget.getTargetAndOpinion().values() ) {
            //System.out.println("Rule : " + t.getRule() +  " Target : " + t.getTargetWord() + " at : " + t.getTargetPosBegin() + " " + t.getTargetPosEnd() + " Opinion : " + t.getOpinionWord() + " at : " + t.getOpinionPosBegin() + " " +  t.getOpinionPosEnd() );

        }

    }

    private void saveToDataBaseHotelExtractedAspect(){
        int i=0;
        for(TargetOpinion t : this.rulesTarget.getTargetAndOpinion().values() ) {
            ReviewHotelAspect reviewaspect = new ReviewHotelAspect();
            ReviewHotel r = this.em.find(ReviewHotel.class, t.getId());
            reviewaspect.setIdReviewHotel(r);
            reviewaspect.setLemmatarget(t.getTargetLemma());
            reviewaspect.setPostagtarget(t.getTargetPosTag());
            reviewaspect.setWordformtarget(t.getTargetWord());
            reviewaspect.setPosbegintarget(t.getTargetPosBegin());
            reviewaspect.setPosendtarget(t.getTargetPosEnd());
            reviewaspect.setLemmaopinion(t.getOpinionLemma());
            reviewaspect.setPostagopinion(t.getOpinionPosTag());
            reviewaspect.setWordformopinion(t.getOpinionWord());
            reviewaspect.setPosbeginopinion(t.getOpinionPosBegin());
            reviewaspect.setPosendopinion(t.getOpinionPosEnd());
            //reviewaspect.setExtractRule(t.getRule());
            this.em.getTransaction().begin();
            this.em.persist(reviewaspect);
            this.em.getTransaction().commit();
            i = i + 1;

        }
        this.rulesTarget.getTargetAndOpinion().clear();
        //System.out.println("Done saved data aspects:" + i );
    }


    private void saveToDataBaseRestaurantExtractedAspect(){
        //System.out.println("Saving data...");
        int i=0;
        for(TargetOpinion t : this.rulesTarget.getTargetAndOpinion().values() ) {
            ReviewRestaurantAspect reviewaspect = new ReviewRestaurantAspect();
            ReviewRestaurant r = this.em.find(ReviewRestaurant.class, t.getId());
            reviewaspect.setIdReviewRestaurant(r);
            reviewaspect.setLemmatarget(t.getTargetLemma());
            reviewaspect.setPostagtarget(t.getTargetPosTag());
            reviewaspect.setWordformtarget(t.getTargetWord());
            reviewaspect.setPosbegintarget(t.getTargetPosBegin());
            reviewaspect.setPosendtarget(t.getTargetPosEnd());
            reviewaspect.setLemmaopinion(t.getOpinionLemma());
            reviewaspect.setPostagopinion(t.getOpinionPosTag());
            reviewaspect.setWordformopinion(t.getOpinionWord());
            reviewaspect.setPosbeginopinion(t.getOpinionPosBegin());
            reviewaspect.setPosendopinion(t.getOpinionPosEnd());
            //reviewaspect.setExtractRule(t.getRule());
            this.em.getTransaction().begin();
            this.em.persist(reviewaspect);
            this.em.getTransaction().commit();
            i = i + 1;
            //System.out.println("Saving data... " + i);
        }
    }



}
