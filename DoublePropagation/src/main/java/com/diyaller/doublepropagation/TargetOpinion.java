package com.diyaller.doublepropagation;
/**
 * Created by dbonesso on 06/11/15.
 */
public class TargetOpinion {
    private String  rule;
    private String  targetLemma;
    private String  targetWord;
    private String targetPosTag;
    private int  targetPosBegin;
    private int  targetPosEnd;
    private String relation;
    private String  opinionLemma;
    private String  opinionWord;
    private String opinionPosTag;
    private int  opinionPosBegin;
    private int  opinionPosEnd;
    private Double opinionPolarity;
    private String extractedOpinion;
    private String extractedTarget;    
    private String id;
    private String key;
    private String sentence;


    public TargetOpinion(){
        this.rule ="none";
        this.targetLemma="none";
        this.extractedTarget ="none";
        this.relation="none";
        this.opinionLemma="none";
        this.extractedOpinion="none";
        this.id = "none";
        this.targetPosBegin =0;
        this.targetPosEnd =0;
        this.opinionPosBegin =0;
        this.opinionPosEnd=0;
        this.key = "";
        this.sentence ="none";



    }



    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getTargetLemma() {
        return targetLemma;
    }

    public void setTargetLemma(String targetLemma) {
        this.targetLemma = targetLemma;
    }

    public String getTargetWord() {
        return targetWord;
    }

    public void setTargetWord(String targetWord) {
        this.targetWord = targetWord;
    }

    public String getTargetPosTag() {
        return targetPosTag;
    }

    public void setTargetPosTag(String targetPosTag) {
        this.targetPosTag = targetPosTag;
    }

    public int getTargetPosBegin() {
        return targetPosBegin;
    }

    public void setTargetPosBegin(int targetPosBegin) {
        this.targetPosBegin = targetPosBegin;
    }

    public int getTargetPosEnd() {
        return targetPosEnd;
    }

    public void setTargetPosEnd(int targetPosEnd) {
        this.targetPosEnd = targetPosEnd;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getOpinionLemma() {
        return opinionLemma;
    }

    public void setOpinionLemma(String opinionLemma) {
        this.opinionLemma = opinionLemma;
    }

    public String getOpinionWord() {
        return opinionWord;
    }

    public void setOpinionWord(String opinionWord) {
        this.opinionWord = opinionWord;
    }

    public String getOpinionPosTag() {
        return opinionPosTag;
    }

    public void setOpinionPosTag(String opinionPosTag) {
        this.opinionPosTag = opinionPosTag;
    }

    public int getOpinionPosBegin() {
        return opinionPosBegin;
    }

    public void setOpinionPosBegin(int opinionPosBegin) {
        this.opinionPosBegin = opinionPosBegin;
    }

    public int getOpinionPosEnd() {
        return opinionPosEnd;
    }

    public void setOpinionPosEnd(int opinionPosEnd) {
        this.opinionPosEnd = opinionPosEnd;
    }

    public Double getOpinionPolarity() {
        return opinionPolarity;
    }

    public void setOpinionPolarity(Double opinionPolarity) {
        this.opinionPolarity = opinionPolarity;
    }

    @Override
    public String toString() {
        return "[rules:" + this.rule + ",target:" + this.targetLemma + ",Relation:" + this.relation + ",opinion:"  + this.opinionLemma + ";ID =" + "'" + this.getId() + "'"  +  "]";

    }

    public String getExtractedOpinion() {
        return extractedOpinion;
    }

    public void setExtractedOpinion(String extractedOpinion) {
        this.extractedOpinion = extractedOpinion;
    }

    public String getExtractedTarget() {
        return extractedTarget;
    }

    public void setExtractedTarget(String extractedTarget) {
        this.extractedTarget = extractedTarget;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getKey() {
        return this.getTargetLemma() + "_" + this.getTargetPosBegin() + "_" + this.getTargetPosEnd() + "_" + this.getOpinionLemma() + "_" + this.getOpinionPosBegin() + "_" + this.getOpinionPosEnd() + "_" + this.getId();
    }

    /**
     * @return the sentence
     */
    public String getSentence() {
        return sentence;
    }

    /**
     * @param sentence the sentence to set
     */
    public void setSentence(String sentence) {
        this.sentence = sentence;
    }
}
