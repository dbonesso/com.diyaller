package com.diyaller.doublepropagation;
import com.aylien.textapi.TextAPIClient;
import com.aylien.textapi.TextAPIException;
import com.aylien.textapi.parameters.AspectBasedSentimentParams;
import com.aylien.textapi.responses.Aspect;
import com.aylien.textapi.responses.AspectSentence;
import com.aylien.textapi.responses.AspectsSentiment;
import com.diyaller.database.ImportWangDataSet;
import com.diyaller.database.ReviewHotel;
import com.diyaller.database.ReviewHotelAspect;
import com.diyaller.database.Revieworacle;
import com.diyaller.database.RevieworaclePK;
import com.diyaller.database.Tripdata;

import com.diyaller.models.Review;
import com.diyaller.utils.ReadReviewJSON;
import edu.stanford.nlp.ling.Sentence;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;



public class BuildReviewConsultOracle{
    
        private static EntityManagerFactory factory;
        private static  EntityManager em;
         
	public static void main(String[] args)
	{
	               
                
                    
                            
                String dicOpinion = "/home/bonesso/Dropbox/dictionary/";
                String dicTarget  = "/home/bonesso/Dropbox/dictionary/";
                String wordNet    = "/home/bonesso/Dropbox/WordNet-3.0/";
                
                factory = Persistence.createEntityManagerFactory("SentiObserver");
                em = factory.createEntityManager();
                //DoublePropagation ddp = new DoublePropagation(em);
                Query query = em.createQuery("select s from Tripdata s where s.processed = FALSE");                
                List<Tripdata> lstReviews = query.getResultList();
                query.setMaxResults(1000);
               
               /* for (ReviewHotel review : lstReviews) {                    
                     List<ReviewHotelAspect> lstReviewHotelAspect = ddp.processGetReviewHotels(review);
                     for (ReviewHotelAspect reviewaspect : lstReviewHotelAspect) {
                              
                           RevieworaclePK ropk =  new RevieworaclePK();
                           ropk.setId(reviewaspect.getIdReviewHotel().getId());
                           ropk.setHotel(reviewaspect.getIdReviewHotel().getMerchantid().getMerchantid());
                           ropk.setAuthor(reviewaspect.getIdReviewHotel().getAuthor());
                           ropk.setFreetext(reviewaspect.getSentece());
                           ropk.setAspect(reviewaspect.getWordformtarget());
                           
                           Revieworacle rocheck = em.find(Revieworacle.class, ropk);
                           if (rocheck == null){
                               Revieworacle ro = new Revieworacle();
                               ro.setRevieworaclePK(ropk);
                               ro.setPolarity(0.0);
                               ro.setOpinion(reviewaspect.getWordformopinion());
                           
                               System.out.println(reviewaspect.getSentece());
                               System.out.println(reviewaspect.getWordformtarget());
                               System.out.println(reviewaspect.getWordformopinion());
                               System.out.println(reviewaspect.getExtractrule());
                               System.out.println("*******************************");
                                                     
                               //ro.setOpinion(reviewaspect.getWordformopinion());
                               //ro.setPolarity(0.0);
                               em.getTransaction().begin();
                               em.persist(ro);
                               em.getTransaction().commit();
                           }
                           
                           
                    }
                }*/
                
                TextAPIClient client = new TextAPIClient("bb5a70bb", "1c0eea5e4a316311ef57776874bbe847");       
                AspectBasedSentimentParams.Builder builder = AspectBasedSentimentParams.newBuilder();
                builder.setDomain(AspectBasedSentimentParams.StandardDomain.HOTELS);
                AspectsSentiment aspectsSentiment;
                
                int i =0;
                for (Tripdata review : lstReviews) {                    
                    builder.setText(review.getFreetext());
                    try {
                        aspectsSentiment = client.aspectBasedSentiment(builder.build());
                        
                        try {
                            TimeUnit.MILLISECONDS.sleep(1000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(BuildReviewConsultOracle.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        int k = aspectsSentiment.getSentences().length;
                        if (k+i>=1000){
                           System.out.print("API FINISHED");
                           break;
                        }
                        
                        em.getTransaction().begin();
                        for (AspectSentence s : aspectsSentiment.getSentences()) {
                             i=i+1;         
                           
                            if (s.getAspects().length > 0) 
                            {
                                for(Aspect a : s.getAspects())
                                {
                                 
                                 RevieworaclePK ropk =  new RevieworaclePK();
                                 ropk.setId(review.getId());
                                 ropk.setHotel(review.getHotel());
                                 ropk.setAuthor(review.getAuthor());
                                 ropk.setSentence(s.getText());
                                 ropk.setAspect(a.getName());                      
                                 Revieworacle rocheck = em.find(Revieworacle.class, ropk);
                                 if (rocheck == null ){
                                     Revieworacle ro = new Revieworacle();
                                     ro.setRevieworaclePK(ropk);                                                                          
                                     ro.setAspectconfidence(a.getAspectConfidence());                          
                                     ro.setPolarity(a.getPolarity());
                                     ro.setPolarityconfidence(a.getAspectConfidence());                                                                                         
                                     em.persist(ro);
                                    
                                 }
                                 
                                }
                            }else{
                            
                                 RevieworaclePK ropk =  new RevieworaclePK();
                                 ropk.setId(review.getId());
                                 ropk.setHotel(review.getHotel());
                                 ropk.setAuthor(review.getAuthor());
                                 ropk.setSentence(s.getText());
                                 ropk.setAspect("none");                      
                                 Revieworacle rocheck = em.find(Revieworacle.class, ropk);
                                 if (rocheck == null ){
                                     Revieworacle ro = new Revieworacle();
                                     ro.setRevieworaclePK(ropk);
                                     ro.setAspectconfidence(0.0);                          
                                     ro.setPolarity(s.getPolarity());
                                     ro.setPolarityconfidence(s.getPolarityConfidence());                                                     
                                     em.persist(ro);
                                     
                            
                                  }
                            }
                            
                        }
                                            
                        Tripdata tp = em.find(Tripdata.class, review.getId());
                        tp.setProcessed(Boolean.TRUE);
                        
                        em.persist(tp);
                        em.getTransaction().commit();                                    
                        System.out.println("Another saved" + review.getId());
                                 
                        
                      
                        
                        
                    } catch (TextAPIException ex) {
                        Logger.getLogger(BuildReviewConsultOracle.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("Error" + review.getId());
                     
                        
                        break;
                    }
                    
                    
            
                }
                
                System.out.println("Extraction finished");
	}
        
        
        
        
        
        
}