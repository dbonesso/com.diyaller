package com.diyaller.doublepropagation;

/**
 * Created by bonesso on 06/11/15.
 */
public class NewWord {
    private String textId;
    private String wordForm;
    private String lemma;
    private String postag;
    private Integer start;
    private Integer end;
    private String wordKey;

    /**
     *
     * @param textId
     * @param wordForm
     * @param lemma
     * @param postag
     * @param start
     * @param end
     */
    public NewWord(String textId, String wordForm, String lemma, String postag, Integer start, Integer end){
        this.textId =  textId;
        this.wordForm = wordForm;
        this.lemma = lemma;
        this.postag = postag;
        this.start = start;
        this.end = end;
    }

    public NewWord(){

    }

    public String getTextId() {
        return textId;
    }

    public void setTextId(String textId) {
        this.textId = textId;
    }

    public String getWordForm() {
        return wordForm;
    }

    public void setWordForm(String wordForm) {
        this.wordForm = wordForm;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getPostag() {
        return postag;
    }

    public void setPostag(String postag) {
        this.postag = postag;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public String getWordKey() {
        return  this.lemma.concat("_").concat(Integer.toString(this.start)).concat("_").concat(Integer.toString(this.end));

    }



}
