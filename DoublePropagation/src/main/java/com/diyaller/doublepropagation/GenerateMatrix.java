package com.diyaller.doublepropagation;

import com.diyaller.doublepropagation.DoublePropagation;
import com.diyaller.utils.BuildAspectHotelTable;
import com.diyaller.database.ReviewHotel;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by bonesso on 02/11/15.
 */
public class GenerateMatrix {

    private static final String PERSISTENCE_UNIT_NAME_1 = "TripAdvisorPU";
    private static final String PERSISTENCE_UNIT_NAME_2 = "ReviewPU";
    private static EntityManagerFactory factory;
    private BuildAspectHotelTable build;
    private static  EntityManager em;
    private DoublePropagation ddp;
    private int registerCount;
    private long begin = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    private long end = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());

    public GenerateMatrix(){
        factory = Persistence.createEntityManagerFactory("SentiObserver");
        em = factory.createEntityManager();
        ddp = new DoublePropagation(em);
        build = new BuildAspectHotelTable(em);
        this.registerCount =0;
    }


    public int buildMatrix(String id){
        Query query = em.createQuery("select s from ReviewHotel s where s.id= :id");
        ReviewHotel review = (ReviewHotel) query.setParameter("id",id).getSingleResult();
        
        
        System.out.println("Start DDP");
        begin = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        ddp.processSigleReviewHotels(review);
        end = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());        
        System.out.println("Time:" + (end-begin) );
        


        System.out.println("Generate Row of Table Frequency/Polarity");
        begin = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        build.generateTableSingleReview(review);
        end = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        System.out.println("Time:" + (end-begin) );
        
        registerCount = registerCount+1;
        System.out.println("Number of register: " + registerCount);
        
        
        
        
        
        return 1;
    }


    public void testBuildMatrix(){

        Query query = em.createQuery("SELECT s.merchantHotelByMerchantid.merchantid FROM ReviewHotelEntity s  WHERE  s.author IN (SELECT t.author FROM  ReviewHotelEntity t WHERE length(t.title)>0 and length(t.content) >= 50 and  length(t.content) <= 1500   group by t.author HAVING count(t.author) >=4 and count(t.author) <= 30 )  group by s.merchantHotelByMerchantid.merchantid");
        List<String> merchantid =  query.getResultList();
        int i = 0;

        
        for(String id : merchantid){
                  query = em.createQuery("SELECT s  FROM ReviewHotelEntity s  WHERE   length(s.title)>0 and length(s.content) >= 50  and  length(s.content) <= 1500 and  s.merchantHotelByMerchantid.merchantid = :id");
                  List<ReviewHotel>  reviews =  query.setParameter("id",id).getResultList();
                  for(ReviewHotel  r : reviews){
                            System.out.println("Start DDP");
                            begin = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                            System.out.println(r.getId());
                            ddp.processSigleReviewHotels(r);
                            end = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                            System.out.println("Time:" + (end-begin) );

                            System.out.println("Generate Row of Table Frequency/Polarity");
                            begin = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                            build.generateTableSingleReview(r);
                            end = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                            System.out.println("Time:" + (end-begin) );
                            i += 1;
                   }
        }
        System.out.println(i);

       
    }

    

}

