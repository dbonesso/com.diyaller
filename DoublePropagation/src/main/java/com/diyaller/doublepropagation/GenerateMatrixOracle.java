package com.diyaller.doublepropagation;

import com.diyaller.database.MatrixHotelAspect;
import com.diyaller.database.MatrixOracle;
import com.diyaller.database.RatingHotel;
import com.diyaller.database.ReviewHotel;
import com.diyaller.doublepropagation.DoublePropagation;
import com.diyaller.utils.BuildAspectHotelTable;
import com.diyaller.database.Revieworacle;
import com.diyaller.database.Tripdata;
import com.diyaller.utils.SentiWordNet;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;
import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.EnglishStemmer;

/**
 * Created by bonesso on 02/11/15.
 */
public class GenerateMatrixOracle {
    private Map<String, String> mapAspects;
    private Map<String, Double> profile;
    private Map<String, Double> profilepolarity;
    private Map<String, Double> aspects;
    private Map<String, Double> aspectspolarity;    
    private int countReviews;    
    private static EntityManagerFactory factory;    
    private static  EntityManager em;
    
   
    
    
    
    public GenerateMatrixOracle()
    {
            
            this.countReviews =0;            
            this.profile =  new HashMap<String,Double>();
            this.profilepolarity = new HashMap<String,Double>();
            this.aspects =  new HashMap<String,Double>();
            this.aspectspolarity = new HashMap<String,Double>();
            this.mapAspects = new HashMap<String, String>();             
               
            factory = Persistence.createEntityManagerFactory("SentiObserver");
            em = factory.createEntityManager();            
            mapAspects.put("beds","sleep");         
            mapAspects.put("food/drinks","food");        
            mapAspects.put("location", "location");
            mapAspects.put("room amenities","room");        
            mapAspects.put("cleanliness","cleanliness");
            mapAspects.put("customer support","service");
            mapAspects.put("service","service");
            mapAspects.put("comfort","room");
            mapAspects.put("wifi",  "facility"); 
            mapAspects.put("payment", "value");           
            mapAspects.put("facilities",  "facility"); 
            mapAspects.put("value", "value");  
            mapAspects.put("staff","service");
            mapAspects.put("quietness","cleanliness");
            mapAspects.put("view",  "facility"); 
            mapAspects.put("design",  "facility");      
            mapAspects.put("none",  "none");      
         
    }
    
        
    
     public void calculateRowOfW(){



        Query query = em.createQuery("select c from Tripdata c where c.processed=TRUE");
        List<Tripdata> lreviews = query.getResultList();
        

        for (Tripdata t : lreviews) {            
            
            Query subquery = em.createQuery("select c from Revieworacle c where c.revieworaclePK.id = :idr");
            List<Revieworacle> laspects = subquery.setParameter("idr",t.getId()).getResultList();
            
            for(Revieworacle a : laspects){
                
                String aspectGroup = this.mapAspects.get(a.getRevieworaclePK().getAspect());
                System.out.println(aspectGroup);
                
                 if (this.aspects.containsKey(aspectGroup)) {
                    double i = this.aspects.get(aspectGroup);
                    this.aspects.replace(aspectGroup, i, i + 1.0);
                    double j = this.aspectspolarity.get(aspectGroup);
                    switch (a.getPolarity()) {
                        case "positive":
                            this.aspectspolarity.replace(aspectGroup, j,j + 1.0);
                            break;
                        case "negative":
                            this.aspectspolarity.replace(aspectGroup, j,j - 1.0);
                            break;
                        case "neutral":
                            this.aspectspolarity.replace(aspectGroup, j,j + 0.0);  
                            break;
                        default:
                            break;
                    }
                    
                } else {
                      this.aspects.put(aspectGroup, 1.0);
                      
                      switch (a.getPolarity()) {
                        case "positive":
                            this.aspectspolarity.put(aspectGroup, 1.0);
                            break;
                        case "negative":
                            this.aspectspolarity.put(aspectGroup,1.0);
                            break;
                        case "neutral":
                            this.aspectspolarity.put(aspectGroup,0.0);  
                            break;
                        default:
                            break;
                    }
                }
                
            }
            saveMatrixW(t);
            
            
            

        }

      
    }
    
    
     private void saveMatrixW(Tripdata t){

        double value =0;
        double location=0;
        double room=0;
        double cleanliness=0;
        double sleep =0;
        double service=0;
        double facility=0;
        double food=0;



        double valuepolarity =0;
        double locationpolarity=0;
        double roompolarity=0;
        double cleanlinesspolarity=0;
        double sleeppolarity =0;
        double servicepolarity=0;
        double facilitypolarity=0;
        double foodpolarity=0;

        double overall =   0;
        double total = 0;
        double countAspect = 0;


        this.countReviews +=1;
        for (String s : this.aspects.keySet()) {
            switch (s){
                case "value":
                    value = this.aspects.get(s);
                    valuepolarity = this.aspectspolarity.get(s);
                    countAspect +=1;
                    break;
                case "location":
                    location = this.aspects.get(s);
                    locationpolarity = this.aspectspolarity.get(s);
                    countAspect +=1;
                    break;
                case "room":
                    room = this.aspects.get(s);
                    roompolarity = this.aspectspolarity.get(s);
                    countAspect +=1;
                    break;
                case "cleanliness":
                    cleanliness = this.aspects.get(s);
                    cleanlinesspolarity = this.aspectspolarity.get(s);
                    countAspect +=1;
                    break;
                case "sleep":
                    sleep = this.aspects.get(s);
                    sleeppolarity = this.aspectspolarity.get(s);
                    countAspect +=1;
                    break;
                case "service":
                    service = this.aspects.get(s);
                    servicepolarity = this.aspectspolarity.get(s);
                    countAspect +=1;
                    break;
                case "food":
                    food = this.aspects.get(s);
                    facilitypolarity = this.aspectspolarity.get(s);
                    countAspect +=1;
                    break;
                case "facility":
                    facility = this.aspects.get(s);
                    foodpolarity = this.aspectspolarity.get(s);
                    countAspect +=1;
                    break;
            }


        }


        Query query = em.createQuery("select c as total from RatingHotel c where c.idReview.id = :idr");
        query.setParameter("idr",t.getId());
        RatingHotel ratings = (RatingHotel)query.getSingleResult();
        total = cleanliness + food + facility + location + room + service + value +  sleep;
        if (total == 0)
            total =1;
        if(countAspect > 0) {
            overall = (valuepolarity + locationpolarity + roompolarity + cleanlinesspolarity + sleeppolarity + servicepolarity + facilitypolarity + foodpolarity) / countAspect;
        }else
        {
            overall=0;
        }




        MatrixOracle matrixReview = new MatrixOracle();
        matrixReview.setId(t.getId());
        matrixReview.setHotel(t.getHotel());
        matrixReview.setAuthor(t.getAuthor());
        matrixReview.setAspectcleanliness(Double.isNaN(cleanliness ) ? 0.0 : cleanliness);
        matrixReview.setCleanlinesspolarity(Double.isNaN(cleanlinesspolarity) ? 0.0 : cleanlinesspolarity);
        matrixReview.setAspectfacility(Double.isNaN(food) ? 0.0 : food);
        matrixReview.setFacilitypolarity(Double.isNaN(facilitypolarity) ? 0.0 : facilitypolarity);
        matrixReview.setAspectfoodquality(Double.isNaN(facility) ? 0.0 :  facility);
        matrixReview.setFoodqualitypolarity(Double.isNaN(foodpolarity) ? 0.0 : foodpolarity);
        matrixReview.setAspectlocation(Double.isNaN(location) ? 0.0 :  location);
        matrixReview.setLocationpolarity(Double.isNaN(locationpolarity) ? 0.0 : locationpolarity);
        matrixReview.setAspectroom(Double.isNaN(room) ? 0.0 : room);
        matrixReview.setRoompolarity(Double.isNaN(roompolarity) ? 0.0 : roompolarity);
        matrixReview.setAspectservice(Double.isNaN(service) ? 0.0 : service);
        matrixReview.setServicepolarity(Double.isNaN(servicepolarity) ? 0.0 : servicepolarity);
        matrixReview.setAspectvalue(Double.isNaN(value) ? 0.0 : value);
        matrixReview.setValuepolarity(Double.isNaN(valuepolarity) ? 0.0 : valuepolarity);
        matrixReview.setAspectsleepquality(Double.isNaN(sleep) ? 0.0 : sleep);
        matrixReview.setSleepqualitypolarity(Double.isNaN(sleeppolarity) ? 0.0 : sleeppolarity);
        matrixReview.setOverallpolarity(Double.isNaN(overall) ? 0.0 : overall);
        matrixReview.setOverall(ratings.getOverall());
        

        this.em.getTransaction().begin();
        this.em.persist(matrixReview);
        this.em.getTransaction().commit();
        this.aspects.clear();
        this.aspectspolarity.clear();
        //System.out.println("Saved reviews in matrix: "  + countReviews);

    }
   
   

    

   

    

}

