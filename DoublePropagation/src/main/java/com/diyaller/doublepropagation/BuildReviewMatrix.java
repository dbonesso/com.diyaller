package com.diyaller.doublepropagation;
import com.diyaller.database.ImportWangDataSet;
import com.diyaller.database.ReviewHotel;

import com.diyaller.models.Review;
import com.diyaller.utils.ReadReviewJSON;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;



public class BuildReviewMatrix{
    
        private static EntityManagerFactory factory;
        private static  EntityManager em;
         
	public static void main(String[] args)
	{
	
           
                
                
                // Construct BufferedReader from FileReader
                //List<String> s = new ArrayList<String>();
                //BufferedReader br = null;
                //try {
                //    br = new BufferedReader(new FileReader("/home/bonesso/Dropbox/DDP/data.csv"));
                //} catch (FileNotFoundException ex) {
                //    Logger.getLogger(BuildReviewMatrix.class.getName()).log(Level.SEVERE, null, ex);
                //}
                
                
                
                //String line = null;
                //while ((line = br.readLine()) != null) {                    
                //   s.add(line);
                //   System.out.println(line);
                //}
                
                //br.close();               
                            
                String dicOpinion = "/home/bonesso/Dropbox/dictionary/";
                String dicTarget  = "/home/bonesso/Dropbox/dictionary/";
                String wordNet    = "/home/bonesso/Dropbox/WordNet-3.0/";
                GenerateMatrix g =  new GenerateMatrix();
                factory = Persistence.createEntityManagerFactory("SentiObserver");
                em = factory.createEntityManager();
                Query query = em.createQuery("select s from ReviewHotel s ");
                query.setMaxResults(10);
                List<ReviewHotel> lstReviews = query.getResultList();
                for (ReviewHotel review : lstReviews) {
                    g.buildMatrix(review.getId());
                }
                System.out.println("Extraction finished");
	}
        
        
        
        
        
        
}