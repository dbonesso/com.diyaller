package com.diyaller.doublepropagation;

/**
 * Class that abstracts the relations (dependency) between words, provided by a
 * parsing tool. This way the rest of the code can be tool-agnostic. The only
 * tool-dependent code should be in the mapper that fills this structure from
 * the actual tool result
 * 
 * @author Bonesso
 * 
 */
public class Relations {
    private String textId;
	private String idRelation;
	private NewWord lemma;
	private String relation;
	private NewWord target;
	private NewWord dependent;
	private NewWord governor;
	private boolean processed;
	private String testRule;
        private String sentence;

	public Relations(){
		this.textId = "none";
		this.idRelation ="none";
		this.lemma = new NewWord("none", "none", "none", "none", 0, 0);
		this.relation = "";
		this.target = new NewWord("none", "none", "none", "none", 0, 0);
		this.dependent = new NewWord("none", "none", "none","none", 0, 0);
		this.governor = new NewWord("none", "none", "none", "none", 0, 0);
                this.sentence = "";

	}


	public void copyRelations(Relations relations){
		this.textId = relations.textId;
		this.idRelation = relations.idRelation;
		this.lemma = relations.lemma;
		this.relation = relations.relation;
		this.target = relations.target;
		this.dependent = relations.dependent;
		this.governor =  relations.governor;
                this.sentence = relations.getSentence();
	}


	public String getTextId() {
		return textId;
	}

	public void setTextId(String textId) {
		this.textId = textId;
	}

	public NewWord getLemma() {
		return lemma;
	}

	public void setLemma(NewWord lemma) {
		this.lemma = lemma;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public NewWord getTarget() {
		return target;
	}

	public void setTarget(NewWord target) {
		this.target = target;
	}

	public NewWord getDependent() {
		return dependent;
	}

	public void setDependent(NewWord dependent) {
		this.dependent = dependent;
	}

	public NewWord getGovernor() {
		return governor;
	}

	public void setGovernor(NewWord governor) {
		this.governor = governor;
	}
	@Override
	public String toString(){
			return "[RID:" + this.idRelation + ",Governor:" + this.getGovernor().getWordForm() + ",Dependent:" +  this.getDependent().getWordForm() + ",Relation:" + this.getRelation() + ",Target:"  + this.getTarget().getWordForm() + "; processed:" + this.isProcessed() + ";Test Rule:" + this.getTestRule() + "]";
	}

	public String getIdRelation() {
		return idRelation;
	}

	public void setIdRelation(String idRelation) {
		this.idRelation = idRelation;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	public String getWordKey() {
		return  this.lemma.getWordForm().concat("_").concat(Integer.toString(this.lemma.getStart())).concat("_").concat(Integer.toString(this.lemma.getEnd()));

	}


	public boolean isSameWordInTheSentenceThan(Relations anotherWord){

		return this.getWordKey().equals(anotherWord.getWordKey());
	}

	public boolean isPreviousWordInTheSentenceThan(Relations anotherWord) {
		return   anotherWord.getLemma().getStart() > this.getLemma().getEnd();
	}


	public String getTestRule() {
		return testRule;
	}

	public void setTestRule(String testRule) {
		this.testRule = testRule;
	}

    /**
     * @return the sentence
     */
    public String getSentence() {
        return sentence;
    }

    /**
     * @param sentence the sentence to set
     */
    public void setSentence(String sentence) {
        this.sentence = sentence;
    }
}