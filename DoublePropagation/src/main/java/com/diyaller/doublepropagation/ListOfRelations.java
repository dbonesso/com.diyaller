package com.diyaller.doublepropagation;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bonesso on 10/11/15.
 */
public class ListOfRelations {
    private List<Relations> relations;
    private Multimap<String,Relations> governorWordRelationMap;
    private Multimap<String, Relations> dependentWordRelationMap;



    public ListOfRelations(){
        this.governorWordRelationMap  = ArrayListMultimap.create();
        this.dependentWordRelationMap =  ArrayListMultimap.create();
        this.relations = new ArrayList<>();
    }

    public List<Relations> getRelations() {
        return relations;
    }

    public void setRelations(List<Relations> relations) {
        this.relations = relations;
    }

    public void addRelation(Relations relation){

            governorWordRelationMap.put(relation.getWordKey(),relation);
            dependentWordRelationMap.put(relation.getWordKey(),relation);

            //special case, the list is empty
            if(getRelations().isEmpty()){
                getRelations().add(relation);
            }else{
                //general case, look for the first word that goes after the word we want to add, and add at that point
                boolean added=false;
                boolean theVerySameWordAlreadyInTheList=false;
                for(int i=0;i< getRelations().size();i++){
                    //Relations iesimWord= getRelations().get(i);
                   // if(relation.isSameWordInTheSentenceThan(iesimWord)){
                        //if is the same word, because it participates in many relations as the governor (i.e. the beautiful eye -> the->eye, beautiful->eye)
                        //the break, it must not be added again
                     //   theVerySameWordAlreadyInTheList=true;
                    //    break;
                    //}else if(relation.isPreviousWordInTheSentenceThan(iesimWord)){
                        getRelations().add(i, relation);
                        added=true;
                        break;
                   // }
                }
                //special case, the word is the last of the current list
                boolean weHaveBrokenTheLoopOnPurpose=added || theVerySameWordAlreadyInTheList;
                if(!weHaveBrokenTheLoopOnPurpose){
                    getRelations().add(relation);
                }
            }

    }









}
