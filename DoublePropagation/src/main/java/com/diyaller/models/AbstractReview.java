package com.diyaller.models;


/**
 * Created by bonesso on 14/12/15.
 */
public interface  AbstractReview {

     String getTitle();
     void setTitle(String title);
     String getAuthor();
     void setAuthor(String author);
     String getContent();
     void setContent(String content);
     String getDate() ;
     void setDate(String date);
     String getId();
     void setId(String id);

}
