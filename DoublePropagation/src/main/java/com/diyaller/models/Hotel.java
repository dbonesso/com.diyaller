package com.diyaller.models;
import java.util.ArrayList;
import java.util.List;

public class Hotel {
	
	public Hotel(){
		this.hotelAspects = new ArrayList<Aspect>();
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPolarityValue() {
		return polarityValue;
	}
	public void setPolarityValue(Double polarityValue) {
		this.polarityValue = polarityValue;
	}
	public Double getPolarityLocation() {
		return polarityLocation;
	}
	public void setPolarityLocation(Double polarityLocation) {
		this.polarityLocation = polarityLocation;
	}
	public Double getPolarityCleanliness() {
		return polarityCleanliness;
	}
	public void setPolarityCleanliness(Double polarityCleanliness) {
		this.polarityCleanliness = polarityCleanliness;
	}
	public Double getPolaritySleepQuality() {
		return polaritySleepQuality;
	}
	public void setPolaritySleepQuality(Double polaritySleepQuality) {
		this.polaritySleepQuality = polaritySleepQuality;
	}
	public Double getPolarityService() {
		return polarityService;
	}
	public void setPolarityService(Double polarityService) {
		this.polarityService = polarityService;
	}
	public Double getPolarityAmenities() {
		return polarityAmenities;
	}
	public void setPolarityAmenities(Double polarityAmenities) {
		this.polarityAmenities = polarityAmenities;
	}
	public Double getPolarityFood() {
		return polarityFood;
	}
	public void setPolarityFood(Double polarityFood) {
		this.polarityFood = polarityFood;
	}
	public List<Aspect> getHotelAspects() {
		return hotelAspects;
	}
	public void setHotelAspects(List<Aspect> hotelAspects) {
		this.hotelAspects = hotelAspects;
	}
	private String id;
	   private String name;	   
	   private Double polarityValue;   
	   private Double polarityLocation;	   
	   private Double polarityCleanliness;	   
	   private Double polaritySleepQuality;	   
	   private Double polarityService;   
	   private Double polarityAmenities;   
	   private Double polarityFood;
	   private List<Aspect> hotelAspects;
	   
	   

}
