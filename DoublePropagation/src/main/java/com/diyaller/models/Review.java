package com.diyaller.models;

import java.util.ArrayList;
import java.util.List;

public class Review {
	
   public List<Aspect> getAspect() {
		return aspect;
	}

	public void setAspect(List<Aspect> aspect) {
		this.aspect = aspect;
	}

public Review(){
	   this.hotel = new Hotel();
	   this.aspect =  new ArrayList<Aspect>();
   }	
		
   public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	public int getFreqAspectValue() {
		return freqAspectValue;
	}
	public void setFreqAspectValue(int freqAspectValue) {
		this.freqAspectValue = freqAspectValue;
	}
	public Double getPolarityValue() {
		return polarityValue;
	}
	public void setPolarityValue(Double polarityValue) {
		this.polarityValue = polarityValue;
	}
	public Double getWeightValue() {
		return weightValue;
	}
	public void setWeightValue(Double weightValue) {
		this.weightValue = weightValue;
	}
	public int getFreqAspectLocation() {
		return freqAspectLocation;
	}
	public void setFreqAspectLocation(int freqAspectLocation) {
		this.freqAspectLocation = freqAspectLocation;
	}
	public Double getPolarityLocation() {
		return polarityLocation;
	}
	public void setPolarityLocation(Double polarityLocation) {
		this.polarityLocation = polarityLocation;
	}
	public Double getWeightLocation() {
		return weightLocation;
	}
	public void setWeightLocation(Double weightLocation) {
		this.weightLocation = weightLocation;
	}
	public int getFreqAspectCleanliness() {
		return freqAspectCleanliness;
	}
	public void setFreqAspectCleanliness(int freqAspectCleanliness) {
		this.freqAspectCleanliness = freqAspectCleanliness;
	}
	public Double getPolarityCleanliness() {
		return polarityCleanliness;
	}
	public void setPolarityCleanliness(Double polarityCleanliness) {
		this.polarityCleanliness = polarityCleanliness;
	}
	public Double getWeightCleanliness() {
		return weightCleanliness;
	}
	public void setWeightCleanliness(Double weightCleanliness) {
		this.weightCleanliness = weightCleanliness;
	}
	public int getFreqAspectSleepQuality() {
		return freqAspectSleepQuality;
	}
	public void setFreqAspectSleepQuality(int freqAspectSleepQuality) {
		this.freqAspectSleepQuality = freqAspectSleepQuality;
	}
	public Double getPolaritySleepQuality() {
		return polaritySleepQuality;
	}
	public void setPolaritySleepQuality(Double polaritySleepQuality) {
		this.polaritySleepQuality = polaritySleepQuality;
	}
	public Double getWeightSleepQuality() {
		return weightSleepQuality;
	}
	public void setWeightSleepQuality(Double weightSleepQuality) {
		this.weightSleepQuality = weightSleepQuality;
	}
	public int getFreqAspectService() {
		return freqAspectService;
	}
	public void setFreqAspectService(int freqAspectService) {
		this.freqAspectService = freqAspectService;
	}
	public Double getPolarityService() {
		return polarityService;
	}
	public void setPolarityService(Double polarityService) {
		this.polarityService = polarityService;
	}
	public Double getWeightService() {
		return weightService;
	}
	public void setWeightService(Double weightService) {
		this.weightService = weightService;
	}
	public int getFreqAspectAmenities() {
		return freqAspectAmenities;
	}
	public void setFreqAspectAmenities(int freqAspectAmenities) {
		this.freqAspectAmenities = freqAspectAmenities;
	}
	public Double getPolarityAmenities() {
		return polarityAmenities;
	}
	public void setPolarityAmenities(Double polarityAmenities) {
		this.polarityAmenities = polarityAmenities;
	}
	public Double getWeightAmenities() {
		return weightAmenities;
	}
	public void setWeightAmenities(Double weightAmenities) {
		this.weightAmenities = weightAmenities;
	}
	public int getFreqAspectFood() {
		return freqAspectFood;
	}
	public void setFreqAspectFood(int freqAspectFood) {
		this.freqAspectFood = freqAspectFood;
	}
	public Double getPolarityFood() {
		return polarityFood;
	}
	public void setPolarityFood(Double polarityFood) {
		this.polarityFood = polarityFood;
	}
	public Double getWeightFood() {
		return weightFood;
	}
	public void setWeightFood(Double weightFood) {
		this.weightFood = weightFood;
	}
	
   private String id;
   private String author;
   private String title;
   private String content;
   private Hotel hotel;
   private int freqAspectValue;
   private Double polarityValue;
   private Double weightValue;
   private int freqAspectLocation;
   private Double polarityLocation;
   private Double weightLocation;
   private int freqAspectCleanliness;
   private Double polarityCleanliness;
   private Double weightCleanliness;
   private int freqAspectSleepQuality;
   private Double polaritySleepQuality;
   private Double weightSleepQuality;
   private int freqAspectService;
   private Double polarityService;
   private Double weightService;   
   private int freqAspectAmenities;
   private Double polarityAmenities;
   private Double weightAmenities;
   private int freqAspectFood;
   private Double polarityFood;
   private Double weightFood;
   private List<Aspect> aspect;
      
   
   
      
}
