package com.diyaller.models;

import java.util.ArrayList;
import java.util.List;

public class Aspect {
	
	 public Aspect(){
		 this.features =  new ArrayList<Feature>();
	 }
		
    public String getAspect() {
		return aspect;
	}
	public void setAspect(String aspect) {
		this.aspect = aspect;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getPolarity() {
		return polarity;
	}
	public void setPolarity(String polarity) {
		this.polarity = polarity;
	}
	public List<Feature> getFeatures() {
		return features;
	}
	public void setFeatures(List<Feature> features) {
		this.features = features;
	}
	private String aspect;
    private String frequency;
    private String polarity;
    private List<Feature> features;
     
}
