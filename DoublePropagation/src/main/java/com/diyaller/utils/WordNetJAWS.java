package com.diyaller.utils;

import com.google.common.collect.Multimap;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bonesso on 28/12/15.
 */
public class WordNetJAWS {

    private  WordNetDatabase database;
    File file;
    public WordNetJAWS(){

        File f =  new File("/home/bonesso/Dropbox/WordNet-3.0/dict");
        System.setProperty("wordnet.database.dir",f.toString());
        this.database = WordNetDatabase.getFileInstance();
    }


    public Map<String, String>  getsynonymous(String wordForm){

        Map<String, String> mapSynon = new HashMap<>();
        NounSynset nounSynset;
        NounSynset[] hyponyms;

        WordNetDatabase database = WordNetDatabase.getFileInstance();
        Synset[] synsets = database.getSynsets(wordForm, SynsetType.NOUN);
        if(synsets.length > 0)
        {
            System.out.println("                BEGIN :                         " + wordForm);
            for (int i = 0; i < synsets.length; i++) {
                nounSynset = (NounSynset)(synsets[i]);
                hyponyms = nounSynset.getHyponyms();
                for (int j = 0; j < hyponyms.length; j++) {
                    for (int k = 0; k < hyponyms[j].getWordForms().length; k++) {
                        System.err.println(hyponyms[j].getWordForms()[k] + " - " + hyponyms[j].getDefinition());
                    }
                }

            }
            System.out.println("                END :                         " + wordForm);
        }


        return mapSynon;

    }

    public Boolean  checkNoun(String wordForm){

        Map<String, String> mapSynon = new HashMap<>();
        NounSynset nounSynset;
        NounSynset[] hyponyms;

        WordNetDatabase database = WordNetDatabase.getFileInstance();
        Synset[] synsets = database.getSynsets(wordForm, SynsetType.NOUN);
        if(synsets.length > 0)
        {
            return true;
        }
        return false;

    }

    public Boolean  checkAdjective(String wordForm){

        Map<String, String> mapSynon = new HashMap<>();
        NounSynset nounSynset;
        NounSynset[] hyponyms;

        WordNetDatabase database = WordNetDatabase.getFileInstance();
        Synset[] synsets = database.getSynsets(wordForm, SynsetType.ADJECTIVE);
        if(synsets.length > 0)
        {
            return true;
        }
        synsets = database.getSynsets(wordForm, SynsetType.ADJECTIVE_SATELLITE);
        if(synsets.length > 0)
        {
            return true;
        }
        return false;

    }





}
