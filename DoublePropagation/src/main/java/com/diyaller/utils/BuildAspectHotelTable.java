
package com.diyaller.utils;
import com.diyaller.database.*;
import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.EnglishStemmer;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bonesso on 15/12/15.
 */
public class BuildAspectHotelTable {
    private Map<String, String> mapAspects;
    private Map<String, Double> profile;
    private Map<String, Double> profilepolarity;
    private Map<String, Double> review;
    private Map<String, Double> reviewpolarity;
    private SnowballProgram stemmer;
    private EntityManager em;
    private int countReviews;
    private SentiWordNet sentiwordnet;

    public BuildAspectHotelTable(EntityManager entitymanager)
    {
        this.stemmer = new EnglishStemmer();
        this.countReviews =0;
        this.em = entitymanager;
        this.profile =  new HashMap<String,Double>();
        this.profilepolarity = new HashMap<String,Double>();
        this.review =  new HashMap<String,Double>();
        this.reviewpolarity = new HashMap<String,Double>();
        this.mapAspects = new HashMap<String, String>();

        try {
            this.sentiwordnet = new SentiWordNet("/home/bonesso/Dropbox/SentiWordNet-3.0/SentiWordNet_3.0.0_20130122.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        mapAspects.put(getSteammer("value"), "value");
        mapAspects.put(getSteammer("price"), "value");
        mapAspects.put(getSteammer("money"), "value");
        mapAspects.put(getSteammer("quality"), "value");
        mapAspects.put(getSteammer("deal"), "value");
        mapAspects.put(getSteammer("hotel"), "value");
        mapAspects.put(getSteammer("package"), "value");
        mapAspects.put(getSteammer("rate"), "value");
        mapAspects.put(getSteammer("resort"), "value");
        mapAspects.put(getSteammer("budget"), "value");
        mapAspects.put(getSteammer("ticket"), "value");
        mapAspects.put(getSteammer("accommodation"), "value");
        mapAspects.put(getSteammer("amount"), "value");
        mapAspects.put(getSteammer("credit card"), "value");
        mapAspects.put(getSteammer("dollar"), "value");
        mapAspects.put(getSteammer("building"), "value");
        mapAspects.put(getSteammer("travel"), "value");
        mapAspects.put(getSteammer("discount"), "value");
        mapAspects.put(getSteammer("agent"), "value");
        mapAspects.put(getSteammer("luxury"), "value");
        mapAspects.put(getSteammer("corner"), "value");
        mapAspects.put(getSteammer("location"), "location");
        mapAspects.put(getSteammer("place"), "location");
        mapAspects.put(getSteammer("distance"), "location");
        mapAspects.put(getSteammer("station"), "location");
        mapAspects.put(getSteammer("shuttle"), "location");
        mapAspects.put(getSteammer("cab"), "location");
        mapAspects.put(getSteammer("taxi"), "location");
        mapAspects.put(getSteammer("subway"), "location");
        mapAspects.put(getSteammer("airport"), "location");
        mapAspects.put(getSteammer("attraction"), "location");
        mapAspects.put(getSteammer("shopping"), "location");
        mapAspects.put(getSteammer("block"), "location");
        mapAspects.put(getSteammer("bus"), "location");
        mapAspects.put(getSteammer("ride"), "location");
        mapAspects.put(getSteammer("metro"), "location");
        mapAspects.put(getSteammer("mall"),  "location");
        mapAspects.put(getSteammer("train"),  "location");
        mapAspects.put(getSteammer("bus stop"),  "location");
        mapAspects.put(getSteammer("downtown"),  "location");
        mapAspects.put(getSteammer("park"), "location");
        mapAspects.put(getSteammer("theater"),  "location");
        mapAspects.put(getSteammer("strip"),  "location");
        mapAspects.put(getSteammer("district"),  "location");
        mapAspects.put(getSteammer("museum"),  "location");
        mapAspects.put(getSteammer("transportation"),  "location");
        mapAspects.put(getSteammer("quarter"),  "location");
        mapAspects.put(getSteammer("tourist interest"),  "location");
        mapAspects.put(getSteammer("heart"),"location");
        mapAspects.put(getSteammer("trolley"),  "location");
        mapAspects.put(getSteammer("middle"),  "location");
        mapAspects.put(getSteammer("square"),  "location");
        mapAspects.put(getSteammer("sight-seeing"), "location");
        mapAspects.put(getSteammer("room"),"room");
        mapAspects.put(getSteammer("size"),"room");
        mapAspects.put(getSteammer("bathroom"),"room");
        mapAspects.put(getSteammer("bath"),"room");
        mapAspects.put(getSteammer("closet"),"room");
        mapAspects.put(getSteammer("kitchen"),"room");
        mapAspects.put(getSteammer("kitchenette"),"room");
        mapAspects.put(getSteammer("dryer"),"room");
        mapAspects.put(getSteammer("microwave"),"room");
        mapAspects.put(getSteammer("hotel"),"room");
        mapAspects.put(getSteammer("view"),"room");
        mapAspects.put(getSteammer("floor"),"room");
        mapAspects.put(getSteammer("shower"),"room");
        mapAspects.put(getSteammer("tv"),"room");
        mapAspects.put(getSteammer("stay"),"room");
        mapAspects.put(getSteammer("property"),"room");
        mapAspects.put(getSteammer("sink"),"room");
        mapAspects.put(getSteammer("screen"),"room");
        mapAspects.put(getSteammer("window"),"room");
        mapAspects.put(getSteammer("balcony"),"room");
        mapAspects.put(getSteammer("cafe maker"),"room");
        mapAspects.put(getSteammer("option"),"room");
        mapAspects.put(getSteammer("refrigerator"),"room");
        mapAspects.put(getSteammer("mirror"),"room");
        mapAspects.put(getSteammer("ceiling"),"room");
        mapAspects.put(getSteammer("water pressure"),"room");
        mapAspects.put(getSteammer("neighborhood"),"room");
        mapAspects.put(getSteammer("cleanliness"),"cleanliness");
        mapAspects.put(getSteammer("cleaning"),"cleanliness");
        mapAspects.put(getSteammer("smell"),"cleanliness");
        mapAspects.put(getSteammer("smoke"),"cleanliness");
        mapAspects.put(getSteammer("carpet"),"cleanliness");
        mapAspects.put(getSteammer("smoking"),"cleanliness");
        mapAspects.put(getSteammer("hallway"),"cleanliness");
        mapAspects.put(getSteammer("furniture"),"cleanliness");
        mapAspects.put(getSteammer("wall"),"cleanliness");
        mapAspects.put(getSteammer("air conditioner"),"cleanliness");
        mapAspects.put(getSteammer("elevator"),"cleanliness");
        mapAspects.put(getSteammer("hall"),"cleanliness");
        mapAspects.put(getSteammer("air conditioning"),"cleanliness");
        mapAspects.put(getSteammer("conditioner"),"cleanliness");
        mapAspects.put(getSteammer("stair"),"cleanliness");
        mapAspects.put(getSteammer("noise"),"cleanliness");
        mapAspects.put(getSteammer("neighbor"),"cleanliness");
        mapAspects.put(getSteammer("construction"),"cleanliness");
        mapAspects.put(getSteammer("sound"),"cleanliness");
        mapAspects.put(getSteammer("toilet"),"cleanliness");
        mapAspects.put(getSteammer("complaint"),"cleanliness");
        mapAspects.put(getSteammer("fan"),"cleanliness");
        mapAspects.put(getSteammer("maintenance"),"cleanliness");
        mapAspects.put(getSteammer("ceiling"),"cleanliness");
        mapAspects.put(getSteammer("heat"),"cleanliness");
        mapAspects.put(getSteammer("level"),"cleanliness");
        mapAspects.put(getSteammer("sleep"),"sleep");
        mapAspects.put(getSteammer("bed"),"sleep");
        mapAspects.put(getSteammer("bedroom"),"sleep");
        mapAspects.put(getSteammer("pillow"),"sleep");
        mapAspects.put(getSteammer("sofa"),"sleep");
        mapAspects.put(getSteammer("linen"),"sleep");
        mapAspects.put(getSteammer("sheet"),"sleep");
        mapAspects.put(getSteammer("suite"),"sleep");
        mapAspects.put(getSteammer("bedding"),"sleep");
        mapAspects.put(getSteammer("couch"),"sleep");
        mapAspects.put(getSteammer("living room"),"sleep");
        mapAspects.put(getSteammer("towel"),"sleep");
        mapAspects.put(getSteammer("unit"),"sleep");
        mapAspects.put(getSteammer("chair"),"sleep");
        mapAspects.put(getSteammer("apartment"),"sleep");
        mapAspects.put(getSteammer("lobby"),"sleep");
        mapAspects.put(getSteammer("experience"),"sleep");
        mapAspects.put(getSteammer("space"),"sleep");
        mapAspects.put(getSteammer("studio"),"sleep");
        mapAspects.put(getSteammer("choice"),"sleep");
        mapAspects.put(getSteammer("town"),"sleep");
        mapAspects.put(getSteammer("road"),"sleep");
        mapAspects.put(getSteammer("boutique"),"sleep");
        mapAspects.put(getSteammer("employee"),"sleep");
        mapAspects.put(getSteammer("comfort"),"sleep");
        mapAspects.put(getSteammer("neighborhood"),"sleep");
        mapAspects.put(getSteammer("service"),"service");
        mapAspects.put(getSteammer("staff"),"service");
        mapAspects.put(getSteammer("reception"),"service");
        mapAspects.put(getSteammer("check-in"),"service");
        mapAspects.put(getSteammer("checkout"),"service");
        mapAspects.put(getSteammer("bartender"),"service");
        mapAspects.put(getSteammer("valet"),"service");
        mapAspects.put(getSteammer("member"),"service");
        mapAspects.put(getSteammer("concierge"),"service");
        mapAspects.put(getSteammer("front desk"),"service");
        mapAspects.put(getSteammer("maid"),"service");
        mapAspects.put(getSteammer("clerk"),"service");
        mapAspects.put(getSteammer("doorman"),"service");
        mapAspects.put(getSteammer("question"),"service");
        mapAspects.put(getSteammer("smile"),"service");
        mapAspects.put(getSteammer("check"),"service");
        mapAspects.put(getSteammer("bellman"),"service");
        mapAspects.put(getSteammer("bell man"),"service");
        mapAspects.put(getSteammer("manager"),"service");
        mapAspects.put(getSteammer("attitude"),"service");
        mapAspects.put(getSteammer("direction"),"service");
        mapAspects.put(getSteammer("help"),"service");
        mapAspects.put(getSteammer("request"),"service");
        mapAspects.put(getSteammer("notch"),"service");
        mapAspects.put(getSteammer("care"),"service");
        mapAspects.put(getSteammer("information"),"service");
        mapAspects.put(getSteammer("person"),"service");
        mapAspects.put(getSteammer("arrival"),"service");
        mapAspects.put(getSteammer("suggestion"),"service");
        mapAspects.put(getSteammer("guy"),"service");
        mapAspects.put(getSteammer("customer"),"service");
        mapAspects.put(getSteammer("luggage"),"service");
        mapAspects.put(getSteammer("bag"),"service");
        mapAspects.put(getSteammer("facility"), "facility");
        mapAspects.put(getSteammer("wifi"),  "facility");
        mapAspects.put(getSteammer("pool"),  "facility");
        mapAspects.put(getSteammer("gym"),  "facility");
        mapAspects.put(getSteammer("business"),  "facility");
        mapAspects.put(getSteammer("internet"),  "facility");
        mapAspects.put(getSteammer("parking"),  "facility");
        mapAspects.put(getSteammer("conference room"),  "facility");
        mapAspects.put(getSteammer("swimming pool"),  "facility");
        mapAspects.put(getSteammer("casino"),  "facility");
        mapAspects.put(getSteammer("garage"),  "facility");
        mapAspects.put(getSteammer("area"),  "facility");
        mapAspects.put(getSteammer("center"),  "facility");
        mapAspects.put(getSteammer("fitness room"),  "facility");
        mapAspects.put(getSteammer("fee"),  "facility");
        mapAspects.put(getSteammer("internet access"), "facility");
        mapAspects.put(getSteammer("traveler"),  "facility");
        mapAspects.put(getSteammer("spa"),  "facility");
        mapAspects.put(getSteammer("computer"),  "facility");
        mapAspects.put(getSteammer("connection"),  "facility");
        mapAspects.put(getSteammer("meeting"),  "facility");
        mapAspects.put(getSteammer("charge"),  "facility");
        mapAspects.put(getSteammer("activity"),  "facility");
        mapAspects.put(getSteammer("river"),  "facility");
        mapAspects.put(getSteammer("tub"),  "facility");
        mapAspects.put(getSteammer("grounds"),  "facility");
        mapAspects.put(getSteammer("pass"),  "facility");
        mapAspects.put(getSteammer("rooftop"),  "facility");
        mapAspects.put(getSteammer("slot"),  "facility");
        mapAspects.put(getSteammer("lounge"),  "facility");
        mapAspects.put(getSteammer("jacuzzi"),  "facility");
        mapAspects.put(getSteammer("machine"),  "facility");
        mapAspects.put(getSteammer("game"),  "facility");
        mapAspects.put(getSteammer("music"),  "facility");
        mapAspects.put(getSteammer("movie"),  "facility");
        mapAspects.put(getSteammer("beach"),  "facility");
        mapAspects.put(getSteammer("convention"), "facility");
        mapAspects.put(getSteammer("food"),"food");
        mapAspects.put(getSteammer("drink"),"food");
        mapAspects.put(getSteammer("dish"),"food");
        mapAspects.put(getSteammer("wine"),"food");
        mapAspects.put(getSteammer("salad"),"food");
        mapAspects.put(getSteammer("restaurant"),"food");
        mapAspects.put(getSteammer("meal"),"food");
        mapAspects.put(getSteammer("bar"),"food");
        mapAspects.put(getSteammer("breakfast"),"food");
        mapAspects.put(getSteammer("pizza"),"food");
        mapAspects.put(getSteammer("buffet variety"),"food");
        mapAspects.put(getSteammer("court"),"food");
        mapAspects.put(getSteammer("shop"),"food");
        mapAspects.put(getSteammer("dinner"),"food");
        mapAspects.put(getSteammer("selection"),"food");
        mapAspects.put(getSteammer("snack"),"food");
        mapAspects.put(getSteammer("fruit"),"food");
        mapAspects.put(getSteammer("lunch"),"food");
        mapAspects.put(getSteammer("cereal"),"food");
        mapAspects.put(getSteammer("egg"),"food");
        mapAspects.put(getSteammer("cheese"),"food");
        mapAspects.put(getSteammer("juice"),"food");
        mapAspects.put(getSteammer("variety"),"food");
        mapAspects.put(getSteammer("coffee"),"food");
        mapAspects.put(getSteammer("bagel"),"food");
        mapAspects.put(getSteammer("pastry"),"food");
        mapAspects.put(getSteammer("waffle"),"food");
        mapAspects.put(getSteammer("cafe"),"food");
        mapAspects.put(getSteammer("menu"),"food");
        mapAspects.put(getSteammer("tea"),"food");
        mapAspects.put(getSteammer("beer"),"food");
        mapAspects.put(getSteammer("cocktail"),"food");
        mapAspects.put(getSteammer("downstairs"),"food");
        mapAspects.put(getSteammer("option"),"food");
        mapAspects.put(getSteammer("item"),"food");
        mapAspects.put(getSteammer("gift"),"food");
        mapAspects.put(getSteammer("cup"),"food");
        mapAspects.put(getSteammer("dining"),"food");
    }




    public void genetaProfile(List<ReviewHotel> reviews){
        this.countReviews = 0;
        for (ReviewHotel r : reviews) {
            this.calculateRowProfile(r);
        }
    }

    private void calculateRowProfile(ReviewHotel r){


        Query query = em.createQuery("select c from ReviewHotelAspect c where c.reviewHotelEntity.id = :idr");
        List<ReviewHotelAspect> laspects = query.setParameter("idr",r.getId() ).getResultList();
        double overall = 0;
        double sum =0;


        for (ReviewHotelAspect u : laspects) {
            if(this.mapAspects.containsKey(u.getLemmatarget()))
            {
                //System.out.println("value:" + this.mapAspects.get(u.getLemmatarget()));
                //System.out.println("aspect:" + u.getLemmatarget());
                String aspectGroup = this.mapAspects.get(u.getLemmatarget());
                if (this.profile.containsKey(aspectGroup)) {
                    double i = this.profile.get(aspectGroup);
                    double j = this.profilepolarity.get(aspectGroup);
                    this.profile.replace(aspectGroup, i, i + 1.0);
                    double temp = this.sentiwordnet.extract(u.getWordformopinion(),"a");
                    sum  = ((j*j)+(temp*temp))/ (j+temp);
                    this.profilepolarity.replace(aspectGroup, j,sum);
                } else {
                    this.profile.put(aspectGroup, 1.0);
                    this.profilepolarity.put(aspectGroup, this.sentiwordnet.extract(u.getWordformopinion(), "a"));
                }

            }
            
        }

        saveProfile(r);
    }

    private void saveProfile(ReviewHotel r){

        double value =0;
        double location=0;
        double room=0;
        double cleanliness=0;
        double sleep =0;
        double service=0;
        double facility=0;
        double food=0;

        double valuepolarity =0;
        double locationpolarity=0;
        double roompolarity=0;
        double cleanlinesspolarity=0;
        double sleeppolarity =0;
        double servicepolarity=0;
        double facilitypolarity=0;
        double foodpolarity=0;

        double overall =   0;
        double total = 0;
        double countAspect = 0;


        this.countReviews +=1;
        for (String s : this.profile.keySet()) {
            switch (s){
                case "value":
                    value = this.profile.get(s);
                    valuepolarity = this.profilepolarity.get(s);
                    countAspect +=1;
                    break;
                case "location":
                    location = this.profile.get(s);
                    locationpolarity = this.profilepolarity.get(s);
                    countAspect +=1;
                    break;
                case "room":
                    room = this.profile.get(s);
                    roompolarity = this.profilepolarity.get(s);
                    countAspect +=1;
                    break;
                case "cleanliness":
                    cleanliness = this.profile.get(s);
                    cleanlinesspolarity = this.profilepolarity.get(s);
                    countAspect +=1;
                    break;
                case "sleep":
                    sleep = this.profile.get(s);
                    sleeppolarity = this.profilepolarity.get(s);
                    countAspect +=1;
                    break;
                case "service":
                    service = this.profile.get(s);
                    servicepolarity = this.profilepolarity.get(s);
                    countAspect +=1;
                    break;
                case "food":
                    food = this.profile.get(s);
                    facilitypolarity = this.profilepolarity.get(s);
                    countAspect +=1;
                    break;
                case "facility":
                    facility = this.profile.get(s);
                    foodpolarity = this.profilepolarity.get(s);
                    countAspect +=1;
                    break;
            }


        }


        Query query = em.createQuery("select c as total from RatingHotelEntity c where c.reviewHotelByIdReview.id = :idr");
        query.setParameter("idr",r.getId());
        RatingHotel ratings = (RatingHotel)query.getSingleResult();

        total = cleanliness + food + facility + location + room + service + value +  sleep;
        if (total == 0)
            total =1;
        if(countAspect > 0) {
            overall = (valuepolarity + locationpolarity + roompolarity + cleanlinesspolarity + sleeppolarity + servicepolarity + facilitypolarity + foodpolarity) / countAspect;
        }else
        {
            overall=0;
        }

        UserProfile profileEntity = new UserProfile();
        profileEntity.setIdReviewHotel(r);
        /*profileEntity.setAspectcleanliness(cleanliness / total);
        profileEntity.setCleanlinesspolarity(cleanlinesspolarity);
        profileEntity.setAspectfacility(food / total);
        profileEntity.setFacilitypolarity(facilitypolarity);
        profileEntity.setAspectfoodquality(facility / total);
        profileEntity.setFoodqualitypolarity(foodpolarity);
        profileEntity.setAspectlocation(location / total);
        profileEntity.setLocationpolarity(locationpolarity);
        profileEntity.setAspectroom(room / total);
        profileEntity.setRoompolarity(roompolarity);
        profileEntity.setAspectservice(service / total);
        profileEntity.setServicepolarity(servicepolarity);
        profileEntity.setAspectvalue(value / total);
        profileEntity.setValuepolarity(valuepolarity);
        profileEntity.setAspectsleepquality(sleep / total);
        profileEntity.setSleepqualitypolarity(sleeppolarity);
        profileEntity.setOverallpolarity(overall);
        profileEntity.setOverall(ratings.getOverall());*/

        profileEntity.setAspectcleanliness(cleanliness);
        profileEntity.setCleanlinesspolarity(cleanlinesspolarity);
        profileEntity.setAspectfacility(food);
        profileEntity.setFacilitypolarity(facilitypolarity);
        profileEntity.setAspectfoodquality(facility);
        profileEntity.setFoodqualitypolarity(foodpolarity);
        profileEntity.setAspectlocation(location);
        profileEntity.setLocationpolarity(locationpolarity);
        profileEntity.setAspectroom(room);
        profileEntity.setRoompolarity(roompolarity);
        profileEntity.setAspectservice(service);
        profileEntity.setServicepolarity(servicepolarity);
        profileEntity.setAspectvalue(value);
        profileEntity.setValuepolarity(valuepolarity);
        profileEntity.setAspectsleepquality(sleep);
        profileEntity.setSleepqualitypolarity(sleeppolarity);
        profileEntity.setOverallpolarity(overall);
        profileEntity.setOverall(ratings.getOverall());

        this.em.getTransaction().begin();
        this.em.persist(profileEntity);
        this.em.getTransaction().commit();
        this.profile.clear();
        //System.out.println("Saved reviews in profile: " + this.countReviews);

    }




    public void generateTable(List<ReviewHotel> reviews){
        this.countReviews = 0;
        for (ReviewHotel r : reviews) {
            this.calculateRowOfW(r);
        }
    }


    public void generateTableSingleReview(ReviewHotel review){
            this.calculateRowOfW(review);
    }

    private void calculateRowOfW(ReviewHotel r){



        Query query = em.createQuery("select c from ReviewHotelAspect c where c.idReviewHotel.id = :idr");
        List<ReviewHotelAspect> laspects = query.setParameter("idr",r.getId() ).getResultList();
        double overall = 0;
        double sum =0;


        for (ReviewHotelAspect u : laspects) {
            //System.out.println(u.getLemmatarget());
            if(this.mapAspects.containsKey(u.getLemmatarget()))
            {
                //System.out.println("value:" + this.mapAspects.get(u.getLemmatarget()));
                //System.out.println("aspect:" + u.getLemmatarget());
                String aspectGroup = this.mapAspects.get(u.getLemmatarget());
                if (this.review.containsKey(aspectGroup)) {
                    double i = this.review.get(aspectGroup);
                    double j = this.reviewpolarity.get(aspectGroup);
                    this.review.replace(aspectGroup, i, i + 1.0);
                    double temp = this.sentiwordnet.extract(u.getWordformopinion(), "a");
                    sum = (j + temp) / 2; //average polatiry
                    //sum  = ((j*j)+(temp*temp))/ (j+temp);calculate the weigth sentiment (Preference based clustering reviews for argumentative e-commerce recommendation)
                    this.reviewpolarity.replace(aspectGroup, j,sum);
                } else {
                    this.review.put(aspectGroup, 1.0);
                    this.reviewpolarity.put(aspectGroup, this.sentiwordnet.extract(u.getWordformopinion(), "a"));
                }

            }

        }

      saveMatrixW(r);
    }


    private void saveMatrixW(ReviewHotel r){

        double value =0;
        double location=0;
        double room=0;
        double cleanliness=0;
        double sleep =0;
        double service=0;
        double facility=0;
        double food=0;



        double valuepolarity =0;
        double locationpolarity=0;
        double roompolarity=0;
        double cleanlinesspolarity=0;
        double sleeppolarity =0;
        double servicepolarity=0;
        double facilitypolarity=0;
        double foodpolarity=0;

        double overall =   0;
        double total = 0;
        double countAspect = 0;


        this.countReviews +=1;
        for (String s : this.review.keySet()) {
            switch (s){
                case "value":
                    value = this.review.get(s);
                    valuepolarity = this.reviewpolarity.get(s);
                    countAspect +=1;
                    break;
                case "location":
                    location = this.review.get(s);
                    locationpolarity = this.reviewpolarity.get(s);
                    countAspect +=1;
                    break;
                case "room":
                    room = this.review.get(s);
                    roompolarity = this.reviewpolarity.get(s);
                    countAspect +=1;
                    break;
                case "cleanliness":
                    cleanliness = this.review.get(s);
                    cleanlinesspolarity = this.reviewpolarity.get(s);
                    countAspect +=1;
                    break;
                case "sleep":
                    sleep = this.review.get(s);
                    sleeppolarity = this.reviewpolarity.get(s);
                    countAspect +=1;
                    break;
                case "service":
                    service = this.review.get(s);
                    servicepolarity = this.reviewpolarity.get(s);
                    countAspect +=1;
                    break;
                case "food":
                    food = this.review.get(s);
                    facilitypolarity = this.reviewpolarity.get(s);
                    countAspect +=1;
                    break;
                case "facility":
                    facility = this.review.get(s);
                    foodpolarity = this.reviewpolarity.get(s);
                    countAspect +=1;
                    break;
            }


        }


        Query query = em.createQuery("select c as total from RatingHotel c where c.idReview.id = :idr");
        query.setParameter("idr",r.getId());
        RatingHotel ratings = (RatingHotel)query.getSingleResult();
        total = cleanliness + food + facility + location + room + service + value +  sleep;
        if (total == 0)
            total =1;
        if(countAspect > 0) {
            overall = (valuepolarity + locationpolarity + roompolarity + cleanlinesspolarity + sleeppolarity + servicepolarity + facilitypolarity + foodpolarity) / countAspect;
        }else
        {
            overall=0;
        }




        MatrixHotelAspect matrixReview = new MatrixHotelAspect();
        matrixReview.setIdReviewHotel(r);
        matrixReview.setAspectcleanliness(Double.isNaN(cleanliness ) ? 0.0 : cleanliness);
        matrixReview.setCleanlinesspolarity(Double.isNaN(cleanlinesspolarity) ? 0.0 : cleanlinesspolarity);
        matrixReview.setAspectfacility(Double.isNaN(food) ? 0.0 : food);
        matrixReview.setFacilitypolarity(Double.isNaN(facilitypolarity) ? 0.0 : facilitypolarity);
        matrixReview.setAspectfoodquality(Double.isNaN(facility) ? 0.0 :  facility);
        matrixReview.setFoodqualitypolarity(Double.isNaN(foodpolarity) ? 0.0 : foodpolarity);
        matrixReview.setAspectlocation(Double.isNaN(location) ? 0.0 :  location);
        matrixReview.setLocationpolarity(Double.isNaN(locationpolarity) ? 0.0 : locationpolarity);
        matrixReview.setAspectroom(Double.isNaN(room) ? 0.0 : room);
        matrixReview.setRoompolarity(Double.isNaN(roompolarity) ? 0.0 : roompolarity);
        matrixReview.setAspectservice(Double.isNaN(service) ? 0.0 : service);
        matrixReview.setServicepolarity(Double.isNaN(servicepolarity) ? 0.0 : servicepolarity);
        matrixReview.setAspectvalue(Double.isNaN(value) ? 0.0 : value);
        matrixReview.setValuepolarity(Double.isNaN(valuepolarity) ? 0.0 : valuepolarity);
        matrixReview.setAspectsleepquality(Double.isNaN(sleep) ? 0.0 : sleep);
        matrixReview.setSleepqualitypolarity(Double.isNaN(sleeppolarity) ? 0.0 : sleeppolarity);
        matrixReview.setOverallpolarity(Double.isNaN(overall) ? 0.0 : overall);
        matrixReview.setOverall(ratings.getOverall());
        

        this.em.getTransaction().begin();
        this.em.persist(matrixReview);
        this.em.getTransaction().commit();
        this.review.clear();
        //System.out.println("Saved reviews in matrix: "  + countReviews);

    }


    private String getSteammer(String word)
    {
        this.stemmer.setCurrent(word.toLowerCase().trim().toLowerCase());
        this.stemmer.stem();
        return stemmer.getCurrent();
    }


    
     

}
