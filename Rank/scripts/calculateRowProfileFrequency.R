calculateRowProfileFrequency=function(reviewsList){
             threshold<-1
             #cat(sprintf("this is the review list size %s \n", dim(reviewsList[1])))
             aspectvalue <-rep(0,1)
             aspectlocation <-rep(0,1)
             aspectroom <-rep(0,1)
             aspectcleanliness <-rep(0,1)
             aspectsleepquality<-rep(0,1)
             aspectservice <-rep(0,1)
             aspectfacility<-rep(0,1)
             aspectfoodquality <-rep(0,1)
             profile <-  data.frame(aspectvalue,aspectlocation,aspectroom,aspectcleanliness,aspectsleepquality,aspectservice,aspectfacility,aspectfoodquality)
          
            for(i in 1:dim(reviewsList)[1])  
            {
              profile$aspectvalue[1] <- profile$aspectvalue[1] +  reviewsList$aspectvalue[i]
              profile$aspectlocation[1] <- profile$aspectlocation[1] +  reviewsList$aspectlocation[i]
              profile$aspectroom[1] <- profile$aspectroom[1] +  reviewsList$aspectroom[i]
              profile$aspectcleanliness[1] <- profile$aspectcleanliness[1] +  reviewsList$aspectcleanliness[i]
              profile$aspectsleepquality[1]<-profile$aspectsleepquality[1] + reviewsList$aspectsleepquality[i]
              profile$aspectservice[1] <- profile$aspectservice[1] +  reviewsList$aspectservice[i]
              profile$aspectfacility[1]<- profile$aspectfacility[1] +  reviewsList$aspectfacility[i]
              profile$aspectfoodquality[1] <- profile$aspectfoodquality[1] +  reviewsList$aspectfoodquality[i] 
            }
          
           profile$aspectvalue <- if ( profile$aspectvalue[1]>= threshold) 1 else 0
           profile$aspectlocation <- if ( profile$aspectlocation[1]>= threshold) 1 else 0
           profile$aspectroom<- if (profile$aspectroom[1] >= threshold) 1 else 0
           profile$aspectcleanliness <- if (profile$aspectcleanliness[1] >= threshold) 1 else 0
           profile$aspectsleepquality<-if ( profile$aspectsleepquality[1]>= threshold) 1 else 0
           profile$aspectservice <- if ( profile$aspectservice[1]>= threshold) 1 else 0
           profile$aspectfacility<- if ( profile$aspectfacility[1]>= threshold) 1 else 0
           profile$aspectfoodquality <-if ( profile$aspectfoodquality[1]>= threshold) 1 else 0
          
  
           
          return(profile)
}

