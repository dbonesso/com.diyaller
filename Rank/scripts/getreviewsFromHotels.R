getreviewsFromHotels = function(hotels){

workdir<- getwd()
setwd("..")
setwd("..")

drv <- dbDriver("PostgreSQL")
con <- dbConnect(drv, dbname = "sentiobserver",host = "localhost",port = 5432,user = "postgres",password ="ultrabook")


reviews_ids <- paste("'",reviews$id_review,"'",sep="",collapse=",")
queryDB <- sprintf( "select id_review_hotel as id,rtrim(A.author) as author,rtrim(content) as content,merchantid as hotel,
                     aspectvalue ,valuepolarity ,
                     aspectlocation ,locationpolarity  ,
                     aspectroom  , roompolarity ,
                     aspectcleanliness , cleanlinesspolarity ,
                     aspectsleepquality , sleepqualitypolarity ,
                     aspectservice ,servicepolarity ,
                     aspectfacility ,facilitypolarity ,
                     aspectfoodquality ,foodqualitypolarity,
                     B.overall
                     from review_hotel A, 
                     matrix_hotel_aspect B 
                     where B.id_review_hotel = A.id
                     AND
                     A.merchantid IN (%s)", hotels )
recordSource  <- dbSendQuery(con,queryDB)
table <- fetch(recordSource, n = -1)
saveRDS(table, file="./tables/data_review_hotels.rds")
dbDisconnect(con)
setwd(workdir)

}