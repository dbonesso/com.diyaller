#generate the rank
#this use a score using the weights of the user profile to weight the aspects. The values are in the scale [1,5]
rankRegression=function(cross_validation,con){

#path_file <- paste0(path.expand('~'),'/com.diyaller/cross_validation')
#setwd(path_file)

user_profile_name <- paste('/home/com.diyaller/profile/user_profile_weights.rds',sep="")    
fileRDS <- gzfile(user_profile_name)
user_profile<-readRDS(fileRDS)
close(fileRDS)  
  
file_name <- paste('/home/com.diyaller/tables/matrix_weights.rds',sep="")    
fileRDS <- gzfile(file_name)
matrix_regression<-readRDS(fileRDS)
close(fileRDS)   
relevant <- 10



for(i in 1:dim(user_profile)[1])  # for each row
{
  #print(paste0('user profile: ',i))
  #print(paste0('validation: ',user_profile$id_validation[i]))
  #print(paste0('user profile: ',i))
  #print(paste0('validation: ',user_profile$id_validation[i]))
  all_reviews_from_hotel <- getHotelReviewsWeights(matrix_regression,user_profile$id_hotel[i],con)
  relevancy <- rep(0,dim(all_reviews_from_hotel)[1])
  position <- rep(0,dim(all_reviews_from_hotel)[1])
  rank <- rep(0,dim(all_reviews_from_hotel)[1])
  reviewId <- rep(0,dim(all_reviews_from_hotel)[1])
  valuepolarity <- rep(0,dim(all_reviews_from_hotel)[1])
  locationpolarity <- rep(0,dim(all_reviews_from_hotel)[1])
  roompolarity <- rep(0,dim(all_reviews_from_hotel)[1])
  cleanlinesspolarity <- rep(0,dim(all_reviews_from_hotel)[1])
  sleepqualitypolarity <- rep(0,dim(all_reviews_from_hotel)[1])
  servicepolarity <- rep(0,dim(all_reviews_from_hotel)[1])
  facilitypolarity <- rep(0,dim(all_reviews_from_hotel)[1])
  foodqualitypolarity <- rep(0,dim(all_reviews_from_hotel)[1])
  overall <-rep(0,dim(all_reviews_from_hotel)[1])
  overallEstimated <-rep(0,dim(all_reviews_from_hotel)[1])
  rank_reviews <- data.frame(relevancy,position,rank,reviewId,valuepolarity,locationpolarity,roompolarity,cleanlinesspolarity,sleepqualitypolarity,servicepolarity,facilitypolarity,foodqualitypolarity,overall,overallEstimated)
  
  
  maxBeta1 <-max(all_reviews_from_hotel$betavaluepolarity)
  maxBeta2 <-max(all_reviews_from_hotel$betalocationpolarity)
  maxBeta3 <-max(all_reviews_from_hotel$betaroompolarity)
  maxBeta4 <-max(all_reviews_from_hotel$betacleanlinesspolarity)
  maxBeta5 <-max(all_reviews_from_hotel$betasleepqualitypolarity)
  maxBeta6 <-max(all_reviews_from_hotel$betaservicepolarity)
  maxBeta7 <-max(all_reviews_from_hotel$betafacilitypolarity)
  maxBeta8 <-max(all_reviews_from_hotel$betafoodqualitypolarity)
  
  
  minBeta1 <-min(all_reviews_from_hotel$betavaluepolarity)
  minBeta2 <-min(all_reviews_from_hotel$betalocationpolarity)
  minBeta3 <-min(all_reviews_from_hotel$betaroompolarity)
  minBeta4 <-min(all_reviews_from_hotel$betacleanlinesspolarity)
  minBeta5 <-min(all_reviews_from_hotel$betasleepqualitypolarity)
  minBeta6 <-min(all_reviews_from_hotel$betaservicepolarity)
  minBeta7 <-min(all_reviews_from_hotel$betafacilitypolarity)
  minBeta8 <-min(all_reviews_from_hotel$betafoodqualitypolarity)
  
  
  
  for(j in 1:dim(all_reviews_from_hotel)[1])  # for each row
  {
    rank_reviews$position[j]  <- 0
    rank_reviews$rank[j] <- 0
    rank_reviews$reviewId[j]  <- all_reviews_from_hotel$reviewid[j]
    rank_reviews$valuepolarity[j]  <- all_reviews_from_hotel$betavaluepolarity[j] * all_reviews_from_hotel$valuepolarity[j];  #(((all_reviews_from_hotel$betavaluepolarity[j] - minBeta1) * (5 - 1)) / (maxBeta1 - minBeta1)) + 1 
    rank_reviews$locationpolarity[j]  <-all_reviews_from_hotel$betalocationpolarity[j] * all_reviews_from_hotel$polarity[j] #(((all_reviews_from_hotel$betalocationpolarity[j] - minBeta2) * (5 - 1)) / (maxBeta2 - minBeta2)) + 1
    rank_reviews$roompolarity[j]  <- all_reviews_from_hotel$betaroompolarity[j]*all_reviews_from_hotel$roompolarity[j] #(((all_reviews_from_hotel$betaroompolarity[j] - minBeta3) * (5 - 1)) / (maxBeta3 - minBeta3)) + 1
    rank_reviews$cleanlinesspolarity[j]  <- all_reviews_from_hotel$betacleanlinesspolarity[j]*all_reviews_from_hotel$cleanlinesspolarity[j]  #(((all_reviews_from_hotel$betacleanlinesspolarity[j] - minBeta4) * (5 - 1)) / (maxBeta4 - minBeta4)) + 1
    rank_reviews$sleepqualitypolarity[j]  <- all_reviews_from_hotel$betasleepqualitypolarity[j]*all_reviews_from_hotel$sleepqualitypolarity[j] #(((all_reviews_from_hotel$betasleepqualitypolarity[j] - minBeta5) * (5 - 1)) / (maxBeta5 - minBeta5)) + 1
    rank_reviews$servicepolarity[j] <- all_reviews_from_hotel$betaservicepolarity[j] #(((all_reviews_from_hotel$betaservicepolarity[j] - minBeta6) * (5 - 1)) / (maxBeta6 - minBeta6)) + 1
    rank_reviews$facilitypolarity[j] <- all_reviews_from_hotel$betafacilitypolarity[j] #(((all_reviews_from_hotel$betafacilitypolarity[j] - minBeta7) * (5 - 1)) / (maxBeta7 - minBeta7)) + 1
    rank_reviews$foodqualitypolarity[j] <-all_reviews_from_hotel$foodqualitypolarity[j] #(((all_reviews_from_hotel$betafacilitypolarity[j] - minBeta8) * (5 - 1)) / (maxBeta8 - minBeta8)) + 1
    rank_reviews$overall[j] <- all_reviews_from_hotel$overall[j]
    rank_reviews$overallEstimated[j] <- all_reviews_from_hotel$overallEstimated[j]
    
    
    
    profile <- c( 
      (((user_profile$valuepolarity[i] - minBeta1) * (5 - 1)) / (maxBeta1 - minBeta1)) + 1,
      (((user_profile$locationpolarity[i] - minBeta2) * (5 - 1)) / (maxBeta2 - minBeta2)) + 1,
      (((user_profile$roompolarity[i] - minBeta3) * (5 - 1)) / (maxBeta3 - minBeta3)) + 1,
      (((user_profile$cleanlinesspolarity[i] - minBeta4) * (5 - 1)) / (maxBeta4 - minBeta4)) + 1,
      (((user_profile$sleepqualitypolarity[i] - minBeta5) * (5 - 1)) / (maxBeta5 - minBeta5)) + 1,
      (((user_profile$servicepolarity[i] - minBeta6) * (5 - 1)) / (maxBeta6 - minBeta6)) + 1,
      (((user_profile$facilitypolarity[i] - minBeta7) * (5 - 1)) / (maxBeta7 - minBeta7)) + 1,
      (((user_profile$foodqualitypolarity[i] - minBeta8) * (5 - 1)) / (maxBeta8 - minBeta8)) + 1
    )
    review <- c(rank_reviews$valuepolarity[j],rank_reviews$locationpolarity[j],rank_reviews$roompolarity[j],rank_reviews$cleanlinesspolarity[j],rank_reviews$sleepqualitypolarity[j],rank_reviews$servicepolarity[j],rank_reviews$facilitypolarity[j],rank_reviews$foodqualitypolarity[j])
    rank_reviews$rank[j] <-dist(rbind(profile,review) ,method="minkowski")
  }
  
  rank_reviews<-rank_reviews[ order(-rank_reviews[,2]), ]
  count <- 0;
  for(j in 1:dim(all_reviews_from_hotel)[1])  # for each row
  {
    count = count + 1  
    rank_reviews$position[j] <- count
  }
  rank_name <- paste('/home/com.diyaller/rank_cross_validation/rank_regression',user_profile$id_validation[i],'.rds',sep="")
  saveRDS(rank_reviews,file=rank_name)
  
}

id_validation <-  rep(0,dim(user_profile)[1])
id_test <-  rep(0,dim(user_profile)[1])
id_hotel <-  rep(0,dim(user_profile)[1])
MRR <-  rep(0,dim(user_profile)[1])
rank_result <- data.frame(id_validation,id_test,id_hotel,MRR)

#load the rank to memory and calcule the 
for(i in 1:dim(user_profile)[1])  # for each row
{

  rank_base_line_name <- paste('/home/com.diyaller/rank_cross_validation/rank_base_rating',user_profile$id_validation[i],".rds",sep="")
  fileRDS <- gzfile(rank_base_line_name)
  rank_base_line<-readRDS(fileRDS)


  fileRDS <- gzfile(paste('/home/com.diyaller/rank_cross_validation/rank_regression',user_profile$id_validation[i],'.rds',sep=""))
  rank<-readRDS(fileRDS)
  close(fileRDS)  
  rank_position <-rank[rank$reviewId==user_profile$id_validation[i],]
  rank_result$id_validation[i] <- cross_validation$id_validation[i]
  rank_result$id_test[i] <-  cross_validation$id_test[i]
  rank_result$id_hotel[i] <- cross_validation$id_hotel[i]
  n <- dim(rank)[1]
  #rank_result$MRR[i] <-  ((n - rank_position$position) / n)
  rank_result$MRR[i] <-  (1/rank_position$position)
  rank_result$MRR[i] <- 1/r
}

rank_name <- paste('/home/com.diyaller/rank/rank_regressionMRR.rds',sep="")
saveRDS(rank_result,file=rank_name)





}



