#We have 896 reviews. Total 74578 Reviews
#sudo apt-get install r-cran-rjava
#R CMD javareconf
#
"This function takes generate a matrix for cross validation, and generate cross validation table"

buildLeaveOneOut = function(con){
  workdir<- getwd()
  setwd("..")
  #call java code to genrerate the profile
  #if(file.exists("cross_validation.rda")==TRUE)
  #    file.remove("cross_validation.rda")
 
    
    queryDB <- "SELECT B.id as id, B.merchantid as merchantid, B.author as author
                from
                matrix_oracle A,
                review_hotel B
                where
                B.id = A.id
                and
                B.author = A.author
                and 
                B.author in (Select author from matrix_oracle group by author having count(id) >= 3)
                order by author"
                recordSource  <- dbSendQuery(con,queryDB)
                review_to_build_D <- fetch(recordSource, n = -1)
    
    id_validation <-  rep(0,0)
    id_test       <-  rep(0,0)
    id_hotel      <-  rep(0,0)
    author        <-  rep(0,0)                                                    
    cross_validation <- data.frame(id_validation,id_test,id_hotel,author)
    #for each author I need generate cross validation 
    for(i in 1:dim(review_to_build_D)[1])  # for each row
    {   
        selected <- subset(review_to_build_D, author ==review_to_build_D$author[i] & id !=review_to_build_D$id[i])
        #get all reviews from author which at least comment 4 times
        if(dim(selected)[1]>=3)
        {
          #print(selected)
          new_row <- c(review_to_build_D$id[i],paste(selected$id,collapse = ","),review_to_build_D$merchantid[i],review_to_build_D$author[i])
          cross_validation <- insertRow(cross_validation,new_row)
        }
    }   
    saveRDS(cross_validation,file='./tables/cross_validation2.rds')
    
    
}





