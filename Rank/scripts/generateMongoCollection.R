generateMongoCollection = function(){
  
  options(sqldf.driver = "SQLite")
  urlConnection = "mongodb://experimental:data103@ds021182.mlab.com:21182/experimental_test";
  #urlConnection = "mongodb://experimental:data103@ds011024.mlab.com:11024/experimental";
  #con <- mongo("review", url = "mongodb://experimental:data103@ds011024.mlab.com:11024/experimental")
  #get the ten authors with best MRR profile.
  authors <- sqldf("select distinct(authorreviews) from rank_frequency order by MRR desc limit 10")
  
  #convert the authors to a list of string comma separated.
  authorList <- ''
  for(i in 1:dim(authors)[1])  # for each row
  {
    r <-paste("'",authors$authorreviews[i],"'",sep="")
    if (authorList =='')
    {
      authorList <- r
    }else{
      authorList <-paste(authorList,r,sep=',')
    }
    
  }
  #take all revies from the authors to generate validate dataset
  reviewsJson <- sqldf(sprintf("select * from reviews where author in (%s) order by author",paste(authorList,sep="")))
  
  
  #take all hotel from the validate dataset
  hotels <- sqldf("select distinct(hotel) from reviewsJson")
  
  #convert the hotels to a list of string comma separated.
  hotelsList <- ''
  for(i in 1:dim(hotels)[1])  # for each row
  {
    r <-paste("'",hotels$hotel[i],"'",sep="")
    if (hotelsList =='')
    {
      hotelsList <- r
    }else{
      hotelsList <-paste(hotelsList,r,sep=',')
    }
    
  }
  
  #reviews from hotel
  #getreviewsFromHotels(hotelsList)
  reviewsHotel <- sqldf("select * from reviews_hotels where id in ('UR120815708','UR115757366')")
  con <- mongo("reviewsTrain", url =urlConnection)
  reviewsHotel[1,"train"]<- 1
  reviewsHotel[2,"train"]<- 2
  con$insert(reviewsHotel)
  
 
  
  #View(reviewsHotel)
  #View(reviews_hotels)
  
  reviewsJson[1:16,"group"]<- "group1"
  reviewsJson[17:34,"group"]<-"group2"
  reviewsJson[35:49,"group"]<-"group3"
  
  
  
  #fileConn<-file("dataReview.json")
  #writeLines(toJSON(reviewsJson), fileConn)
  #close(fileConn)
  con <- mongo("review", url =urlConnection)
  con$insert(reviewsJson)
  
  group <- rep(0,3)
  total <- rep(0,3)
  reviewExperimentControl <- data.frame(group,total)
  reviewExperimentControl$group[1]<- "group1"
  reviewExperimentControl$group[2]<- "group2"
  reviewExperimentControl$group[3]<- "group3"
  
  
  
  con <- mongo("reviewExperimentControl", url =urlConnection)
  con$insert(reviewExperimentControl)
  
  
  judge <- rep(0,1)
  text <-  rep(0,1)
  survey <- data.frame(judge,text)
  

  con <- mongo("survey", url =urlConnection)
  con$insert(survey)
  
  rm(con)
   
  #con <- mongo("review", url =urlConnection)
  #con$drop()
  #con <- mongo("reviewsTrain", url =urlConnection)
  #con$drop()
  #con <- mongo("reviewExperimentControl", url =urlConnection)
  #con$drop() 
  #con <- mongo("survey", url =urlConnection)
  #con$drop()
  #con <- mongo("user", url =urlConnection)
  #con$drop() 
  #con <- mongo("judgement", url =urlConnection)
  #con$drop() 
  
}